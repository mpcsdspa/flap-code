subroutine compute_spanwise_load_FS(N, t_val, fx, fy, fz, mx, my, mz)
  use finite_elements
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  use my_funcs
  implicit none

  !=======================================================================
  !                               Inputs
  !=======================================================================
  integer, intent(IN)                            :: N

  double precision, intent(IN)                   :: t_val

  !=======================================================================
  !                              Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: fx(N), fy(N), fz(N), mx(N), my(N), mz(N)

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  integer                                        :: i, k, element_num

  double precision                               ::  x_min, x_max, r1, t1, w_ddash, w_dddash, theta_double_dot, &
                                                     q_vec(nodes, dof_per_element), theta, theta_dot, x(N), &
                                                     x_domain(np), theta_0, theta_1c, psi, &
                                                     theta_1s, lambda, w, w_dash, w_dot, w_dot_dash, &
                                                     w_double_dot, w_ddot_dash, w_tdot, w_tdot_dash, &
                                                     u_R, u_P, u_T, L_u_aero, L_v_aero, L_w_aero, M_phi_bar, &
                                                     w_root, L_ubar, L_vbar, L_wbar, &
                                                     M_u_inertial, M_v_inertial, M_w_inertial, M_u_aero, M_v_aero, &
                                                     M_w_aero, L_u_inertial, L_v_inertial, L_w_inertial, &
                                                     w_dash_root, w_dot_root, w_dot_dash_root, l

  !=======================================================================
  !                        Blade Parmeters (Read from module)
  !=======================================================================

   x_min = offset * R/2
   x_max = R/2

   l = R/ne

   q_input = 10.0d0
   q_output = 0.0d0
   if ( aerodynamic_model == 2 ) then
     do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
       q_input = q_output
       call aero_code()
     end do
   else if (aerodynamic_model == 1) then
     q_input = q_output
     call aero_code()
   else
     print *, 'No Such Aerodynamic Model exists!!!'
   end if


   q_input = q_output



   fx = 0.0d0
   fy = 0.0d0
   fz = 0.0d0
   mx = 0.0d0
   my = 0.0d0
   mz = 0.0d0

   theta_0 = trim_vec(3)
   theta_1c = trim_vec(4)
   theta_1s = trim_vec(5)
   lambda = trim_vec(6)

   call compute_w_double_dot()


   psi = omega * t_val
   theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)
   theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))
   theta_double_dot = -omega*omega * (theta_1c * cos(psi) + theta_1s * sin(psi))


   call linspace(x_min, x_max, N, x)

   do i = 1, N, 1
     x_min = x(i)
     x_max = R/2
     x_domain = 0.5d0 * ((x_max + x_min) + l_nodes*(x_max - x_min))

     do k = 1, np, 1

       !=======================================================================
       !                Compute w, w_dash, w_dot, w_dot_dash
       !=======================================================================
       element_num = ((x_domain(k) - offset - mod(x_domain(k)-offset, l))/l) + 1
       call find_location(x_domain(k), t_val, q_output, element_num, q_vec, r1, t1)
       call compute_w(x_domain(k), t_val, q_vec, r1, t1, 1, w, w_dash, w_dot, w_dot_dash)


       !=======================================================================
       !                Compute w_root, w_dash_root, w_dot_root, w_dot_dash_root
       !=======================================================================
       element_num = ((x_min - offset - mod(x_min-offset, l))/l) + 1
       call find_location(x_min, t_val, q_output, element_num, q_vec, r1, t1)
       call compute_w(x_min, t_val, q_vec, r1, t1, 1, w_root, w_dash_root, w_dot_root, w_dot_dash_root)

       !=======================================================================
       !                Compute w_double_dot, w_ddot_dash
       !=======================================================================
       element_num = ((x_domain(k) - offset - mod(x_domain(k)-offset, l))/l) + 1

       call find_location(x_domain(k), t_val, q_ddot, element_num, q_vec, r1, t1)

       call compute_w(x_domain(k), t_val, q_vec, r1, t1, 1, w_double_dot, w_ddot_dash, w_tdot, w_tdot_dash)



       !=======================================================================
       !                Compute velocities
       !=======================================================================
       call compute_velocities(x_domain(k), t_val, w, w_dash, w_dot, w_dot_dash, u_R, u_P, u_T, &
                               L_u_aero, L_v_aero, L_w_aero, M_phi_bar)




       L_ubar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_R * u_T)

       L_vbar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_T * u_T - (Cl_o*u_P - Cd1*abs(u_P))*u_T + (Cl_alpha - Cd2)*u_P*u_P)

       L_wbar = (Lock_no/(6.0d0*Cl_alpha)) * (Cl_o*u_T*u_T - (Cl_alpha + Cdo)*u_T*u_P + Cd1*abs(u_P)*u_P)

       L_u_aero = (1 - w_dash*w_dash*0.5d0) * L_ubar - w_dash * sin(theta) * L_vbar - w_dash * cos(theta) * L_wbar
       L_u_aero = 0.0d0

       L_v_aero = cos(theta) * L_vbar - sin(theta) * L_wbar

       L_w_aero = w_dash * L_ubar + (1 - 0.5d0*w_dash*w_dash) * sin(theta)*L_vbar + (1 - w_dash*w_dash*0.5d0)*cos(theta)*L_wbar

       M_phi_bar = (Lock_no/(6.0d0*Cl_alpha)) * ((chord/R)*(Cmac*(u_T*u_T + u_P*u_P) - f1*u_T*u_P)) - e_d*L_wbar


       M_u_aero = (1 - 0.5d0*w_dash*w_dash) * M_phi_bar - (w - w_root)*L_v_aero
       M_v_aero = (w - w_root)*L_u_aero - (x_domain(k) - x_min)*L_w_aero
       M_w_aero = (x_domain(k) - x_min)*L_v_aero


       L_u_inertial = - (-x_domain(k) + beta_p * w + 2.0d0*theta_dot*e_g*sin(theta))

       L_v_inertial = - (-2.0d0*beta_p*w_dot + e_g*cos(theta)*(-theta_dot*theta_dot -2.0d0*beta_p*theta_dot &
                             - 1.0d0 - 2.0d0*w_dash*theta_dot) + e_g*sin(theta)*(-theta_double_dot - 2.0d0*w_dot_dash))

       L_w_inertial = - (w_double_dot + beta_p*x_domain(k) &
                             + e_g*sin(theta)*(-w_dash - 2.0d0*beta_p*theta_dot - theta_dot*theta_dot) &
                             + e_g*cos(theta)*theta_double_dot)



       M_u_inertial = - (Km*Km*theta_double_dot + cos(theta)*sin(theta)*(Km2*Km2 - Km1*Km1)*(1 + 2.0d0*w_dash*theta_dot) &
                             + 2.0d0*w_dot_dash*(Km2*Km2*sin(theta)*sin(theta) + Km1*Km1*cos(theta)*cos(theta)) &
                             + e_g*cos(theta)*(w_double_dot + beta_p*x_domain(k)) + 2.0d0*e_g*sin(theta)*beta_p*w_dot)

       M_v_inertial = - (-2.0d0*w_dot_dash*theta_dot*cos(theta)*sin(theta)*(Km2*Km2 - Km1*Km1) &
                             + (Km2*Km2*sin(theta)*sin(theta) + Km1*Km1*cos(theta)*cos(theta)) * (w_dash &
                             + beta_p + 2.0d0*theta_dot - 2.0d0*beta_p*theta_dot - w_ddot_dash) &
                             + e_g*sin(theta)*(beta_p*w - x_domain(k) + beta_p*x_domain(k)*w_dash))

       M_v_inertial = 0.0d0

       M_w_inertial = - (Km*Km*w_dash*theta_double_dot + sin(theta)*cos(theta)*(Km2*Km2 - Km1*Km1)*(w_ddot_dash &
                             - beta_p - 2.0d0*theta_dot) + 2.0d0*w_dot_dash*theta_dot*(Km2*Km2*cos(theta)*cos(theta) &
                             + Km1*Km1*sin(theta)*sin(theta)) + e_g*cos(theta)*(x_domain(k) - beta_p*w))

       M_u_inertial = M_u_inertial - (w - w_root) * L_v_inertial
       M_v_inertial = M_v_inertial + (w - w_root) * L_u_inertial - (x_domain(k) - x_min)*L_w_inertial
       M_w_inertial = M_w_inertial + (x_domain(k) - x_min) * L_v_inertial

       fx(i) = fx(i) + l_weights(k) * (L_u_aero + L_u_inertial)
       fy(i) = fy(i) + l_weights(k) * (L_v_aero + L_v_inertial)
       fz(i) = fz(i) + l_weights(k) * (L_w_aero + L_w_inertial)

       mx(i) = mx(i) + l_weights(k) * (M_u_aero + M_u_inertial)
       my(i) = my(i) + l_weights(k) * (M_v_aero + M_v_inertial)
       mz(i) = mz(i) + l_weights(k) * (M_w_aero + M_w_inertial)


    end do
    mx(i) = mx(i) * 0.5d0 * (x_max - x_min)
    my(i) = my(i) * 0.5d0 * (x_max - x_min)
    mz(i) = mz(i) * 0.5d0 * (x_max - x_min)
    fx(i) = fx(i) * 0.5d0 * (x_max - x_min)
    fy(i) = fy(i) * 0.5d0 * (x_max - x_min)
    fz(i) = fz(i) * 0.5d0 * (x_max - x_min)
 end do


end subroutine compute_spanwise_load_FS

!-------------------------------------------------------------------------------

subroutine compute_spanwise_load_MM(N, t_val, fz, my)
  use finite_elements
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  use my_funcs
  implicit none

 !=======================================================================
 !                                 Inputs
 !=======================================================================
  double precision, intent(IN)                   :: t_val

  integer, intent(IN)                            :: N

 !=======================================================================
 !                                 Outputs
 !=======================================================================
  double precision, intent(OUT)                  :: my(N), fz(N)

 !=======================================================================
 !                                 Local Variables
 !=======================================================================
  integer                                        :: i, element_num

  double precision                               :: x_min, x_max, x_domain(N), r1, &
                                                    t1, w_ddash, w_dddash, &
                                                    q_vec(nodes, dof_per_element), l



  q_input = 10.0d0
  q_output = 0.0d0
  if ( aerodynamic_model == 2 ) then
    do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
      q_input = q_output
      call aero_code()
    end do
  else if (aerodynamic_model == 1) then
    q_input = q_output
    call aero_code()
  else
    print *, 'No Such Aerodynamic Model exists!!!'
  end if

   q_input = q_output


   x_min = offset*R/2
   x_max = R/2
   l = R/ne


   call linspace(x_min, x_max, N, x_domain)

   !=======================================================================
   !                      Deflection Method
   !=======================================================================

    do i = 1, N, 1
      element_num = ((x_domain(i) - offset - mod(x_domain(i)-offset, l))/l) + 1
      call find_location(x_domain(i), t_val, q_output, element_num, q_vec, r1, t1)
      call compute_w_ddash(x_domain(i), t_val, q_vec, r1, t1, w_ddash, w_dddash)
      fz(i) = EI * w_dddash;
      my(i) = EI * w_ddash;
    end do


end subroutine compute_spanwise_load_MM

!-------------------------------------------------------------------------------


subroutine compute_w_ddash(x_val, t_val, q_vec, r1, t1, w_ddash, w_dddash)
  use hermite
  use lagrangian
  use blade_parameters
  implicit none


  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                   :: x_val, t_val, q_vec(nodes, dof_per_element), r1, t1

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: w_ddash, w_dddash


  !=======================================================================
  !                             Local Variables
  !=======================================================================
  integer                                        :: i

  double precision                               :: l, dt, h_temp(1, dof_per_element), &
                                                    ht, h_func(dof_per_element)


  !=======================================================================
  !                             Main Code
  !=======================================================================


  l = R*(1 - offset)/dble(ne)
  dt = T/nt

  call Hermite_Second_Der_Funcs(x_val-r1, l, 1, h_func)

  h_temp(1, :) = h_func
  w_ddash = 0.0d0
  do i = 1, nodes, 1
    call lagrangian_time(t_val, t1, i, dt, ht)
    w_ddash = w_ddash + dot_product(h_func, q_vec(i, :)) * ht
  end do

  call Hermite_Third_Der_Funcs(x_val-r1, l, 1, h_func)

  h_temp(1, :) = h_func
  w_dddash = 0.0d0
  do i = 1, nodes, 1
    call lagrangian_time(t_val, t1, i, dt, ht)
    w_dddash = w_dddash + dot_product(h_func, q_vec(i, :)) * ht
  end do


end subroutine compute_w_ddash

!-------------------------------------------------------------------------------
