
module finite_elements
    
    double precision, allocatable :: M_bar(:, :), K_bar(:, :), Modes_bar(:, :), r_left(:), r_right(:), &
                                     t_left(:), t_right(:), M_bar1(:, :), K_bar1(:, :), Modes_bar1(:, :)

    integer, allocatable  :: b(:), element_dofs(:, :), element_nodes(:,:)

    double precision, allocatable :: q_input(:,:), q_output(:,:), q_output_mr(:,:), q_ddot(:,:)

end module finite_elements


module trim_parameters

    double precision, allocatable :: trim_vec(:)

    double precision :: mu, lambda_vec(50,37)

end module trim_parameters


module trim_residuals

    double precision :: C_H, C_Y, C_T, C_MX, C_MY, C_MZ

end module trim_residuals


module loads

    integer, parameter :: n_azim = 361

    double precision   :: fxa(n_azim), fya(n_azim), fza(n_azim), &
                        fxi(n_azim), fyi(n_azim), fzi(n_azim), &
                        fx(n_azim), fy(n_azim), fz(n_azim), &
                        mxa(n_azim), mya(n_azim), mza(n_azim), &
                        mxi(n_azim), myi(n_azim), mzi(n_azim), &
                        mx(n_azim),  my(n_azim), mz(n_azim)

    double precision :: F_XH(n_azim), F_YH(n_azim), F_ZH(n_azim), &
                        M_XH(n_azim), M_YH(n_azim), M_ZH(n_azim), time(n_azim)


end module loads

