!=======================================================================
!> This subroutine implements the FET to solve for the aerodynamic
!> response of a rotor. In this subroutine, basically we compute
!> the A matrix, Q matrix and the solution vector q.
!                           ~             ~        ~
!                                     A q_output = Q
!                           ~             ~        ~
!> It outputs only the q vector as A and Q are no longer needed.
!> The outputs are stored in the module, so that
!=======================================================================

subroutine aero_code()
  use trim_parameters
  use finite_elements
  use aerodynamic_parameters
  use blade_parameters
  implicit none

  !=======================================================================
  !                          Local variables
  !=======================================================================
  double precision                               :: l, dt, t1, t2, t_val, t_domain(np)

  integer                                        :: i, j, p, k, info, signed

  integer                                        :: ipiv(num_modes*nt*(nodes-1))


  double precision, allocatable, dimension(:,:)  :: M_new, K_new, C_new, F_new


  double precision                               :: A(nt*(nodes-1)*num_modes, nt*(nodes-1)*num_modes), &
                                                    A1(num_modes*nodes, num_modes*nodes), &
                                                    A2(num_modes*nodes, num_modes*nodes), &
                                                    A3(num_modes*nodes, num_modes*nodes), &
                                                    A_temp(num_modes*nodes, num_modes*nodes)

  double precision                               :: Q(nt*(nodes-1)*num_modes, 1), &
                                                    q_temp(num_modes*nodes, 1), &
                                                    qn(num_modes*nt*(nodes-1), 1)


  double precision                               :: func1(num_modes*nodes, num_modes*nodes), &
                                                    func2(num_modes*nodes, num_modes*nodes), &
                                                    func3(num_modes*nodes, num_modes*nodes), &
                                                    func4(num_modes*nodes, 1)


  double precision                               :: K_AERO(2*ne+2, 2*ne+2), C_AERO(2*ne+2, 2*ne+2), &
                                                    force_vector(2*ne+2, 1), f1_aero(dof_per_element, 1), &
                                                    k1_aero(dof_per_element, dof_per_element), &
                                                    c1_aero(dof_per_element, dof_per_element)


  double precision                               :: h_stiff(num_modes, num_modes*nodes), &
                                                    h_dot(num_modes, num_modes*nodes)


  double precision                               :: I1(num_modes, num_modes), &
                                                    temp1(max_modes, max_modes), &
                                                    temp2(max_modes, max_modes), &
                                                    temp3(max_modes, 1)

  !=======================================================================
  !                        Blade Parmeters (Read from module)
  !=======================================================================

  l                    = R*(1.0d0 - offset)/dble(ne)
  dt                   = T / nt


  !=======================================================================
  !                         Elemental Degree of Freedom
  !=======================================================================
  if (.not. allocated(element_dofs)) then
    allocate(element_dofs(ne, dof_per_element))

    do i = 1, ne, 1
      p = 2*i - 1
      do j = 1, dof_per_element, 1
        element_dofs(i,j) = p
        p = p + 1
      end do
    end do

    element_dofs(ne/2 +1,1) = 1

    element_dofs(ne/2 +1, 2) = 2
  end if

  !=======================================================================
  !                         Elemental coordinates
  !=======================================================================
  if (.not. allocated(r_left)) then
    allocate(r_left(ne))
    allocate(r_right(ne))


    do i = 1, ne/2, 1
      r_left(i)  = (i-1)*l + offset*R
      r_right(i) = i*l + offset*R
    end do

    r_left(ne/2 +1:ne) = r_left(1:ne/2)

    r_right(ne/2 +1:ne) = r_right(1:ne/2)

  end if


  !=======================================================================
  !                         Rotor Boundary Conditions
  !=======================================================================

  if (.not. allocated(b)) then
    allocate(b(max_modes))

    do i = 2, 2*ne+2, 1
      b(i-1) = i
    end do

  end if


  !=======================================================================
  !                        Time Elemental coordinates
  !=======================================================================
  if (.not. allocated(t_left)) allocate(t_left(nt))
  if (.not. allocated(t_right)) allocate(t_right(nt))

  do i = 1, nt, 1
    t_left(i)  = (i-1)*dt
    t_right(i) = i*dt
  end do


  !=======================================================================
  !                         Elemental Nodes
  !=======================================================================


  if (.not. allocated(element_nodes)) allocate(element_nodes(nt, nodes*num_modes))

  p = num_modes + 1
  do i = 1, nt, 1
    p = p - num_modes
    do j = 1, nodes*num_modes, 1
      element_nodes(i,j) = p
      p = p + 1
    end do
  end do

  do i = 1, num_modes, 1
    element_nodes(nt, nodes*num_modes+i-num_modes) = i
  end do

  !=======================================================================
  !                        Allocation of Variables
  !=======================================================================
  allocate(M_new(num_modes, num_modes))
  allocate(K_new(num_modes, num_modes))
  allocate(C_new(num_modes, num_modes))
  allocate(F_new(num_modes, 1))

  !=======================================================================
  !                       Computing Stuctural Matrices
  !=======================================================================
  call main_fem()


  !=======================================================================
  !                        Initialization of variables
  !=======================================================================
  A = 0.0d0
  Q = 0.0d0

  !=======================================================================
  !                        Loop over time elements, nt
  !=======================================================================
  do j = 1, nt, 1
    t1 = t_left(j)
    t2 = t_right(j)
    t_domain = 0.5d0*((t2 + t1) + l_nodes*(t2 - t1))
    A1 = 0.0d0
    A2 = 0.0d0
    A3 = 0.0d0
    q_temp = 0.0d0
    !=======================================================================
    !                        Loop over gaussian quadrature, np
    !=======================================================================
    do k = 1, np, 1
      t_val = t_domain(k)
      K_AERO = 0.0d0
      C_AERO = 0.0d0
      force_vector = 0.0d0
      !=======================================================================
      !                        Loop over space elements, ne
      !=======================================================================
      do i = 1, ne, 1

        if (i <= ne/2) then
          signed = 1
        else
          signed = -1
        end if


        call compute_aero_stiffness(t_val, r_left(i), r_right(i), l, i, signed, k1_aero)

        call compute_aero_damping(t_val, r_left(i), r_right(i), l, i, signed, c1_aero)
        call compute_force_vector(t_val, r_left(i), r_right(i), l, i, signed, f1_aero)

        K_AERO(element_dofs(i,:), element_dofs(i,:)) = K_AERO(element_dofs(i,:), element_dofs(i,:)) + k1_aero
        C_AERO(element_dofs(i,:), element_dofs(i,:)) = C_AERO(element_dofs(i,:), element_dofs(i,:)) + c1_aero
        force_vector(element_dofs(i,:), 1) = force_vector(element_dofs(i,:), 1) + f1_aero(:,1)
      end do



      if ( flag_modal_reduction == 0 ) then
        M_new = M_bar1
        K_new = K_bar1 + K_AERO(b, b)
        C_new = C_AERO(b,b)
        F_new(:,1) = force_vector(b,1)
      else if (flag_modal_reduction == 1) then
        M_new = M_bar
        K_new = K_bar + MATMUL(transpose(Modes_bar), MATMUL(K_AERO(b,b), Modes_bar))
        C_new = MATMUL(transpose(Modes_bar), MATMUL(C_AERO(b,b), Modes_bar))
        F_new(:,1) = MATMUL(transpose(Modes_bar), force_vector(b,1))
      end if

      !=======================================================================
      !                 Compute Time Shape functions matrix
      !=======================================================================
      call compute_h(t_domain(k), t1, dt, 1, h_dot)
      call compute_h(t_domain(k), t1, dt, 2, h_stiff)

      func1 = MATMUL(MATMUL(transpose(h_dot), M_new), h_dot)
      A1 = A1 + l_weights(k) * func1

      func2 = MATMUL(MATMUL(transpose(h_stiff), K_new), h_stiff)
      A2 = A2 + l_weights(k) * func2

      func3 = MATMUL(MATMUL(transpose(h_stiff), C_new), h_dot)
      A3 = A3 + l_weights(k) * func3

      func4 = MATMUL(transpose(h_stiff), F_new)
      q_temp = q_temp + l_weights(k) * func4

      A_temp = -A1 + A2 + A3
    end do

    A_temp = A_temp * 0.5d0 * dt
    q_temp = q_temp * 0.5d0 * dt





    A(element_nodes(j,:), element_nodes(j,:)) = A(element_nodes(j,:), element_nodes(j,:)) + A_temp
    Q(element_nodes(j,:), 1) = Q(element_nodes(j,:), 1) + q_temp(:, 1)
  end do

  !=======================================================================
  !                        Solving the system, Aq = Q
  !=======================================================================

  if ( flag_modal_reduction == 0 ) then
    qn(:,1) = Q(:,1)
    call DGESV(num_modes*nt*(nodes-1), 1, A, num_modes*nt*(nodes-1), ipiv, qn, num_modes*nt*(nodes-1), info)
    q_output_mr(:,1) = qn(:,1)
    q_output(1:num_modes*nt*(nodes-1),1) = qn(1:num_modes*nt*(nodes-1), 1)
  else if (flag_modal_reduction == 1) then
    qn(:,1) = Q(:,1)
    call DGESV(num_modes*nt*(nodes-1), 1, A, num_modes*nt*(nodes-1), ipiv, qn, num_modes*nt*(nodes-1), info)
    !=======================================================================
    !         Transforming Modally reduced vector to original form
    !=======================================================================
    do i = 1, nt*(nodes-1), 1
      q_output(max_modes*(i-1)+1 : max_modes*i, 1) = MATMUL(Modes_bar, qn(num_modes*(i-1)+1 : num_modes*i, 1))
    end do
    q_output_mr(:,1) = qn(:,1)
  end if


end subroutine aero_code


!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the  time shape function
!> matrix.
!                           ~   ~   ~
!                              h_t
!                           ~   ~   ~
!=======================================================================

subroutine compute_h(t_val, t1, dt, flag, h)
  use my_funcs
  use blade_parameters
  use lagrangian
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: t_val, t1, dt

  integer, intent(IN)                            :: flag

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: h(num_modes,num_modes*nodes)

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  double precision                               :: I(num_modes, num_modes), h1

  integer                                        :: j


  !=======================================================================
  !                             Main Code
  !=======================================================================

  if ( flag == 1 ) then

    !=======================================================================
    ! computing the H matrix(time), flag=1 -----> 'mass', H_dot
    !=======================================================================

    call eye(num_modes, I)

    do j = 1, nodes, 1
      call lagrangian_first_der(t_val, t1, j, dt, h1)
      h(:, num_modes*(j-1)+1:num_modes*j) = h1*I
    end do

  else if (flag == 2) then

    !=======================================================================
    ! computing the H matrix(time), flag=2 -----> 'stiffness', H
    !=======================================================================
    call eye(num_modes, I)

    do j = 1, nodes, 1
      call lagrangian_time(t_val, t1, j, dt, h1)
      h(:, num_modes*(j-1)+1:num_modes*j) = h1*I
    end do

  end if


end subroutine compute_h

!-------------------------------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the 4x4 aero stiffness matrix
!> for a given trim parameters. 4x4 corresponds to an element
!> at a azimuth location.
!                           ~   ~   ~
!                               k1_aero
!                           ~   ~   ~
!=======================================================================

subroutine compute_aero_stiffness(t_val, r1, r2, l, element_num, signed, k1_aero)
  use trim_parameters
  use aerodynamic_parameters
  use hermite
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: t_val, r1, r2, l

  integer, intent(IN)                            :: signed, element_num

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: k1_aero(dof_per_element, dof_per_element)

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  integer                                        :: i, sign1

  double precision                               :: aero_force, aero_force_NL, t_val1

  double precision                               :: x_domain(np), h_func(dof_per_element), &
                                                    h_dash(dof_per_element)

  double precision                               :: integral(dof_per_element, dof_per_element), &
                                                    integral1(dof_per_element, dof_per_element)

  double precision                               :: h_temp(1, dof_per_element), &
                                                    h_dash_temp(1, dof_per_element)


  !=======================================================================
  !                             Main Code
  !=======================================================================


  x_domain = 0.5d0 * ((r2 + r1) + l_nodes*(r2 - r1))

  integral = 0.0d0
  sign1 = 1



  do i = 1, np, 1
    call Hermite_Funcs(x_domain(i)-r1, l, sign1, h_func)

    if (signed < 0) then
      t_val1 = t_val + pi
      sign1 = 1
      if (t_val1 > 2.0d0*pi) then
        t_val1 = t_val1 - 2.0d0*pi
      end if
    end if

    if (signed > 0) then
      t_val1 = t_val
    end if


    call compute_aero_force(x_domain(i), t_val1, 1, aero_force)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 1, aero_force_NL)


    h_temp(1, :) = h_func
    integral = integral + l_weights(i) * (aero_force + aero_force_NL) * MATMUL(transpose(h_temp), h_temp)


  end do

  integral = integral * 0.5d0 * (r2 - r1)




  integral1 = 0.0d0

  do i = 1, np, 1


    call Hermite_Funcs(x_domain(i) - r1, l, sign1, h_func)
    h_temp(1, :) = h_func
    call Hermite_First_Der_Funcs(x_domain(i)-r1, l, sign1, h_dash)
    h_dash_temp(1, :) = h_dash



    if (signed < 0) then
      t_val1 = t_val + pi
      sign1 = 1
      if (t_val1 > 2.0d0*pi) then
        t_val1 = t_val1 - 2.0d0*pi
      end if
    end if



    if (signed > 0) then
      t_val1 = t_val
    end if


    call compute_aero_force(x_domain(i), t_val1, 2, aero_force)

    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 2, aero_force_NL)


    integral1 = integral1 + l_weights(i) * (aero_force + aero_force_NL) * MATMUL(transpose(h_temp), h_dash_temp)


  end do


  integral1 = integral1 * 0.5d0 * (r2 - r1)

  k1_aero = -integral - integral1


end subroutine compute_aero_stiffness


!---------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the 4x4 aero damping matrix
!> for a given trim parameters. 4x4 corresponds to an element
!> at a azimuth location.
!                           ~   ~   ~
!                             c1_aero
!                           ~   ~   ~
!=======================================================================

subroutine compute_aero_damping(t_val, r1, r2, l, element_num, signed, c1_aero)
  use trim_parameters
  use aerodynamic_parameters
  use hermite
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: t_val, r1, r2, l

  integer, intent(IN)                            :: signed, element_num

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: c1_aero(dof_per_element, dof_per_element)

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  integer                                        :: i, sign1

  double precision                               :: aero_force, aero_force_NL, t_val1

  double precision                               :: x_domain(np), h_func(dof_per_element), &
                                                    h_dash(dof_per_element)

  double precision                               :: integral(dof_per_element, dof_per_element), &
                                                    integral1(dof_per_element, dof_per_element)

  double precision                               :: h_temp(1, dof_per_element), &
                                                    h_dash_temp(1, dof_per_element)

  !=======================================================================
  !                             Main Code
  !=======================================================================

  x_domain = 0.5d0 * ((r2 + r1) + l_nodes*(r2 - r1))

  integral = 0.0d0
  sign1 = 1

  do i = 1, np, 1
    call Hermite_Funcs(x_domain(i)-r1, l, sign1, h_func)

    if (signed < 0) then
      t_val1 = t_val + pi
      sign1 = 1
      if (t_val1 > 2.0d0*pi) then
        t_val1 = t_val1 - 2.0d0*pi
      end if
    end if


    if (signed > 0) then
      t_val1 = t_val
    end if

    call compute_aero_force(x_domain(i), t_val1, 3, aero_force)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 3, aero_force_NL)
    h_temp(1, :) = h_func
    integral = integral + l_weights(i) * (aero_force+aero_force_NL) * MATMUL(transpose(h_temp), h_temp)

  end do

  integral = integral * 0.5d0 * (r2 - r1)


  integral1 = 0.0d0

  do i = 1, np, 1
    call Hermite_Funcs(x_domain(i) - r1, l, sign1, h_func)
    h_temp(1, :) = h_func
    call Hermite_First_Der_Funcs(x_domain(i)-r1, l, sign1, h_dash)
    h_dash_temp(1, :) = h_dash

    if (signed < 0) then
      t_val1 = t_val + pi
      sign1 = 1
      if (t_val1 > 2.0d0*pi) then
        t_val1 = t_val1 - 2.0d0*pi
      end if
    end if

    if (signed > 0) then
      t_val1 = t_val
    end if

    call compute_aero_force(x_domain(i), t_val1, 4, aero_force)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 4, aero_force_NL)
    integral1 = integral1 + l_weights(i) * (aero_force+aero_force_NL) * MATMUL(transpose(h_temp), h_dash_temp)

  end do

  integral1 = integral1 * 0.5d0 * (r2 - r1)

  c1_aero = -integral - integral1

end subroutine compute_aero_damping

!------------------------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the 4x1 aero force vector
!> for a given trim parameters. 4x1 corresponds to an element
!> at a azimuth location.
!                           ~   ~   ~
!                            f1_aero
!                           ~   ~   ~
!=======================================================================

subroutine compute_force_vector(t_val, ri, r2, l, element_num, signed, f1_aero)
  use finite_elements
  use trim_parameters
  use aerodynamic_parameters
  use hermite
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: t_val, ri, r2, l

  integer, intent(IN)                            :: signed, element_num

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: f1_aero(dof_per_element, 1)

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  integer                                        :: i, sign1

  double precision                               :: aero_force, af0, af1, &
                                                    af2, af3, af4, w, w_dash, &
                                                    w_dot, w_dot_dash, t1, r1, t_val1

  double precision                               :: x_domain(np), h_func(dof_per_element)

  double precision                               :: integral(dof_per_element, 1), &
                                                    h_temp(1, dof_per_element), &
                                                    q_vec(nodes, dof_per_element)

  !=======================================================================
  !                             Main Code
  !=======================================================================


  x_domain = 0.5d0 * ((r2 + ri) + l_nodes*(r2 - ri))


  integral = 0.0d0
  sign1 = 1



  do i = 1, np, 1

    if (signed < 0) then
      t_val1 = t_val + pi
      sign1 = 1
      if (t_val1 > 2.0d0*pi) then
        t_val1 = t_val1 - 2.0d0*pi
      end if
    end if

    if (signed > 0) then
      t_val1 = t_val
    end if

    call Hermite_Funcs(x_domain(i)-ri, l, sign1, h_func)
    h_temp(1, :) = h_func


    call find_location(x_domain(i), t_val1, q_input, element_num, q_vec, r1, t1)


    call compute_w(x_domain(i), t_val1, q_vec, r1, t1, signed, w, w_dash, w_dot, w_dot_dash)

    call compute_aero_force(x_domain(i), t_val1, 0, aero_force)

    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 0, af0)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num ,1, af1)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 2, af2)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 3, af3)
    call compute_non_linear_aero(x_domain(i), t_val1, element_num, 4, af4)


    integral = integral + l_weights(i) * (aero_force + af0 - w * af1 - w_dash * af2 &
                                          - w_dot * af3 - w_dot_dash * af4) *  transpose(h_temp)


  end do

  integral = integral * 0.5d0 * (r2 - ri)
  f1_aero = integral

end subroutine compute_force_vector

!-------------------------------------------------------------------------------
