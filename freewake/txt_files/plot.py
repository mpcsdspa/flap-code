import numpy as np
import matplotlib.pyplot as plt

############################# GAMMA #############################


phi = np.loadtxt('gamma')

r = np.linspace(0, 1.0, len(phi[0]))


plt.figure(figsize=(10,8))
plt.plot(r, phi[0], 'b-o', linewidth=2.0)
plt.title('Bound Circulation Distribution over span', fontsize=20)
plt.xlabel('x/R', fontsize =16)
plt.ylabel('Bound Circulation', fontsize = 16)
plt.grid()
plt.savefig('../figures/gamma.png')

############################# AOA #############################

phi = np.loadtxt('angle_of_attack')

r = np.linspace(0, 1.0, len(phi[0]))


plt.figure(figsize=(10,8))
plt.plot(r, phi[0], 'b-o',  linewidth=2.0)
plt.title('Angle of attack Distribution over span', fontsize=20)
plt.xlabel('x/R', fontsize =16)
plt.ylabel('Angle of Attack (deg)', fontsize = 16)
plt.grid()
plt.savefig('../figures/aoa.png')


############################# INFLOW #############################


phi = np.loadtxt('inflow')

r = np.linspace(0, 1.0, len(phi[0]))


plt.figure(figsize=(10,8))
plt.plot(r, phi[0],  'b-o', linewidth=2.0)
plt.title('Inflow Distribution over span', fontsize=20)
plt.xlabel('x/R', fontsize =16)
plt.ylabel('Inflow (m/s)', fontsize = 16)
plt.grid()
plt.savefig('../figures/inflow.png')

############################# CL #############################

phi = np.loadtxt('lift_coeff')

r = np.linspace(0, 1.0, len(phi[0]))


plt.figure(figsize=(10,8))
plt.plot(r, phi[0],  'b-o', linewidth=2.0)
plt.title('Lift Coefficient Distribution over span', fontsize=20)
plt.xlabel('x/R', fontsize =16)
plt.ylabel('Cl', fontsize = 16)
plt.grid()
plt.savefig('../figures/lift_coeff.png')
