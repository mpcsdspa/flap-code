!=======================================================================
!> This module contains some of the aerodynamic parametes used for trim..
!                           ~         ~        ~
!                           Aerodynamic Parmeters
!                           ~         ~        ~
!=======================================================================
module aerodynamic_parameters
  implicit none

 !=======================================================================
 !                          Aerodynamic Parameters
 !======================================================================

  ! Zero Angle of Attack Lift Coefficient, Cl_o
     real(kind=8), parameter :: Cl_o = 0.0d0
  ! Lift Curve Slope, Cl_alpha
     real(kind=8), parameter :: Cl_alpha = 5.7d0
  ! Constant Drag Coefficient, Cdo
     real(kind=8), parameter :: Cdo = 0.01d0
  ! Linear component of Drag Coefficient, Cd1
     real(kind=8), parameter :: Cd1 = 0.0d0
  ! Quadratic component of Drag Coefficient, Cd2
     real(kind=8), parameter :: Cd2 = 0.0d0
  ! Moment Coefficient, Cmac
     real(kind=8), parameter :: Cmac = 0.0d0
  ! Linear component of Moment Coefficient, f1
     real(kind=8), parameter :: f1 = 0.0d0
  ! Lock Number, Gamma
     real(kind=8), parameter :: Lock_no = 8.0d0
  ! Precone Angle, beta_p
     real(kind=8), parameter :: beta_p = 0.0d0
  ! Center of Lift from elastic axis, e_d/R
     real(kind=8), parameter :: e_d = 0.0d0
  ! Center of gravity from elastic axis, e_g/R
     real(kind=8), parameter :: e_g = 0.0d0
  ! Center of gravity location, xcg, ycg, hcg
     real(kind=8), parameter :: xcg = 0.0d0
     real(kind=8), parameter :: ycg = 0.0d0
     real(kind=8), parameter :: hcg = 0.2d0
  ! Weight Coefficient, C_W
     real(kind=8), parameter :: C_W = 0.00595d0
  ! Fuselage Moment coefficients
     real(kind=8), parameter :: C_mx_f = 0.0d0
     real(kind=8), parameter :: C_my_f = 0.0d0
     real(kind=8), parameter :: C_mz_f = 0.0d0
  ! X,Y,Z locations
     real(kind=8), parameter :: X_ac = 0.0d0
     real(kind=8), parameter :: Y_ac = 0.0d0
     real(kind=8), parameter :: H_ac = 0.0d0
  ! Equivalent flat plate aera, f_d
     real(kind=8), parameter :: f_d = 0.0d0
  ! Flight Path Angle
     real(kind=8), parameter :: theta_fp = 0.0d0
     real(kind=8), parameter :: beta_1c = 0.0d0
  ! Correction Factor, 1.15 - Hover, 1.0 - FF
     real(kind=8), parameter :: k_f = 1.0d0
  ! Number of Trim variables
     integer, parameter :: n_trim = 6

end module aerodynamic_parameters
