! program test_aero
!   use trim_residuals
!   use trim_parameters
!   use finite_elements
!   use loads
!   use aerodynamic_parameters
!   use blade_parameters
!   use my_funcs
!   implicit none
!
!   integer :: i, nmax
!   double precision, dimension(4,4) :: ktemp, I1
!   !double precision, allocatable, dimension(:,:) :: I1
!   double precision, dimension(4,1) :: ftemp
!   double precision, dimension(max_modes*(nodes-1)*nt,1) :: q, qi
!   double precision, dimension(num_modes*(nodes-1)*nt,1) :: q_mr
!   double precision :: aero_force, qtip(nt*(nodes-1), 1), r_vec(100), phi_vec(100)
!
!   ! double precision :: fxa(n_azim), fya(n_azim), fza(n_azim), &
!   !                     fxi(n_azim), fyi(n_azim), fzi(n_azim), &
!   !                     fx(n_azim), fy(n_azim), fz(n_azim), &
!   !                     mxa(n_azim), mya(n_azim), mza(n_azim), &
!   !                     mxi(n_azim), myi(n_azim), mzi(n_azim), &
!   !                     mx(n_azim), my(n_azim), mz(n_azim), time(n_azim)
!
!   ! double precision :: F_XH(n_azim), F_YH(n_azim), F_ZH(n_azim), &
!   !                     M_XH(n_azim), M_YH(n_azim), M_ZH(n_azim), tv(6)
!
!   double precision :: fx_fs(100), fy_fs(100), fz_fs(100), mx_fs(100), my_fs(100), mz_fs(100), &
!                       fz_mm(100), my_mm(100)
!
!
!   ! double precision :: M_bar(num_modes, num_modes), K_bar(num_modes, num_modes), Modes_bar(max_modes, num_modes)
!
!
!   double precision :: seconds, residual(n_trim), r1, t1, q_vec(6,4)
!   integer :: a0, b0, c0, a1, b1, c1
!
!
!
!   !
!   !
!   ! if (.not. allocated(q_input)) allocate(q_input(max_modes*(nodes-1)*nt, 1))
!   ! if (.not. allocated(q_output)) allocate(q_output(max_modes*(nodes-1)*nt, 1))
!   ! if (.not. allocated(q_output_mr)) allocate(q_output_mr(num_modes*(nodes-1)*nt, 1))
!   !
!   ! if (.not. allocated(trim_vec)) allocate(trim_vec(n_trim))
!   !
!   ! trim_vec(1) = 0.0d0
!   ! trim_vec(2) = 0.0d0
!   ! trim_vec(3) = 6.35d0 * pi / 180.0d0
!   ! trim_vec(4) = 1.15d0 * pi / 180.0d0
!   ! trim_vec(5) = -4.72d0 * pi / 180.0d0
!   ! trim_vec(6) = 0.0110d0
!   !
!   !
!   ! mu = 0.3d0
!   ! call get_root_forces()
!   ! q_input = 0.0d0
!   ! q_output = 0.0d0
!   ! do i = 1, , 1
!   !   call aero_code()
!   ! end do
!
!   ! print *, q_output(1:10,1)
!   ! call main_fem()
!
!   ! =======================================================================
!   !                             Solve FET
!   ! =======================================================================
!   !   q_input = 10.0d0
!   !   q_output = 0.0d0
!   !   do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
!   !     q_input = q_output
!   !     call aero_code()
!   !     print *, q_output(10, 1)
!   !     ! stop
!   !   end do
!   !
!   ! call get_root_forces()
!   ! open(unit = 1, file='../txt_files/hingeless/force_x')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', fxa(i), ',', fxi(i), ',', fx(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/force_y')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', fya(i), ',', fyi(i), ',', fy(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/force_z')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', fza(i), ',', fzi(i), ',', fz(i)
!   ! end do
!   ! close(1)
!   !
!   ! call get_root_moments()
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/moment_x')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', mxa(i), ',', mxi(i), ',', mx(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/moment_y')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', mya(i), ',', myi(i), ',', my(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/moment_z')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) i, ',', mza(i), ',', mzi(i), ',', mz(i)
!   ! end do
!   ! close(1)
!   !
!   ! call get_hub_loads()
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/hub_forces')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) time(i), ',', F_XH(i), ',', F_YH(i), ',', F_ZH(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='../txt_files/hingeless/hub_moments')
!   ! do i = 1, n_azim, 1
!   !   write(1,*) time(i), ',', M_XH(i), ',', M_YH(i), ',', M_ZH(i)
!   ! end do
!   ! close(1)
!   !
!   !
!   ! stop
!
!   ! !=======================================================================
!   ! !                             Solve FET
!   ! !=======================================================================
!   !   q_input = 10.0d0
!   !   q_output = 0.0d0
!   !   do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
!   !     q_input = q_output
!   !     call aero_code()
!   !     print *, q_output(1:10, 1)
!   !   end do
!
!   !   q_input = q_output
!
!     ! if ( aerodynamic_model == 1 ) then
!     !   print *, "Writing to file"
!     !   open(unit = 1, file='./txt_files/hingeless/tip_disp_linear')
!     !   do i = 1, 50, 1
!     !     write(1,*) i, ',', q(max_modes*i-1,1)
!     !   end do
!     !   close(1)
!     ! else if (aerodynamic_model == 2) then
!     !   print *, "Writing to file"
!     !   open(unit = 1, file='./txt_files/hingeless/tip_disp_nonlinear')
!     !   do i = 1, 50, 1
!     !     write(1,*) i, ',', q_output(10 + (i-1)*max_modes,1)
!     !   end do
!     !   close(1)
!     ! end if
!
!
!     ! q_input = 0.0d0
!     ! call aero_code()
!     ! stop
!
!     ! call compute_force_vector(0.1d0, 0.0d0, 0.2d0, 0.2d0, 6, -1, ftemp)
!     ! ! print *, ftemp
!     ! call compute_aero_stiffness(0.0212d0, 0.2d0, 0.4d0, 0.2d0, 2, 1, ktemp)
!     ! print *, ktemp
!     ! stop "ff"
!
!
!
!   print *, '*******************************************************************'
!   print *, 'Coupled Trim for Teetering Rotor with 2 blades (Single Beam Method)'
!   print *, '*******************************************************************'
!
!   call system_clock(a0, b0, c0)
!   call trim()
!   call system_clock(a1, b1, c1)
!   print  *, '------------------------------------------------------------'
!   print  *, '------------------------------------------------------------'
!   print *, 'The time elapsed is', (a1-a0)/1000.0, 'seconds'
!   print  *, '------------------------------------------------------------'
!   print  *, '------------------------------------------------------------'
!
!
!   !stop
!
!
!  print *, C_H, C_Y, C_T, C_MX, C_MY, C_MZ
!
!
!   ! =======================================================================
!   !                Trimmed HUB LOADS
!   ! =======================================================================
!   call get_hub_loads()
!
!   F_XH = F_XH * m0_dim * omega_dim**2 * R_dim**2
!   F_YH = F_YH * m0_dim * omega_dim**2 * R_dim**2
!   F_ZH = F_ZH * m0_dim * omega_dim**2 * R_dim**2
!
!   M_XH = M_XH * m0_dim * omega_dim**2 * R_dim**3
!   M_YH = M_YH * m0_dim * omega_dim**2 * R_dim**3
!   M_ZH = M_ZH * m0_dim * omega_dim**2 * R_dim**3
!
!
!
!   open(unit = 1, file='../txt_files/hingeless/hub_forces')
!   do i = 1, n_azim, 1
!     write(1,*) time(i), ',', F_XH(i), ',', F_YH(i), ',', F_ZH(i)
!   end do
!   close(1)
!
!   open(unit = 1, file='../txt_files/hingeless/hub_moments')
!   do i = 1, n_azim, 1
!     write(1,*) time(i), ',', M_XH(i), ',', M_YH(i), ',', M_ZH(i)
!   end do
!   close(1)
!
! !   !!!!!!!!!!!!!!!!!!!!!!!!! TIP DISPLACEMENT MAIN PROGRAM !!!!!!!!!!!!!!!!!!!!!!
!
! !   if ( aerodynamic_model == 1 ) then
! !     nmax = 1
! !   else if (aerodynamic_model == 2) then
! !     nmax = 5
! !   end if
! !   !
! !   ! qi = 0.0d0
! !   ! do i = 1,nmax,1
! !   !   call aero_code()
! !   !   print *, '--------------------------------------------'
! !   !   print *, q(2*ne-1, 1)
! !   !   print *, '--------------------------------------------'
! !   !   qi = q
! !   ! end do
!
!
!   !=======================================================================
!   !                             Solve FET
!   !=======================================================================
!     q_input = 10.0d0
!     q_output = 0.0d0
!     do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
!       q_input = q_output
!       call aero_code()
!     end do
!
!     q_input = q_output
!
!     if ( aerodynamic_model == 1 ) then
!       print *, "Writing to file"
!       open(unit = 1, file='../txt_files/hingeless/tip_disp_linear')
!       do i = 1, 50, 1
!         write(1,*) i, ',', q(max_modes*i-1,1)
!       end do
!       close(1)
!     else if (aerodynamic_model == 2) then
!       print *, "Writing to file"
!       open(unit = 1, file='../txt_files/hingeless/tip_disp_nonlinear')
!       do i = 1, 50, 1
!         write(1,*) i, ',', q_output(10 + (i-1)*max_modes,1)
!       end do
!       close(1)
!     end if
!
!
! !!!!!!!!!!!!!!!! ROOT FORCES CALCULATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   call get_root_forces()
!
!   fxa = fxa * m0_dim * omega_dim**2 * R_dim**2
!   fya = fya * m0_dim * omega_dim**2 * R_dim**2
!   fza = fza * m0_dim * omega_dim**2 * R_dim**2
!   fxi = fxi * m0_dim * omega_dim**2 * R_dim**2
!   fyi = fyi * m0_dim * omega_dim**2 * R_dim**2
!   fzi = fzi * m0_dim * omega_dim**2 * R_dim**2
!   fx = fx * m0_dim * omega_dim**2 * R_dim**2
!   fy = fy * m0_dim * omega_dim**2 * R_dim**2
!   fz = fz * m0_dim * omega_dim**2 * R_dim**2
!
!
!   open(unit = 1, file='../txt_files/hingeless/force_x')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', fxa(i), ',', fxi(i), ',', fx(i)
!   end do
!   close(1)
!
!   open(unit = 1, file='../txt_files/hingeless/force_y')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', fya(i), ',', fyi(i), ',', fy(i)
!   end do
!   close(1)
!
!   open(unit = 1, file='../txt_files/hingeless/force_z')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', fza(i), ',', fzi(i), ',', fz(i)
!   end do
!   close(1)
!
!
!   !!!!!!!!!!!!!!!! ROOT MOMENTS CALCULATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   call get_root_moments()
!
!
!   mxa = mxa * m0_dim * omega_dim**2 * R_dim**3
!   mya = mya * m0_dim * omega_dim**2 * R_dim**3
!   mza = mza * m0_dim * omega_dim**2 * R_dim**3
!   mxi = mxi * m0_dim * omega_dim**2 * R_dim**3
!   myi = myi * m0_dim * omega_dim**2 * R_dim**3
!   mzi = mzi * m0_dim * omega_dim**2 * R_dim**3
!   mx = mx * m0_dim * omega_dim**2 * R_dim**3
!   my = my * m0_dim * omega_dim**2 * R_dim**3
!   mz = mz * m0_dim * omega_dim**2 * R_dim**3
!
!   do i = 1, n_azim, 1
!     my(i) = my(i) - offset*fz(i)
!     mz(i) =  mz(i) + offset * fy(i)
!   end do
!
!
!   open(unit = 1, file='../txt_files/hingeless/moment_x')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', mxa(i), ',', mxi(i), ',', mx(i)
!   end do
!   close(1)
!
!   open(unit = 1, file='../txt_files/hingeless/moment_y')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', mya(i), ',', myi(i), ',', my(i)
!   end do
!   close(1)
!
!   open(unit = 1, file='../txt_files/hingeless/moment_z')
!   do i = 1, n_azim, 1
!     write(1,*) i, ',', mza(i), ',', mzi(i), ',', mz(i)
!   end do
!   close(1)
!
! ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ! ! SPANWISE LOAD CALCULATION
!
! !   call compute_spanwise_load_MM(100, 0.0d0, fz_mm, my_mm)
!
! !   fz_mm = fz_mm * m0_dim * omega_dim**2 * R_dim**2
! !   my_mm = my_mm * m0_dim * omega_dim**2 * R_dim**3
!
! !   open(unit = 1, file='./txt_files/hingeless/spanwise_mm')
! !   do i = 1, 100, 1
! !     write(1,*) i, ',', fz_mm(i), ',', my_mm(i)
! !   end do
! !   close(1)
!
! !   call compute_spanwise_load_FS(100, 0.0d0, fx_fs, fy_fs, fz_fs, mx_fs, my_fs, mz_fs)
!
! !   fx_fs = fx_fs * m0_dim * omega_dim**2 * R_dim**2
! !   fy_fs = fy_fs * m0_dim * omega_dim**2 * R_dim**2
! !   fz_fs = fz_fs * m0_dim * omega_dim**2 * R_dim**2
!
! !   mx_fs = mx_fs * m0_dim * omega_dim**2 * R_dim**3
! !   my_fs = my_fs * m0_dim * omega_dim**2 * R_dim**3
! !   mz_fs = mz_fs * m0_dim * omega_dim**2 * R_dim**3
!
!
! !   open(unit = 1, file='./txt_files/hingeless/spanwise_fs')
! !   do i = 1, 100, 1
! !     write(1,*) i, ',', fx_fs(i), ',', fy_fs(i), ',', fz_fs(i), ',', mx_fs(i), ',', my_fs(i), ',', mz_fs(i)
! !   end do
! !   close(1)
!
! !   print *, C_H, C_Y, C_T, C_MX, C_MY, C_MZ
!
! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   ! ! !
!   ! if ( aerodynamic_model == 1 ) then
!   !   nmax = 1
!   ! else if (aerodynamic_model == 2) then
!   !   nmax = 5
!   ! end if
!   ! !
!   ! qi = 0.0d0
!   ! do i = 1,nmax,1
!   !   call aero_code(qi, q, q_mr)
!   !   print *, '--------------------------------------------'
!   !   print *, q(2*ne, 1)
!   !   print *, '--------------------------------------------'
!   !   qi = q
!   ! end do
!   !
!   ! qtip(:,1) = q(max_modes-1 : max_modes*(nodes-1)*nt : max_modes, 1)
!   !
!   ! if ( aerodynamic_model == 1 ) then
!   !   call compute_tip_disp(qtip, r_vec, phi_vec)
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_linear')
!   !   do i = 1, 100, 1
!   !     write(1,*) r_vec(i), ',', phi_vec(i)
!   !   end do
!   !   close(1)
!   ! else if (aerodynamic_model == 2) then
!   !   call compute_tip_disp(qtip, r_vec, phi_vec)
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_nonlinear')
!   !   do i = 1, 100, 1
!   !     write(1,*) r_vec(i), ',', phi_vec(i)
!   !   end do
!   !   close(1)
!   ! end if
!
!
!   !allocate(I1(4,4))
!   ! call eye(4,I)
!   ! !h = [2*I 2*I]
!   ! print *, h
!   !qi = 2.0d0
!   !call compute_non_linear_aero(0.1d0, 0.1d0, 4, qi, aero_force)
!   !print *, aero_force
!   ! call compute_h(0.1d0, 0.0d0, 0.2d0, 2, h)
!   ! print *, h(:,1)
!
!   ! qi = 0.0d0
!   ! call aero_code(qi, q, q_mr)
!   ! qi = q
!   ! print *, qi(1:5, 1)
!   ! print *, '--------------------------------------------'
!   ! call aero_code(qi,q)
!   ! print *, q(1:5, 1)
!
!   !qi = 1.0d0
!   !call compute_non_linear_aero(0.1d0, 0.1d0, 2, qi, aero_force)
!   !call compute_force_vector(5.75d0, 0.9d0, 1.0d0, 0.1d0, qi, ftemp)
!   !print *, ftemp
!   !call compute_aero_force(0.1d0, 0.1d0, 3, aero_force)
!   !print *, aero_force
!
!
!   ! print *, 1
!   ! call compute_force_vector(0.1d0, 0.0d0, 0.1d0, 0.1d0, qi, ftemp)
!   ! print *, ftemp
!   !
!   ! stop "message"
!   ! call aero_code(qi,q)
!   ! print *, q(1:5,1)
!   !
!
!   ! !!!!
!   ! call get_hub_loads(F_XH,F_YH,F_ZH,M_XH,M_YH,M_ZH)
!   ! open(unit = 1, file='./txt_files/hub_forces')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', F_XH(i), ',', F_YH(i), ',', F_ZH(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='./txt_files/hub_moments')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', M_XH(i), ',', M_YH(i), ',', M_ZH(i)
!   ! end do
!   ! close(1)
!   !
!   ! !!!!!!!!!!!!!!!! ROOT FORCES CALCULATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   ! call get_root_forces(fxa,fya,fza,fxi,fyi,fzi,fx,fy,fz)
!   ! open(unit = 1, file='./txt_files/force_x')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', fxa(i), ',', fxi(i), ',', fx(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='./txt_files/force_y')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', fya(i), ',', fyi(i), ',', fy(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='./txt_files/force_z')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', fza(i), ',', fzi(i), ',', fz(i)
!   ! end do
!   ! close(1)
!   !
!   !
!   ! !!!!!!!!!!!!!!!! ROOT MOMENTS CALCULATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   ! call get_root_moments(mxa,mya,mza,mxi,myi,mzi,mx,my,mz)
!   !
!   ! do i = 1, 400, 1
!   !   my(i) = my(i) - offset*fz(i)
!   !   mz(i) =  mz(i) + offset * fy(i)
!   ! end do
!   !
!   !
!   ! open(unit = 1, file='./txt_files/moment_x')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', mxa(i), ',', mxi(i), ',', mx(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='./txt_files/moment_y')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', mya(i), ',', myi(i), ',', my(i)
!   ! end do
!   ! close(1)
!   !
!   ! open(unit = 1, file='./txt_files/moment_z')
!   ! do i = 1, 400, 1
!   !   write(1,*) i, ',', mza(i), ',', mzi(i), ',', mz(i)
!   ! end do
!   ! close(1)
!
!   !!!!!!!!!!!!!!!!!!!!!!!!! TIP DISPLACEMENT MAIN PROGRAM !!!!!!!!!!!!!!!!!!!!!!
!   !
!   ! if ( aerodynamic_model == 1 ) then
!   !   nmax = 1
!   ! else if (aerodynamic_model == 2) then
!   !   nmax = 5
!   ! end if
!   ! !
!   ! qi = 0.0d0
!   ! do i = 1,nmax,1
!   !   call aero_code(qi, q, q_mr)
!   !   print *, '--------------------------------------------'
!   !   print *, q(2*ne-1, 1)
!   !   print *, '--------------------------------------------'
!   !   qi = q
!   ! end do
!   !
!   !
!   ! if ( aerodynamic_model == 1 ) then
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_linear')
!   !   do i = 1, 50, 1
!   !     write(1,*) i, ',', q(max_modes*i-1,1)
!   !   end do
!   !   close(1)
!   ! else if (aerodynamic_model == 2) then
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_nonlinear')
!   !   do i = 1, 50, 1
!   !     write(1,*) i, ',', q(max_modes*i-1,1)
!   !   end do
!   !   close(1)
!   ! end if
!
!   ! !print *, q(19:25:5,1)
!   !
!   ! qtip(:,1) = q(max_modes-1 : max_modes*(nodes-1)*nt : max_modes, 1)
!   !
!   ! !
!   ! !
!   ! if ( aerodynamic_model == 1 ) then
!   !   call compute_tip_disp(qtip, r_vec, phi_vec)
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_linear')
!   !   do i = 1, 100, 1
!   !     write(1,*) r_vec(i), ',', phi_vec(i)
!   !   end do
!   !   close(1)
!   ! else if (aerodynamic_model == 2) then
!   !   call compute_tip_disp(qtip, r_vec, phi_vec)
!   !   print *, "Writing to file"
!   !   open(unit = 1, file='./txt_files/tip_disp_nonlinear')
!   !   do i = 1, 100, 1
!   !     write(1,*) r_vec(i), ',', phi_vec(i)
!   !   end do
!   !   close(1)
!   ! end if
!   !
!   !!!!!!!!!!!!!!!!!!!!!!!!! TIP DISPLACEMENT MAIN PROGRAM !!!!!!!!!!!!!!!!!!!!!!
!
!   ! call compute_aero_damping(0.1d0, 0.0d0, 0.2d0, 0.2d0, ktemp)
!   ! call compute_force_vector(0.1d0, 0.0d0, 0.2d0, 0.2d0, ftemp)
! end program test_aero
!
!
! ! program mode_shape_plot
! !   use blade_parameters
! !   implicit none
! !
! !   double precision, allocatable, dimension(:)         :: r_left, r_right
! !   integer, allocatable, dimension(:,:)                :: element_dofs
! !   double precision, allocatable, dimension(:,:) :: M_bar, K_bar, Modes_bar
! !   double precision, allocatable, dimension(:) :: r_vec, phi, Freqs
! !   integer :: i, j, p, n_interior
! !   double precision :: l
! !
! !   l                    = R/dble(ne)
! !   n_interior = 51
! !
! !   allocate(r_vec(n_interior*ne))
! !   allocate(phi(n_interior*ne))
! !   allocate(M_bar(num_modes, num_modes))
! !   allocate(K_bar(num_modes, num_modes))
! !   allocate(Modes_bar(max_modes, num_modes))
! !   allocate(r_left(ne))
! !   allocate(r_right(ne))
! !   allocate(element_dofs(ne, dof_per_element))
! !   !=======================================================================
! !   !                         Elemental Degree of Freedom
! !   !=======================================================================
! !     do i = 1, ne, 1
! !       p = 2*i - 1
! !       do j = 1, dof_per_element, 1
! !         element_dofs(i,j) = p
! !         p = p + 1
! !       end do
! !     end do
! !
! !   !=======================================================================
! !   !                         Elemental coordinates
! !   !=======================================================================
! !
! !   do i = 1, ne, 1
! !     r_left(i)  = (i-1)*l
! !     r_right(i) = i*l
! !   end do
! !
! !   allocate(Freqs(max_modes))
! !
! !   call main_fem(0, M_bar, K_bar, Modes_bar)
! !   !call compute_modes(max_modes, M_bar, K_bar, Freqs, Modes_bar)
! !   !print *, Modes_bar
! !   call mode_shape(Modes_bar, r_left, r_right, element_dofs, 2, n_interior, r_vec, phi)
! !   !
! !   ! print *, size(r_vec)
! !   ! print *, size(phi)
! !   !print *, phi
! !   open(unit = 1, file='mode_shape')
! !   do i = 1, ne*n_interior, 1
! !     write(1,*) r_vec(i), ',', phi(i)
! !   end do
! !   close(1)
! !
! ! end program mode_shape_plot
