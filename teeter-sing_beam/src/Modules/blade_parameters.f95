!=======================================================================
!> This module contains some of the blade parametes used for trim..
!                           ~         ~        ~
!                              Blade Parmeters
!                           ~         ~        ~
!=======================================================================
module blade_parameters
implicit none

!=======================================================================
!                            Constants
!=======================================================================
! Value of Constant pi, pi
   real(kind=8), parameter :: pi = 4 * atan (1.0_8)
! Value of constant e, e
   real(kind=8), parameter :: e = 2.7182818285


 !=======================================================================
 !                          Dimensional Parameters
 !=======================================================================
 ! Radius of the rotor blade, R
    real(kind=8), parameter :: R_dim = 1.0d0
 ! Blade Stiffness, EI
    real(kind=8), parameter :: EI_dim = 0.0108d0
 ! Mass per length, m0
    real(kind=8), parameter :: m0_dim = 1.0d0
 ! Rotating Frequency (rad/s), omega
    real(kind=8), parameter :: omega_dim = 1.0d0
  ! Chord of the Blade, m
     real(kind=8), parameter :: chord_dim = 0.05d0
  ! Flapwise radius of gyration, Km1_dim
     real(kind=8), parameter :: Km1_dim = 0.014d0
  ! Chordwise radius of gyration, Km2_dim
     real(kind=8), parameter :: Km2_dim = 0.02d0
  ! Chord to Radius Ratio, chord2R
     real(kind=8), parameter :: chord2R = chord_dim/R_dim


!=======================================================================
!                          NonDimensional Parameters
!=======================================================================
 ! Chord of the airfoil, chord
   real(kind=8), parameter :: chord = chord2R
 ! AC to 3/4 chord offset
   real(kind=8), parameter :: neta_r = -0.5d0 * chord2R
 ! Flapwise radius of gyration, Km1
   real(kind=8), parameter :: Km1 = Km1_dim/R_dim
 ! Chordwise radius of gyration, Km2
   real(kind=8), parameter :: Km2 = Km2_dim/R_dim
   real(kind=8), parameter :: Km = SQRT(Km1*Km1 + Km2*Km2)
 ! Number of blades, Nb
   integer, parameter :: Nb = 2
 ! Solidity, sigma
   real(kind=8), parameter :: sigma = 0.085d0 !Nb*chord2R/pi


!=======================================================================
!                             Integers
!=======================================================================

! Number of spatial elements, ne
   integer, parameter :: ne = 20
! Number of modes, num_modes
   integer, parameter :: num_modes = 6
! Degrees of freedom per element, dof
   integer, parameter :: dof_per_element = 4
! Maximum Number of modes, 2*ne+1 - Teetering
   integer, parameter :: max_modes = 2*ne+1
! Flag for Modal Reduction, 1 - Yes, 0 - No
   integer, parameter :: flag_modal_reduction = 1
! Aerodynamic Model , 1 - Linear Aero,  2 - Nonlinear Aero
  integer, parameter :: aerodynamic_model = 2
! Number of Lagrangian nodes, nodes
  integer, parameter :: nodes = 6
! Number of time elements, nt
  integer, parameter :: nt = 10
! Number of Gaussian integration nodes
  integer, parameter :: np = 6
! Total time period, T
  real(kind = 8), parameter :: T = 2*pi

 !=======================================================================
 !                             Real numbers
 !=======================================================================


! Radius of the rotor blade, R
   real(kind=8), parameter :: R = 2*R_dim/R_dim
! Blade Stiffness, EI
   real(kind=8), parameter :: EI = EI_dim/(m0_dim*omega_dim*omega_dim*R_dim**4)
! Mass per length, m0
   real(kind=8), parameter :: m0 = m0_dim/m0_dim
! Rotating Frequency (rad/s), omega
   real(kind=8), parameter :: omega = omega_dim/omega_dim
! Hinge offset from the hub (non-dimensional wrt R), 0.04 - Articulated
! 0.0 - Hingeless
   real(kind=8), parameter :: offset = 0.0d0 !(Percent with Radius)

 !=======================================================================
 !                  Gaussian Quadrature(Hard Coded) - Not happy!
 !=======================================================================

   real(kind=8), parameter :: l_nodes(np) = [-0.932469d0, -0.6612093d0, -0.23861918d0, 0.23861918d0, 0.6612093d0, 0.932469d0]
   real(kind=8), parameter :: l_weights(np) = [0.1713244d0, 0.36076157d0, 0.4679139d0, 0.4679139d0, 0.36076157d0, 0.1713244d0]


!=======================================================================
!                          FreeWake Parameters
!=======================================================================
 integer, parameter :: adim = 37
 integer, parameter :: NSEG = 50



end module blade_parameters
