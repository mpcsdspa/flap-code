\section{Aerodynamic Modeling}

The rotor response is directly dependent on the aerodynamic forces on the individual blades. We have assumed quasi-steady aerodynamics. The aerodynamic forces and moments on a blade are calculated using the Blade element theory.

\subsection{Aerodynamic Reference Frame}
Blade Element Momentum Theory (BEMT) uses aerodynamic velocities and forces aligned with the rotating hub coordinate system. However this analysis uses aerodynamic velocities and forces aligned in the deformed blade coordinate system. The primary advantage is that, for BEMT coordinates shown on the left in the below figure, there are three critical angles that must be computed while the deformed blade coordinate system shown on the right in the below figure, requires only two angles. The inflow angle $\phi$ has been eliminated from the process. This process allows for a more natural implementation of standard aerodynamic expressions and is easier to implement more advanced aerodynamic formulations. Keep in mind that it does come at the cost of more complex blade velocity expressions and the
inclusion of the $T_{DU}$ matrix to transform the loads back into the undeformed blade frame.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{ref_frame.png}
	\caption{Cross sectional coordinate systems. Classical BEMT blade section (left) and chord aligned cross section used in this analysis (right)}
\end{figure}
 

\subsection{Aerodynamic Velocities}
It is necessary to compute the expressions of aerodynamic velocities before we compute the aerodynamic forces on the blade. The incident velocity at a particular blade section has contributions from both airow and the blade velocity. The general expression for the velocity in the rotating undeformed frame is given by:
\begin{equation}
\vec{V} = \vec{V_p} - \vec{V_w}
\end{equation}

where $V_p$ is the blade velocity relative to the hub fixed frame and $V_w$ is the airflow velocity.

The expression of the airflow velocity is given by

\begin{equation}
V_w = 
\begin{pmatrix}
V_{wx} \\ V_{wy} \\ V_{wz}
\end{pmatrix}
=
\begin{pmatrix}
\mu \Omega R cos(\psi) cos(\beta_p) - \lambda \Omega R sin(\beta_p) \\
-\mu \Omega R sin(\psi) \\
-\mu \Omega R cos(\psi) sin(\beta_p) - \lambda \Omega R cos(\beta_p)
\end{pmatrix}
\end{equation}

The derivation for the blade velocity is more complicated and requires first defining the position vector $\bar{r_p}$ of point p on the blade. A detailed derivation is provided at the end of this report. Here, we present the final expressions obtained. The position vector of point p is given by,

\begin{equation}
\begin{split}
\vec{r_p}  = [r - w'(\eta sin(\theta) + \zeta cos(\theta)] \hat{i} + 
[\eta cos(\theta) - \zeta sin(\theta)] \hat{j} + \\
[w + (1 - \frac{w'^2}{2})(\eta sin(\theta) + \zeta cos(\theta)] \hat{k}
\end{split}
\end{equation}


Using this, the velocity of the point is determined using,

\begin{equation}
\vec{V_p} = \dot{\vec{r_p}} + \vec{\Omega} \times \vec{r_p}
\end{equation}

where,
\begin{equation}
\vec{\Omega} = \Omega sin(\beta_p) \hat{i}	+ \Omega cos(\beta_p) \hat{k}
\end{equation}

On evaluating, the blade velocity at is defined by
\begin{equation}
\vec{V_p} = V_{px} \hat{i} +  V_{py} \hat{j} +  V_{pz} \hat{k}
\end{equation}	

where,

\begin{equation}
\begin{split}
V_{px} = -\dot{w}'(\eta sin(\theta) + \zeta cos(\theta)) - w' \dot{\theta} (\eta cos(\theta) - \zeta sin(\theta)) \\
- \Omega cos(\beta_p) (\eta cos(\theta) - \zeta sin(\theta))
\end{split}
\end{equation}


\begin{equation}
\begin{split}
V_{py} = \dot{\theta} (\eta sin(\theta) + \zeta cos(\theta)) + \Omega cos(\beta_p) (r - w'(\eta sin(\theta) + \zeta cos(\theta))) \\ - \Omega sin(\beta_p) (w + (1 - \frac{w'^2}{2}) (\eta sin(\theta) + \zeta cos(\theta)))	
\end{split}
\end{equation}


\begin{equation}
\begin{split}
V_{pz} = \dot{w} - \dot{w}' w' (\eta sin(\theta) + \zeta cos(\theta)) + (1 - \frac{w'^2}{2}) \dot{\theta} (\eta cos(\theta) - \zeta sin(\theta)) \\ + \Omega sin(\beta_p) (\eta cos(\theta) - \zeta sin(\theta))
\end{split}
\end{equation}

For quasi-steady aerodynamics, the blade aerodynamic loads are based on the angle of attack at the three-quarter chord location. This requires the velocity components to also be calculated at the three-quarter chord location ($\eta = \eta_r$, $\zeta = 0$). In addition, the precone angle, $\beta_p$, is typically small and therefore
the small angle assumptions of $sin(\beta_p) \approx \beta_p$ and $cos(\beta_p) \approx 1$ have been used. Finally, the incident velocity must be rotated into the deformed frame in order to get the radial, tangential, and perpendicular components. The reader is requested to refer the derivation mentioned at the end of the report for better understanding. Hence, the final expressions for the aerodynamic velocities are,

\begin{equation}
\begin{split}
u_R = \frac{U_R}{\Omega R} = (\lambda \beta_p - \eta_r cos\theta - \mu cos\psi) + (\mu \beta_p cos\psi + \lambda) w' + \\ (-\eta_r sin\theta) \dot{w}' + \dot{w}w' + (\frac{1}{2} \mu cos\psi) w'^2
\end{split}
\end{equation}


\begin{equation}
\begin{split}
u_T = \frac{U_T}{\Omega R} = [sin\theta (\lambda + \mu cos\psi \beta_p) + cos\theta (x + \mu sin\psi)] + \\ (-\beta_p cos\theta) w + (\mu cos\psi sin\theta) w' + (sin\theta) \dot{w}
\end{split}
\end{equation}

\begin{equation}
\begin{split}
u_P = \frac{U_P}{\Omega R} = [\eta_r (\dot{\theta} +\beta_p) - sin\theta (x + \mu sin\psi) + cos\theta (\lambda + \mu \beta_p cos\psi) ] + \\ (\beta_p sin\theta) w + (\eta_r + \mu cos\psi cos\theta) w' + (cos\theta)\dot{w}
\end{split}
\end{equation}



\subsection{Quasi-Steady Airloads}

Quasi-steady aerodynamic analysis assumes that the blade airloads are solely a function of the instantaneous blade section angle of attack. Lift, drag and pitching moment is strictly based on static data. Once the aerodynamic velocities are known, the airloads can be determined using two dimensional strip theory. These loads are first derived in deformed frame and later converted to undeformed frame using the Transformation matrix $T_{DU}$.

The aerodynamic coefficients are expressed as,

\begin{equation}
C_l = c_o + a \alpha
\end{equation}

\begin{equation}
C_d = d_o + d_1 |\alpha| + d_2 \alpha^2
\end{equation}

\begin{equation}
C_m = c_{mac} + f_1 \alpha
\end{equation}

A detailed derivation for the aerodynamic forces and moments is provided in the Appendix section. Hence, here we present the final expressions of non-dimensional aerodynamic loads in the deformed frame,

\begin{equation}
\bar{L_u}^A  = \frac{\gamma}{6a} (-d_o u_R u_T) 
\end{equation}

\begin{equation}
\bar{L_v}^A  = \frac{\gamma}{6a} (-d_o u_T^2 - (c_o u_P - d_1 |u_P|) u_T + (a - d_2) u_P^2) 
\end{equation}

\begin{equation}
\bar{L_w}^A  = \frac{\gamma}{6a} (c_o u_T^2 - (a + d_o) u_P u_T + d_1 |u_P|u_P) 
\end{equation}

\begin{equation}
\bar{M_{\phi}}^A  = \frac{\gamma}{6a} [\frac{c}{R} (c_{mac} (u_T^2 u_P^2) - f_1 u_T u_P)] - e_d \bar{L_w}^A
\end{equation}

In the above equation, $e_d$ is the chordwise offset of the aerodynamic center behind the elastic axis. When calculating the blade response, it is necessary to rotate the airloads from the deformed frame to undeformed frame using the transformation matrix $T_{DU}$. The following transformation converts the airloads to undeformed frame.

\begin{equation}
\begin{pmatrix}
L_u^A \\ L_v^A \\ L_w^A 
\end{pmatrix}
= T_{DU}^{-1} 
\begin{pmatrix}
\bar{L_u}^A \\ \bar{L_v}^A\\ \bar{L_w}^A
\end{pmatrix}
\end{equation}


\begin{equation}
\begin{pmatrix}
M_u^A \\ M_v^A \\ M_w^A 
\end{pmatrix}
= T_{DU}^{-1} 
\begin{pmatrix}
\bar{M_{\phi}}^A \\ 0\\ 0
\end{pmatrix}
 + 
 \begin{pmatrix}
 x \\ 0 \\ w
 \end{pmatrix}
 \times
 \begin{pmatrix}
 L_u^A \\ L_v^A \\ L_w^A 
 \end{pmatrix}
\end{equation}


Remember, to compute $T_{DU}^{-1}$, we need not invert, rather compute the transpose of $T_{DU}$. For computing the blade response, only the flapping terms are required, which is explained in the next section.