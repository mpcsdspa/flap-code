

\section{Linear Aerodynamic Results}

Linear Aerodynamic model is an approximate model obtained after neglecting higher order terms. Hence, this model does not capture the complete non-linearity present in the physics of the problem. However the implementation of this model will give a good picture for implementing the Non-linear model. The following tables provide the trimmed parameters obtained after trim analysis.

\begin{table}[H]
	\centering
	\label{my-label}
	\begin{tabular}{@{}|c|c|c|c|c|c|@{}}
		\hline
		& $\bm{\mu = 0}$      & $\bm{ \mu = 0.1}$   & $\bm{\mu = 0.2}$     & $\bm{\mu = 0.3}$     \\ \hline
		$\bm{\alpha_s}$  & 0      & -0.104 & -0.085 & -0.02 \\ \hline
		$\bm{\phi_s}$  & 0      & 0.133      & 0.326       & 0.579      \\ \hline
		$\bm{\theta_0}$ & 9.03   & 6.61     & 5.79       & 5.99      \\ \hline
		$\bm{\theta_{1c}}$  & 0      & 0.613     & 1.12       & 1.64       \\ \hline
		$\bm{\theta_{1s}}$  & 0      & -1.44      & -2.68       & -4.05       \\ \hline
		$\bm{\lambda_{TPP}}$  & 0.0585 & 0.0284     & 0.0145       & 0.0099       \\ \hline
	\end{tabular}
	\caption{Trim results for Non-Linear Aerodynamic forcing for different advance ratios.}
\end{table}


\begin{table}[H]
	\centering
	\label{my-label}
	\begin{tabular}{@{}|c|c|c|c|c|c|@{}}
		\hline
		& $\bm{\mu = 0}$      & $\bm{ \mu = 0.1}$   & $\bm{\mu = 0.2}$     & $\bm{\mu = 0.3}$     \\ \hline
		$\bm{C_H}$  & $-8.71 \times 10^{-15}$       & $-1.08 \times 10^{-5}$  & $-8.85 \times 10^{-6}$  & $-2.1 \times 10^{-7}$  \\ \hline
		$\bm{C_Y}$  & $9.76 \times 10^{-15}$       & $-1.38 \times 10^{-5}$       & $-3.39 \times 10^{-5}$      & $-5.96 \times 10^{-5}$        \\ \hline
		$\bm{C_T}$ & $0.00595$    & $0.00595$      & $0.00595$       & $0.00595$       \\ \hline
		$\bm{C_{Mx}}$  & $7.54 \times 10^{-14}$       & $-2.77 \times 10^{-6}$      & $-6.78 \times 10^{-6}$       & $-1.19 \times 10^{-5}$       \\ \hline
		$\bm{C_{My}}$& $-5.81 \times 10^{-14}$       & $2.15 \times 10^{-6}$       & $1.77 \times 10^{-6}$       & $4.19 \times 10^{-8}$       \\ \hline
		$\bm{C_{Mz}}$  & $-4.40 \times 10^{-4}$  & $-2.67 \times 10^{-4}$      & $-1.98 \times 10^{-4}$      & $-1.93 \times 10^{-4}$        \\ \hline
	\end{tabular}
	\caption{Forces and Moments at trimmed state for Non-Linear Aerodynamic forcing}
\end{table}




\subsection{Tip Displacement}
The tip displacements $(\frac{w_{tip}}{R})$ are computed for different advance ratios for Linear Aerodynamic forcing.


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.57]{./linear_aero/tip_disp_correct.png}
	\caption{Tip displacement with Linear Aerodynamic Forcing.}
	\end{figure}

For hover, the tip displacement is constant over the period. As advance ratio increase, the non-uniformity in the tip displacement increases as seen in the above figure. It is very important to study the harmonics associated with the tip displacement. For a zero hinge offset, the 1/rev harmonics comprises of $\beta_{1c}, \beta_{1s}$. For hover, the 0/rev harmonic is the only contribution, which is nothing but the constant term in the Fourier series expansion. For higher advance ratios, the contribution from 3/rev and higher harmonics are negligible.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.65]{./linear_aero/tip_disp_harmonics.png}
	\caption{Tip displacement harmonics with Linear Aerodynamic Forcing.}
\end{figure}


\subsection{Root Shears}
The root shears are found by integrating the forces in the undeformed frame along the span. The inertial component of spanwise root shear is 0.5, which can be validated analytically. The inertial component of the chordwise root shear is 0. 
\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Fx.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Fy.png}
	\end{minipage}
	\caption{Root shears for linear aerodynamics. The figure on the left corresponds to spanwise blade root shear and on the right corresponds to chordwise blade root shear.}
\end{figure}

In case of vertical root shear, the aerodynamic and inertial are out of phase with each other.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./linear_aero/Fz.png}
	\caption{Blade root shear in the vertical direction for Linear Aerodynamic Forcing.}
\end{figure}

\subsection{Root Moments}
The root moments are computed using the Force summation method as described above. In lagging moment, the inertial contribution is zero. We can see that in steady flapping moment, the inertial and aerodynamic contributions are out of phase with each other. The flapping moment is dominant compared to torsional and lagging root moments.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Mx.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/My.png}
	\end{minipage}
	\caption{Root moments for linear aerodynamics. The figure on the left corresponds to torsional blade root moment and on the right corresponds to flapping blade root moment.}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{./linear_aero/Mz.png}
	\caption{Lagging Blade root moment for linear aerodynamics.}
\end{figure}


\subsection{Hub Forces and Moments}

The hub forces are the total forces on the hub due to all the blades. Since the rotor is 4 bladed, the hub forces and moments contain integral multiples of 4/rev harmonics.  This can be seen analytically, for example, let the rotating frame 	vertical force from each blade be,

\begin{equation}
f_z(\psi) = a_0 + a_1 sin\psi + a_2 sin2\psi + a_3 sin3\psi + a_4sin4\psi
\end{equation}
Then, the fixed frame hub load is,

\begin{equation}
F_z(\psi) = f_z(\psi) + f_z(\psi+90^{\circ}) + f_z(\psi+180^{\circ}) + f_z(\psi+270^{\circ}) = 4a_0 + 4a_4sin4\psi
\end{equation}
From the below plots, we see that the dominant harmonics in each of the hub forces and moments is 4/rev. The hub forces are dominated by the vertical force.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Fxyh.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Fzh.png}
	\end{minipage}
	\caption{Hub forces for linear aerodynamics. The figure on the left corresponds to Longitudinal and lateral hub forces and on the right corresponds to vertical hub force.}
\end{figure}

The below figure show the  hub moments for linear aerodynamics. 
Similarly for hub moments, we can see that they show primarily 4/rev harmonics.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Mxyh.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./linear_aero/Mzh.png}
	\end{minipage}
	\caption{Hub Moments for linear aerodynamics. The figure on the left corresponds to Rolling and Pitching hub moments and on the right corresponds to Yawing hub moment.}
\end{figure}
