!=======================================================================
!> This subroutine computes the location of the point in x-t space.
!> It returns the starting point of spatial and time element.
!                           ~   ~   ~
!                            r1, t1
!                           ~   ~   ~
!=======================================================================
subroutine find_location(x_val, t_val, q_input, element_num, q_vec, r1, t1)
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                   :: x_val, t_val, q_input(nt*(nodes-1)*max_modes, 1)

  integer, intent(IN)                            :: element_num

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: q_vec(nodes, dof_per_element), r1, t1


  !=======================================================================
  !                             Local Variables
  !=======================================================================
  integer                                        :: i, x_element_no, time_element_no, a, b, &
                                                    element_dofs(ne, dof_per_element), p, j, signed

  double precision                               :: t_val_temp, l, dt, q_temp(2*ne+2, 1), &
                                                    r_left(ne), r_right(ne)


  !=======================================================================
  !                             Main Code
  !=======================================================================

  t_val_temp = t_val

  do i = 1, ne, 1
    p = 2*i - 1
    do j = 1, dof_per_element, 1
      element_dofs(i,j) = p
      p = p + 1
    end do
  end do

  element_dofs(ne/2 +1,1) = 1
  element_dofs(ne/2 +1, 2) = 2


  l = R*(1.0d0 - offset)/dble(ne)

  do i = 1, ne/2, 1
    r_left(i)  = (i-1)*l + offset*R
    r_right(i) = i*l + offset*R
  end do

  r_left(ne/2 +1:ne) = r_left(1:ne/2)
  r_right(ne/2 +1:ne) = r_right(1:ne/2)



  dt = T/nt

  q_vec = 0.0d0

  !!!!!!!!!!!!!!!!!!!t1!!!!!!!!!!!!!!!!!!!!!111
  time_element_no =  ((t_val_temp - mod(t_val_temp, dt))/dt) + 1
  if ( time_element_no == nt + 1 ) then
    time_element_no = time_element_no - 1
  end if
  t1 = (time_element_no - 1)*dt
  !!!!!!!!!!!!!!!!!!!t1!!!!!!!!!!!!!!!!!!!!!111


  x_element_no = element_num

  if ( element_num <= ne/2 ) then
    signed = 1
  else
    signed = -1
  end if


  if ( signed == 1 ) then
    time_element_no =  ((t_val_temp - mod(t_val_temp, dt))/dt) + 1
    if ( time_element_no == nt + 1 ) then
      time_element_no = time_element_no - 1
    end if
  end if


  if ( signed == -1 ) then
    if ( t_val_temp >= pi .and. t_val_temp <= 2*pi ) then
      t_val_temp = t_val_temp - pi
    else if (t_val_temp >=0 .and. t_val_temp < pi) then
      t_val_temp = t_val_temp + pi
    end if
    time_element_no =  ((t_val_temp - mod(t_val_temp, dt))/dt) + 1
    if ( time_element_no == nt + 1 ) then
      time_element_no = time_element_no - 1
    end if
  end if



  r1 = r_left(element_num)

  a = 1 + (time_element_no - 1)*(nodes - 1)*max_modes

  do i = 1, nodes, 1
    b = a + max_modes - 1
    if ( time_element_no == nt .AND. i == nodes) then
      a = 1
      b = max_modes
    end if

    q_temp(1,1) = 0.0d0
    q_temp(2:2*ne+2,1) = q_input(a:b,1)
    q_vec(i, :) = q_temp(element_dofs(x_element_no, :), 1)
    a = b + 1
  end do


end subroutine find_location

!--------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the w, w_dash, w_dot, w_dot_dash
!> at a given spatial and time location using the q_vec.
!                           ~   ~   ~   ~   ~   ~   ~   ~
!                           w, w_dash, w_dot, w_dot_dash
!                           ~   ~   ~   ~   ~   ~   ~   ~
!=======================================================================
subroutine compute_w(x_val, t_val, q_vec, r1, t1, signed, w, w_dash, w_dot, w_dot_dash)
  use hermite
  use lagrangian
  use blade_parameters
  implicit none


  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                   :: x_val, t_val, r1, t1, &
                                                    q_vec(nodes, dof_per_element)

  integer, intent(IN)                            :: signed

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: w, w_dash, w_dot, w_dot_dash


  !=======================================================================
  !                             Local Variables
  !=======================================================================
  integer                                        :: i

  double precision                               :: l, dt, h_temp(1, dof_per_element), ht, &
                                                    h_func(dof_per_element), h_dash(dof_per_element)


  !=======================================================================
  !                             Main Code
  !=======================================================================


  l = R*(1 - offset)/dble(ne)
  dt = T/nt

  call Hermite_Funcs(x_val-r1, l, signed, h_func)


  h_temp(1, :) = h_func
  w = 0.0d0
  w_dot = 0.0d0
  do i = 1, nodes, 1
    call lagrangian_time(t_val, t1, i, dt, ht)
    w = w + dot_product(h_func, q_vec(i, :)) * ht
    call lagrangian_first_der(t_val, t1, i, dt, ht)
    w_dot = w_dot + (dot_product(h_func, q_vec(i, :)) * ht)
  end do


  call Hermite_First_Der_Funcs(x_val-r1, l, signed, h_dash)


  h_temp(1, :)  = h_dash
  w_dash = 0.0d0
  w_dot_dash = 0.0d0
  do i = 1, nodes, 1
    call lagrangian_time(t_val, t1, i, dt, ht)
    w_dash = w_dash + dot_product(h_dash, q_vec(i, :)) * ht
    call lagrangian_first_der(t_val, t1, i, dt, ht)
    w_dot_dash = w_dot_dash + dot_product(h_dash, q_vec(i, :)) * ht
  end do


end subroutine compute_w

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the tip displacement as a function of
!> azimuth.
!                           ~   ~   ~   ~   ~   ~   ~   ~
!                                   r_vec, phi_vec
!                           ~   ~   ~   ~   ~   ~   ~   ~
!=======================================================================
subroutine compute_tip_disp(qtip, r_vec, phi_vec)
  use blade_parameters
  use lagrangian
  use my_funcs
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                   :: qtip((nodes-1)*nt, 1)

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision                               :: r_vec(100),phi_vec(100)


  !=======================================================================
  !                             Main Code
  !=======================================================================
  double precision                               :: dt, t_left(nt), t_right(nt), w, q1(nodes)

  double precision, allocatable, dimension(:)    :: s, h_func, qtip_temp

  integer                                        :: n_interior, i, p, j, count, &
                                                    elemental_nodes_per_mode(nt, nodes)


  n_interior = 10
  dt = T/nt
  count = 1

  do i = 1, nt, 1
    t_left(i)  = (i-1)*dt
    t_right(i) = i*dt
  end do

  p = 2
  do i = 1, nt, 1
    p = p - 1
    do j = 1, nodes, 1
      elemental_nodes_per_mode(i,j) = p
      p = p + 1
    end do
  end do

  elemental_nodes_per_mode(nt, nodes) = 1

  allocate(s(n_interior))
  allocate(h_func(nodes))
  allocate(qtip_temp((nodes-1)*nt))

  qtip_temp = qtip(:,1)

  do i = 1, nt, 1
    call linspace(t_left(i), t_right(i), n_interior, s)
    q1 = qtip_temp(elemental_nodes_per_mode(i,:))
    do j = 1, n_interior, 1
      call Lagrangian_Funcs(s(j), t_left(i), dt, h_func)
      w = dot_product(h_func, q1)
      phi_vec(count) = w
      r_vec(count) = s(j)
      count = count + 1
    end do
  end do



end subroutine compute_tip_disp

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the w_double_dot vector.
!> q_double_dot = inv(M) * (F - K * q - C * q_dot)
!                           ~   ~   ~   ~   ~   ~   ~   ~
!                                   w_double_dot
!                           ~   ~   ~   ~   ~   ~   ~   ~
!=======================================================================
subroutine compute_w_double_dot()
  use trim_parameters
  use finite_elements
  use blade_parameters
  use aerodynamic_parameters
  implicit none

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                               :: l, t_val, r1, t1, w, w_dash, &
                                                    w_dot, w_dot_dash, t_val_temp

  double precision                               :: C_qdot(max_modes, 1), K_q(max_modes, 1),&
                                                    A_temp(num_modes, num_modes), b_temp(num_modes, 1), &
                                                    C_qdot1(num_modes, 1), K_q1(num_modes, 1)

  double precision                               :: q_vec(nodes, dof_per_element), q_dot(2*ne+2, 1), &
                                                    q_ddot_temp(max_modes, 1), &
                                                    q_ddot_temp1(num_modes, 1)

  double precision                               :: K_AERO(2*ne+2, 2*ne+2), C_AERO(2*ne+2, 2*ne+2), &
                                                    force_vector(2*ne+2, 1), f1_aero(dof_per_element, 1), &
                                                    k1_aero(dof_per_element, dof_per_element), &
                                                    c1_aero(dof_per_element, dof_per_element)

  double precision, allocatable, dimension(:,:)  :: M_new, K_new, C_new, F_new

  integer                                        :: i, j, p, info, signed, &
                                                    ipiv(max_modes), ipiv1(num_modes)

  !=======================================================================
  !                             Main Code
  !=======================================================================


  l = R*(1.0d0 - offset)/dble(ne)


  !=======================================================================
  !                  Loop over nt*(nodes-1)
  !=======================================================================
  do j = 1, nt*(nodes-1), 1

    t_val = ((j-1)*T)/(nt*(nodes-1))
    !=======================================================================
    !                  Loop over ne
    !=======================================================================
    do i = 1, ne/2, 1
      call find_location(r_left(i), t_val, q_output, i, q_vec, r1, t1)
      call compute_w(r_left(i), t_val, q_vec, r1, t1, 1, w, w_dash, w_dot, w_dot_dash)
      q_dot(2*i-1,1) = w_dot
      q_dot(2*i,1) = w_dot_dash
    end do

    call find_location(r_right(ne/2), t_val, q_output, ne/2, q_vec, r1, t1)
    call compute_w(r_right(ne/2), t_val, q_vec, r1, t1, 1, w, w_dash, w_dot, w_dot_dash)
    q_dot(ne+1,1) = w_dot
    q_dot(ne+2,1) = w_dot_dash

    do i = ne/2 +2, ne, 1
      t_val_temp = t_val + pi
      if ( t_val_temp >= 2.0d0*pi ) then
        t_val_temp = t_val_temp - 2.0d0*pi
      end if
      call find_location(r_left(i), t_val_temp, q_output, i, q_vec, r1, t1)
      call compute_w(r_left(i), t_val_temp, q_vec, r1, t1, -1, w, w_dash, w_dot, w_dot_dash)
      q_dot(2*i-1,1) = w_dot
      q_dot(2*i,1) = w_dot_dash
    end do

    t_val_temp = t_val + pi
    if ( t_val_temp >= 2.0d0*pi ) then
      t_val_temp = t_val_temp - 2.0d0*pi
    end if

    call find_location(r_right(ne), t_val_temp, q_output, ne, q_vec, r1, t1)
    call compute_w(r_right(ne), t_val_temp, q_vec, r1, t1, -1, w, w_dash, w_dot, w_dot_dash)
    q_dot(2*ne+1,1) = w_dot
    q_dot(2*ne+2,1) = w_dot_dash

    call main_fem()

    if ( flag_modal_reduction == 1 ) then
      A_temp = MATMUL(transpose(Modes_bar), Modes_bar)
      b_temp(:,1) = MATMUL(transpose(Modes_bar), q_dot(b,1))
      call DGESV (num_modes, 1, A_temp, num_modes, ipiv1, b_temp, num_modes, info)
    end if

    K_AERO = 0.0d0
    C_AERO = 0.0d0
    force_vector = 0.0d0
    do i = 1, ne, 1

      if (i <= ne/2) then
        signed = 1
      else
        signed = -1
      end if

      call compute_aero_stiffness(t_val, r_left(i), r_right(i), l, i, signed, k1_aero)
      call compute_aero_damping(t_val, r_left(i), r_right(i), l, i, signed, c1_aero)
      call compute_force_vector(t_val, r_left(i), r_right(i), l, i, signed, f1_aero)
      K_AERO(element_dofs(i,:), element_dofs(i,:)) = K_AERO(element_dofs(i,:), element_dofs(i,:)) + k1_aero
      C_AERO(element_dofs(i,:), element_dofs(i,:)) = C_AERO(element_dofs(i,:), element_dofs(i,:)) + c1_aero
      force_vector(element_dofs(i,:), 1) = force_vector(element_dofs(i,:), 1) + f1_aero(:,1)
    end do

    !=======================================================================
    !                  Allocation of Variables
    !=======================================================================
    allocate(M_new(num_modes, num_modes))
    allocate(K_new(num_modes, num_modes))
    allocate(C_new(num_modes, num_modes))
    allocate(F_new(num_modes, 1))


    if ( flag_modal_reduction == 0 ) then
      M_new = M_bar1
      K_new = K_bar1 + K_AERO(b, b)
      C_new = C_AERO(b,b)
      F_new(:,1) = force_vector(b,1)

      C_qdot(:,1) = MATMUL(C_new, q_dot(b,1))
      K_q(:,1) = MATMUL(K_new, q_output(max_modes*(j-1)+1:max_modes*j, 1))

      q_ddot_temp(:,1) = F_new(:,1) - C_qdot(:,1) - K_q(:,1)

      call DGESV(max_modes, 1, M_new, max_modes, ipiv, q_ddot_temp, max_modes, info)

      q_ddot(max_modes*(j-1)+1:max_modes*j, 1) = q_ddot_temp(:, 1)


    else if (flag_modal_reduction == 1) then
      M_new = M_bar
      K_new = K_bar + MATMUL(transpose(Modes_bar), MATMUL(K_AERO(b,b), Modes_bar))
      C_new = MATMUL(transpose(Modes_bar), MATMUL(C_AERO(b,b), Modes_bar))
      F_new(:,1) = MATMUL(transpose(Modes_bar), force_vector(b,1))

      C_qdot1(:,1) = MATMUL(C_new, b_temp(:,1))
      K_q1(:,1) = MATMUL(K_new, q_output_mr(num_modes*(j-1)+1:num_modes*j, 1))

      q_ddot_temp1(:,1) = F_new(:,1) - C_qdot1(:,1) - K_q1(:,1)

      call DGESV(num_modes, 1, M_new, num_modes, ipiv1, q_ddot_temp1, num_modes, info)

      q_ddot(max_modes*(j-1)+1:max_modes*j, 1) = MATMUL(Modes_bar, q_ddot_temp1(:, 1))
    end if


    DEALLOCATE(M_new)
    DEALLOCATE(K_new)
    DEALLOCATE(C_new)
    DEALLOCATE(F_new)

  end do


end subroutine compute_w_double_dot

!-------------------------------------------------------------------------------
