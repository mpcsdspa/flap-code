!=======================================================================
!> This subroutine computes the linear aerodynamic force for a given vector
!> of trim parameters at a given azimuth and space location.
!                   ~     ~     ~     ~     ~     ~
!                               aero_force
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine compute_aero_force(x_val, t_val, flag, aero_force)
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  integer, intent(IN)                            :: flag

  double precision, intent(IN)                   :: x_val, t_val

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: aero_force

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  double precision                               :: l, dt, psi, x, theta, theta_dot, &
                                                    c0, c1, c2, c3, L0, L1, L2, L3, L4, &
                                                    theta_0, theta_1c, theta_1s, lambda, &
                                                    a0, a1, b0, b1, b2, b3


  !=======================================================================
  !                     Extracting the trim parameters
  !=======================================================================
  theta_0 = trim_vec(3)
  theta_1c = trim_vec(4)
  theta_1s = trim_vec(5)
  lambda = trim_vec(6)

  l = R*(1.0d0 - offset)/dble(ne)
  dt = T/nt

  psi = omega * t_val
  x = x_val

  theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)
  theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))

  !=======================================================================
  !                           Dummy Variables
  !=======================================================================
  a0 = -mu*cos(psi) + lambda*beta_p - neta_r*cos(theta)
  a1 = 0.0d0

  b0 = sin(theta) * (lambda + mu*cos(psi)*beta_p) + cos(theta) * (x + mu*sin(psi))
  b1 = -beta_p * cos(theta)
  b2 = mu*cos(psi)*sin(theta)
  b3 = sin(theta)

  c0 = -sin(theta) * (x + mu*sin(psi)) + cos(theta) * (lambda + mu*beta_p*cos(psi)) + neta_r*(theta_dot + beta_p)
  c1 = beta_p * sin(theta)
  c2 = neta_r + mu*cos(psi)*cos(theta)
  c3 = cos(theta)

  !=======================================================================
  !
  !                             Lift expression
  !   "L = L0 + L1 * w + L2 * w_dash + L3 * w_dot + L4 * w_dot_dash"
  !
  !=======================================================================

  L0 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-Cdo * b0*b0 + Cl_alpha * c0*c0) &
                                - cos(theta) * (Cl_alpha + Cdo)*b0*c0)
  L1 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b1 + 2.0d0*Cl_alpha*c0*c1) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c1 + b1*c0))
  L2 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b2 + 2.0d0*Cl_alpha*c0*c2) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c2 + b2*c0) - Cdo*a0*b0)
  L3 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b3 + 2.0d0*Cl_alpha*c0*c3) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c3 + c0*b3))
  L4 = 0.0d0


  if ( flag == 0 ) then
    aero_force = L0
  else if ( flag == 1 ) then
    aero_force = L1
  else if ( flag == 2 ) then
    aero_force = L2
  else if ( flag == 3 ) then
    aero_force = L3
  else if ( flag == 4 ) then
    aero_force = L4
  end if


end subroutine compute_aero_force

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the non-linear aerodynamic force for
!> a given vectormof trim parameters at a given azimuth and space location.
!                   ~     ~     ~     ~     ~     ~
!                               aero_force
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine compute_non_linear_aero(x_val, t_val, element_num, flag, aero_force)
  use finite_elements
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none


  !=======================================================================
  !                             Inputs
  !=======================================================================

  integer, intent(IN)                            :: flag, element_num

  double precision, intent(IN)                   :: x_val, t_val

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: aero_force

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  double precision                               :: l, dt, psi, x, theta, theta_dot,  &
                                                    b0, b1, b2, b3, c0, c1, c2, c3, &
                                                    B_1, B_2, B_3, B_4, B_5, B_6, B_7, B_8, B_9, B_10, &
                                                    BNL_0, BNL_w, BNL_w_dash, BNL_w_dot, BNL_w_dot_dash, &
                                                    w, w_dash, w_dot, w_dot_dash, &
                                                    a0, a1, a2, a3, a4, a5, a6

  integer                                        :: signed                                              

  double precision                               :: q_vec(nodes, dof_per_element), r1, t1, &
                                                    theta_0, theta_1c, theta_1s, lambda


   !=======================================================================
   !                             Main Code
   !=======================================================================

  l = R*(1.0d0 - offset)/dble(ne)
  dt = T/nt

  theta_0 = trim_vec(3)
  theta_1c = trim_vec(4)
  theta_1s = trim_vec(5)
  lambda = trim_vec(6)


  if ( element_num <= ne/2 ) then
    signed = 1
  else
    signed = -1
  end if


  !=======================================================================
  !                Compute w, w_dash, w_dot, w_dot_dash
  !=======================================================================
  call find_location(x_val, t_val, q_input, element_num, q_vec, r1, t1)

  call compute_w(x_val, t_val, q_vec, r1, t1, signed, w, w_dash, w_dot, w_dot_dash)

  psi = omega * t_val
  x = x_val
  theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)
  theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))

  !=======================================================================
  !                             Dummy Variables
  !=======================================================================
  a0 = -mu*cos(psi) + lambda*beta_p - neta_r*cos(theta)
  a1 = 0.0d0
  a2 = mu*beta_p*cos(psi) + lambda
  a3 = 0.0d0
  a4 = -neta_r*sin(theta)
  a5 = 1.0d0
  a6 = 0.5d0*mu*cos(psi)

  b0 = sin(theta) * (lambda + mu*cos(psi)*beta_p) + cos(theta) * (x + mu*sin(psi))
  b1 = -beta_p * cos(theta)
  b2 = mu*cos(psi)*sin(theta)
  b3 = sin(theta)

  c0 = -sin(theta) * (x + mu*sin(psi)) + cos(theta) * (lambda + mu*beta_p*cos(psi)) + neta_r*(theta_dot + beta_p)
  c1 = beta_p * sin(theta)
  c2 = neta_r + mu*cos(psi)*cos(theta)
  c3 = cos(theta)


  !=======================================================================
  !                      Non-Linear component of Lift
  ! L = B_1 * w^2 + B_2 * w * w_dash + B_3 * w * w_dot + B_4 * w * w_dot_dash
  !   + B_5 * w_dash^2 + B_6 * w_dash * w_dot + B_7 * w_dash * w_dot_dash
  !   + B_8 * w_dot^2 + B_9 * w_dot * w_dot_dash + B_10 * w_dot_dash^2
  !=======================================================================

  B_1 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta)*(-Cdo*b1*b1 + Cl_alpha*c1*c1) &
                                    - cos(theta)*(Cl_alpha + Cdo)*b1*c1)

  B_2 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta)*(-2.0d0*Cdo*b1*b2 + 2.0d0*Cl_alpha*c1*c2) &
                                  - cos(theta)*(Cl_alpha+Cdo)*(b1*c2+c1*b2) &
                                  - Cdo*(a0*b1 + a1*b0))

  B_3 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta)*(-2.0d0*Cdo*b1*b3 + 2.0d0*Cl_alpha*c1*c3) &
                                  - cos(theta)*(Cl_alpha + Cdo)*(b1*c3 + c1*b3))

  B_4 = 0.0d0

  B_5 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta)*(-Cdo*b2*b2 + Cl_alpha*c2*c2) &
                                  - cos(theta)*(Cl_alpha+Cdo)*b2*c2 &
                                  - Cdo*(a0*b2 + b0*a2) &
                                  - 0.5d0*sin(theta)*(-Cdo*b0*b0 + Cl_alpha*c0*c0) &
                                  + 0.5d0*cos(theta)*(Cl_alpha+Cdo)*b0*c0)

  B_6 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta)*(-2.0d0*Cdo*b2*b3 + 2.0d0*Cl_alpha*c2*c3) &
                                  - cos(theta)*(Cl_alpha+Cdo)*(b2*c3 + c2*b3) &
                                  - Cdo*(a0*b3 + b0*a3))

  B_7 = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo*a4*b0)

  B_8 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-Cdo*b3*b3 + Cl_alpha*c3*c3) &
                                  - cos(theta)*(Cl_alpha + Cdo)*b3*c3)

  B_9 = 0.0d0

  B_10 = 0.0d0


  BNL_0 = B_1 * w*w + B_2 * w * w_dash + B_3 * w * w_dot + B_4 * w * w_dot_dash &
          + B_5 * w_dash * w_dash + B_6 * w_dash * w_dot + B_7 * w_dash * w_dot_dash &
          + B_8 * w_dot * w_dot + B_9 * w_dot * w_dot_dash &
          + B_10 * w_dot_dash * w_dot_dash

  BNL_w = 2.0d0*B_1*w + B_2*w_dash + B_3*w_dot + B_4*w_dot_dash

  BNL_w_dash = B_2*w + 2.0d0*B_5*w_dash + B_6*w_dot + B_7*w_dot_dash

  BNL_w_dot = B_3*w + B_6*w_dash + 2.0d0*B_8*w_dot + B_9*w_dot_dash

  BNL_w_dot_dash = B_4*w + B_7*w_dash + B_9*w_dot + 2.0d0*B_10*w_dot_dash



  if ( flag == 0 ) then
    aero_force = BNL_0
  else if ( flag == 1 ) then
    aero_force = BNL_w
  else if ( flag == 2 ) then
    aero_force = BNL_w_dash
  else if ( flag == 3 ) then
    aero_force = BNL_w_dot
  else if ( flag == 4 ) then
    aero_force = BNL_w_dot_dash
  end if


end subroutine compute_non_linear_aero

!-------------------------------------------------------------------------------
