\section{Method II Results and Analysis}

Until now, we have looked at the trimmed results obtained for hingeless and articulated rotors. All the above results were computed using Method I. However, we can get the same results using Method II. For example, the tip displacement as a function of $\psi$ for $\mu=0.3$ using both methods is shown below. We can see that both plots are same with slight differences. These differences are due to truncation of higher order terms in NOn-Linear Aerodynamic model of Method I.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.65]{./nonlinear_aero/m1vsm2.png}
	\caption{Tip displacement with Non-Linear Aerodynamic Forcing.}
\end{figure}

Hence, we will not look into the results obtained using Method II. However, in this section, we will analyze the Method II mentioned earlier and see how it can be used to for stability analysis. 


We know that the stiffness and damping matrices present in the governing equation of flapping involve both spatial and temporal variations. In our analysis for solving the flap  response using FEM and FET involves the computations of these stiffness and damping matrices at only Gaussian quadrature nodes. What if we wanted to compute the damping and stiffness matrices at any given time $t$. In such cases, the Method II presented above will come handy. 

We know that the expression for flapping aerodynamic load is written as, 

\begin{equation}
L_w^{A} =  L_0 + L_1 w + L_2 w' + L_3 \dot{w} + L_4 \dot{w}' + L_{Non-Linear}
\end{equation}

where,
\begin{equation}
\begin{gathered}
L_{Non-Linear} = A_1 w^2 + A_2 w w'+ A_3 w \dot{w} + A_4 w \dot{w}' 
A_5 w'^2 + A_6 \dot{w} w' + \\
+ A_7 w' \dot{w}' + A_8 \dot{w}^2 + A_9 \dot{w} \dot{w}' + A_{10} \dot{w}'^2
\end{gathered}
\end{equation}	

Note that the above expression is analytically decomposed as shown above. However, we can still compute the flapping aerodynamic load using,

\begin{equation}
L_w^{A} =  w' \bar{L_u}^A + (1 - \frac{w'^2}{2})sin(\theta) \bar{L_v}^A + (1 - \frac{w'^2}{2})cos(\theta) \bar{L_w}^A
\end{equation}

where $\bar{L_u}^A, \bar{L_v}^A, \bar{L_w}^A$ are defined in the earlier sections. So numerically speaking, it does not matter, whether we use Equation 59, 60 or Equation 61, as both will yield same numerical quantity. But we know that, the stiffness and damping matrices for FEM are computed as,

		\begin{equation}
		C_{e}^{A} = \int_{0}^{l} [-(\left.\frac{\partial L_{w}^A}{\partial \dot{w}}\right|_{w_0} ) H^{T} H  - (\left.\frac{\partial L_{w}^A}{\partial \dot{w}'}\right|_{w_0} )H^T  H' ]ds
		\end{equation}
		
		
		\begin{equation}
		K_{e}^{A} = \int_{0}^{l} [-(\left.\frac{\partial L_{w}^A}{\partial {w}}\right|_{w_0} ) H^{T} H  - (\left.\frac{\partial L_{w}^A}{\partial {w}'}\right|_{w_0} )H^T  H' ]ds
		\end{equation}

To numerically compute the values of $\left.\frac{\partial L_{w}^A}{\partial \dot{w}}\right|_{w_0}, \left.\frac{\partial L_{w}^A}{\partial \dot{w}'}\right|_{w_0}, \left.\frac{\partial L_{w}^A}{\partial {w}}\right|_{w_0}, \left.\frac{\partial L_{w}^A}{\partial {w}'}\right|_{w_0} $, we make use of the Finite Difference schemes. Consider the forward Finite Difference Scheme. For a function $f(x)$, the derivative at $x_o$ is given as,

\begin{equation}
\left.\frac{\partial f}{\partial x}\right|_{x_0} \approx \frac{f(x_o + \Delta x) - f(x_o)}{\Delta x}
\end{equation}

We know that $L_w^A$ is a function of $w, w', \dot{w}, \dot{w}'$. Hence, the above finite difference scheme when applied to $L_w^A$ results in partial derivatives as,

\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}}\right|_{w_0} \approx \frac{L_w^A(w,w', \dot{w} + \Delta \dot{w}, \dot{w}') - L_w^A(w,w', \dot{w}, \dot{w}') }{\Delta \dot{w}}
\end{equation}

\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}'}\right|_{w_0} \approx \frac{L_w^A(w,w', \dot{w}, \dot{w}'+ \Delta \dot{w}') - L_w^A(w,w', \dot{w}, \dot{w}') }{\Delta \dot{w}'}
\end{equation}


\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial w}\right|_{w_0} \approx \frac{L_w^A(w+ \Delta w,w', \dot{w}, \dot{w}') - L_w^A(w,w', \dot{w}, \dot{w}') }{\Delta w}
\end{equation}


\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial w'}\right|_{w_0} \approx \frac{L_w^A(w,w'+ \Delta w', \dot{w} , \dot{w}') - L_w^A(w,w', \dot{w}, \dot{w}') }{\Delta w'}
\end{equation}

For better accuracy, we can make use of the central difference scheme. But near stall, it's recommended to use the forward difference scheme. In the above equations, $\Delta \dot{w}, \Delta \dot{w}', \Delta w, \Delta w'$ can be taken as 0.001 or lesser. Hence, with the help of Equations 62 and 63, we are able to numerically compute the damping and stiffness matrices respectively. Just to make things more clear, the exact value obtained from the above partial derivatives are given by,


\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}}\right|_{w_0} = L_1 + 2 A_1 w_0 + A_2 w_0'+ A_3 \dot{w_0} + A_4 \dot{w_0}'  
\end{equation}

\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}'}\right|_{w_0} = L_2 + A_2 w_0 + 2 A_5 w_0'+ A_6 \dot{w_0} + A_7 \dot{w_0}'  
\end{equation}


\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}}\right|_{w_0} = L_3 + A_3 w_0 + A_6 w_0'+ 2 A_8 \dot{w_0} + A_9 \dot{w_0}'  
\end{equation}

\begin{equation}
\left.\frac{\partial L_{w}^A}{\partial \dot{w}'}\right|_{w_0} = L_4 + A_4 w_0 + A_7 w_0'+ A_9 \dot{w_0} + 2 A_{10} \dot{w_0}'  
\end{equation}

Clearly it is easier to compute the stiffness and damping matrices using the finite difference approach than the analytical approach. Now, we can use similar strategy to compute the forcing vector at a given time $t$. We know that the forcing vector for FEM is computed as, 

\begin{equation}
Q_{e}^{A} = \int_{0}^{l} H^{T} [L_0 + \left.L_{nl}\right|_{w_0} - \left.\frac{\partial L_{nl}}{\partial {w}}\right|_{w_0} w_0    - \left.\frac{\partial L_{nl}}{\partial {w}'}\right|_{w_0} w_0'-
\left.\frac{\partial L_{nl}}{\partial {\dot{w}}}\right|_{w_0} \dot{w_0}-
\left.\frac{\partial L_{nl}}{\partial {\dot{w}'}}\right|_{w_0} \dot{w_0}'] ds
\end{equation}

The above equation an be numerically computed using Method II at any given time $t$, using,

\begin{equation}
Q_{e}^{A} = \int_{0}^{l} H^{T} [L_w^A - \left.\frac{\partial L_{w}^A}{\partial {w}}\right|_{w_0} w_0    - \left.\frac{\partial L_{w}^A}{\partial {w}'}\right|_{w_0} w_0'-
\left.\frac{\partial L_{w}^A}{\partial {\dot{w}}}\right|_{w_0} \dot{w_0}-
\left.\frac{\partial L_{w}^A}{\partial {\dot{w}'}}\right|_{w_0} \dot{w_0}'] ds
\end{equation}

where, the corresponding partial derivatives are obtained using the finite difference scheme mentioned above.

One can understand that the above analysis is based on perturbing the displacements, $w$ and its spatial and temporal derivatives. We can however analyze from the $\eta$ vector level, where $\eta$ is the FET solution of $A \eta = Q$. Basically, $\eta$ contains the displacements and its slopes at nodal points. To proceed, we write using chain rule,

\begin{equation}
\frac{\partial L_{w}^A}{\partial {\eta}} = \frac{\partial L_{w}^A}{\partial {w}} \cdot \frac{\partial w}{\partial {\eta}} + \frac{\partial L_{w}^A}{\partial {w'}} \cdot \frac{\partial w'}{\partial {\eta}} 
\end{equation}

Simplifying and multiplying by the shape function matrix $H$, will result us back to the same equations 62 and 63. The conclusion is that, we can numerically compute the stiffness and damping matrices at any given time $t$ using this approach. The advantage of this approach is that we can easily include the effects of other vehicle components if any. These matrices are further used in the stability analysis of the system.

