!=======================================================================
!> This subroutine computes the residual vector of trim for a given
!> trim parameters.
!                           ~         ~        ~
!                                  residual
!                           ~         ~        ~
!=======================================================================

subroutine trim_residual(residual)
  use loads
  use trim_residuals
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none



  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: residual(n_trim)

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                               :: T_BG(3,3), T_BH(3,3), F_B(3, 1), &
                                                    F_B_A_fuselage(3,1), M_B_A_fuselage(3,1), &
                                                    M_B_I_fuselage(3, 1), F_B_gravity(3,1), &
                                                    Weight_vec(3, 1), force_vec(3,1), moment_vec(3,1), &
                                                    F_B_rotor(3, 1), M_B_gravity(3, 1), &
                                                    M_B_rotor(3, 1), M_B(3, 1)

  double precision                               :: lambda_residual, alpha, phi, const

  integer                                        :: i


  !=======================================================================
  !                             Main Code
  !=======================================================================

  F_B_A_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]
  M_B_I_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]
  M_B_A_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]

  alpha = trim_vec(1)
  phi = trim_vec(2)

  T_BG(1, :) = [cos(alpha), 0.0d0, sin(alpha)]
  T_BG(2, :) = [-sin(alpha)*sin(phi), cos(phi), cos(alpha)*sin(phi)]
  T_BG(3, :) = [-sin(alpha)*cos(phi), -sin(phi), cos(alpha)*cos(phi)]

  Weight_vec(:,1) = [0.0d0, 0.0d0, C_W]
  F_B_gravity = MATMUL(T_BG, Weight_vec)


  !=======================================================================
  !                             LOAD COEFFICIENTS
  !=======================================================================
  call get_hub_loads()

  C_H = 0.0d0
  C_Y = 0.0d0
  C_T = 0.0d0
  C_MX = 0.0d0
  C_MY = 0.0d0
  C_MZ = 0.0d0

  const  = 3.0d0 * sigma * Cl_alpha / (2.0d0 * 4 * atan (1.0_8) * Lock_no * Nb)


  do i = 1, size(time)-1, 1
    C_H = C_H + (time(i+1) - time(i))* 0.5d0 * (F_XH(i) + F_XH(i+1))
    C_Y = C_Y + (time(i+1) - time(i))* 0.5d0 * (F_YH(i) + F_YH(i+1))
    C_T = C_T + (time(i+1) - time(i))* 0.5d0 * (F_ZH(i) + F_ZH(i+1))
    C_MX = C_MX + (time(i+1) - time(i))* 0.5d0 * (M_XH(i) + M_XH(i+1))
    C_MY = C_MY + (time(i+1) - time(i))* 0.5d0 * (M_YH(i) + M_YH(i+1))
    C_MZ = C_MZ + (time(i+1) - time(i))* 0.5d0 * (M_ZH(i) + M_ZH(i+1))
  end do

  C_H = const * C_H
  C_Y = const * C_Y
  C_T = const * C_T
  C_MX = const * C_MX
  C_MY = const * C_MY
  C_MZ = const * C_MZ



  T_BH(1, :) = [-1.0d0, 0.0d0, 0.0d0]
  T_BH(2, :) = [0.0d0, 1.0d0, 0.0d0]
  T_BH(3, :) = [0.0d0, 0.0d0, -1.0d0]

  !=======================================================================
  !                           Residual Calculations
  !=======================================================================

  force_vec(:,1) = [C_H, C_Y, C_T]
  moment_vec(:,1) = [C_MX, C_MY, C_MZ]
  F_B_rotor = MATMUL(T_BH, force_vec)
  F_B = F_B_gravity + F_B_rotor + F_B_A_fuselage

  M_B_gravity(1,1) = ycg * F_B_gravity(3,1) - hcg * F_B_gravity(2,1)
  M_B_gravity(2,1) = hcg * F_B_gravity(1,1) - xcg * F_B_gravity(3,1)
  M_B_gravity(3,1) = xcg * F_B_gravity(2,1) - ycg * F_B_gravity(1,1)

  M_B_rotor = MATMUL(T_BH, moment_vec)
  M_B = M_B_gravity + M_B_A_fuselage + M_B_I_fuselage + M_B_rotor


  lambda_residual = trim_vec(6) - mu * tan(trim_vec(1) + beta_1c + theta_fp) &
                    - (k_f * C_T / (2.0d0 * SQRT(mu*mu + trim_vec(6)*trim_vec(6))))


  residual(1:3) = F_B(:,1)
  residual(4:5) = M_B(1:2,1)
  residual(6) = lambda_residual

end subroutine trim_residual

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the coupled trim solution with an intial
!> guess of trim parameters.
!                           ~         ~        ~
!                                  trim_vec
!                           ~         ~        ~
!=======================================================================
subroutine trim()
  use trim_residuals
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                               :: alpha_s, phi_s, theta_0, theta_1c, &
                                                    theta_1s, lambda, residual(n_trim), &
                                                    Jacobian(n_trim, n_trim), delta_X(n_trim), &
                                                    Jacobian_temp(n_trim, n_trim)

  integer                                        :: stop_fac, niter, i, info, ipiv(n_trim)


  !=======================================================================
  !                             Initial Guesses
  !=======================================================================

  call rigid_blade_trim(alpha_s, phi_s, theta_0, theta_1c, theta_1s, lambda)

  if (.not. allocated(trim_vec)) allocate(trim_vec(n_trim))

  trim_vec(1) = alpha_s
  trim_vec(2) = phi_s
  trim_vec(3) = theta_0
  trim_vec(4) = theta_1c
  trim_vec(5) = theta_1s
  trim_vec(6) = lambda

  stop_fac = 0
  niter = 20

  do i = 1, niter, 1
    print  *, '------------------------------------------------------------'
    print *, 'Coupled Trim Iteration number', i

    call trim_residual(residual)


    if ( i == 1 .or. i == 6 .or. i == 11 .or. i == 16 ) then
      print *, 'Jacobian Computation started'
      call compute_Jacobian(residual, Jacobian)
      print *, 'Jacobian Computation ended'
      Jacobian_temp = Jacobian
    end if

    if ( i > 1 ) then
      Jacobian = Jacobian_temp
    end if

    print *, '  The L2 norm of the residual is', NORM2(residual)

    if ( NORM2(residual) < 0.0000001d0) then
      stop_fac = 1
    end if

    delta_X = residual

    !=======================================================================
    !                             Solve for delta_x
    !=======================================================================

    call DGESV(n_trim, 1, Jacobian, n_trim, ipiv, delta_X, n_trim, info)


    trim_vec = trim_vec - delta_X

    print  *, '------------------------------------------------------------'


    if ( stop_fac == 1 ) then
      print *, 'The trim values are :'
      print *, 'alpha_s    =   ', trim_vec(1)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'phi_s      =   ', trim_vec(2)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_0    =   ', trim_vec(3)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_1c   =   ', trim_vec(4)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_1s   =   ', trim_vec(5)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'lambda     =   ', trim_vec(6)
      print  *, '------------------------------------------------------------'
      print *, 'The Force Coefficients are :'
      print *, 'C_H    =   ', C_H
      print *, 'C_Y    =   ', C_Y
      print *, 'C_T    =   ', C_T
      print *, 'C_MX   =   ', C_MX
      print *, 'C_MY   =   ', C_MY
      print *, 'C_MZ   =   ', C_MZ
      exit
    end if

    print  *, '------------------------------------------------------------'

  end do

end subroutine trim

!-------------------------------------------------------------------------------

  !=======================================================================
  !> This subroutine computes the lift coefficient distribution (NOT USED)
  !=======================================================================
subroutine compute_circulation()
  use finite_elements
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  use my_funcs
  implicit none


  double precision :: x_vec(NSEG+1), x_ctr(NSEG), azim_vec(adim), r1, t1, w, w_dash, w_dot, w_dot_dash, &
                      L_u_aero, L_v_aero, L_w_aero, M_phi_bar, q_vec(nodes, dof_per_element), u_R, u_P, u_T, &
                      alpha_vec(NSEG, adim)

  double precision :: ut(NSEG, adim), up(NSEG, adim), x_root, x_tip, rcout, C_P, lambda, Cl(NSEG)



  integer :: i, j, idx


  lambda = trim_vec(6)

  q_input = 10.0d0
  q_output = 0.0d0
  if ( aerodynamic_model == 2 ) then
    do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
      q_input = q_output
      call aero_code()
    end do
  else if (aerodynamic_model == 1) then
    q_input = q_output
    call aero_code()
  else
    print *, 'No Such Aerodynamic Model exists!!!'
  end if

  q_input = q_output

  call linspace(0.0d0, R, NSEG+1, x_vec)

  do i = 1, NSEG, 1
    x_ctr(i) = (x_vec(i) + x_vec(i+1))*0.5
  end do


  call linspace(0.0d0, T, adim, azim_vec)

  do i = 1, NSEG, 1
    do j = 1, adim, 1

      !=======================================================================
      !                Compute w, w_dash, w_dot, w_dot_dash
      !=======================================================================
      call find_location(x_ctr(i), azim_vec(j), q_output, q_vec, r1, t1)
      call compute_w(x_ctr(i), azim_vec(j), q_vec, r1, t1, w, w_dash, w_dot, w_dot_dash)

      call compute_velocities(x_ctr(i), azim_vec(j), w, w_dash, w_dot, w_dot_dash, &
                              u_R, u_P, u_T, L_u_aero, L_v_aero, L_w_aero, M_phi_bar)

      ut(i, j) = u_T
      up(i, j) = u_P


    end do
  end do


  alpha_vec = atan2(-up, ut)

  Cl = Cl_alpha * alpha_vec(:,1)

  print *, Cl

  C_P = 0.0d0
  do idx = 1, size(x_ctr), 1
    C_P = C_P + (x_ctr(2) - x_ctr(1))* (0.5d0 * sigma * Cl(idx) * x_ctr(idx)*x_ctr(idx) * lambda)
  end do

  print *, 'Induced Power', C_P


end subroutine compute_circulation

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the Jacobian used in the trim procedure.
!                           ~         ~        ~
!                                  Jacobian
!                           ~         ~        ~
!=======================================================================
subroutine compute_Jacobian(residual, Jacobian)
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                   :: residual(n_trim)

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: Jacobian(n_trim, n_trim)

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                               :: delta, residual_new(n_trim)

  integer                                        :: i



  delta = 0.00001d0

  do i = 1, n_trim, 1
    trim_vec(i) = trim_vec(i) + delta
    call trim_residual(residual_new)
    Jacobian(:,i) = (residual_new - residual) / (delta)
    trim_vec(i) = trim_vec(i) - delta
  end do

end subroutine compute_Jacobian

!-------------------------------------------------------------------------------

  !=======================================================================
  !> This subroutine computes the rigid blade trim guesses
  !> Was very lazy to code the exact rigid blade trim equations.
  !> So just hard coded some of the Tyler inputs. (Sorry!)
  !=======================================================================

subroutine rigid_blade_trim(alpha_s, phi_s, theta_0, theta_1c, theta_1s, lambda)
  use trim_parameters
  implicit none


  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: alpha_s, phi_s, theta_0, &
                                                    theta_1c, theta_1s, lambda


  !=======================================================================
  !                             Main Code
  !=======================================================================

  if (abs(mu - 0.3) < 1e-7) then
    alpha_s = 0.575d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -0.570d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.32d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.16d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -4.17d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0134d0
  else if (abs(mu - 0.0) < 1e-7) then
    alpha_s = 0.0d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -0.0d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 9.08d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.0d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -0.0d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0627d0
  else if (abs(mu - 0.4) < 1e-7) then
    alpha_s = 0.835d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = 0.0043d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 7.03d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.39d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -5.70d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0141d0
  else if (abs(mu - 0.1) < 1e-7) then
    alpha_s = 0.171d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -2.08d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.82d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.453d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -1.50d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0289d0
  else if (abs(mu - 0.2) < 1e-7) then
    alpha_s = 0.357d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -1.15d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.05d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.834d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -2.77d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0163d0
  else
    alpha_s = 0.835d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = 0.0043d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 7.03d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.39d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -5.70d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0141d0
  end if

end subroutine rigid_blade_trim

!-------------------------------------------------------------------------------
