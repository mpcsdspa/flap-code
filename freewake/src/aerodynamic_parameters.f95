module aerodynamic_parameters
  implicit none


 !=======================================================================
 !                          Dimensional Parameters
 !======================================================================


  ! Radius of the rotor blade, R
     real(kind=8), parameter :: Cl_o = 0.0d0
  ! Blade Stiffness, EI
     real(kind=8), parameter :: Cl_alpha = 5.7d0
  ! Mass per length, m0
     real(kind=8), parameter :: Cdo = 0.01d0
  ! Rotating Frequency (rad/s), omega
     real(kind=8), parameter :: Cd1 = 0.0d0
  ! Hinge offset from the hub (non-dimensional wrt R), 0.04 - Articulated
     real(kind=8), parameter :: Cd2 = 0.0d0
  ! Rotating Frequency (rad/s), omega
     real(kind=8), parameter :: Cmac = 0.0d0
  ! Hinge offset from the hub (non-dimensional wrt R), 0.04 - Articulated
     real(kind=8), parameter :: f1 = 0.0d0
  ! Rotating Frequency (rad/s), omega
     real(kind=8), parameter :: Lock_no = 8.0d0
  ! Hinge offset from the hub (non-dimensional wrt R), 0.04 - Articulated
  ! Precone Angle, beta_p
     real(kind=8), parameter :: beta_p = 0.0d0
  ! Center of Lift from elastic axis, e_d/R
     real(kind=8), parameter :: e_d = 0.0d0
  ! Center of gravity from elastic axis, e_g/R
     real(kind=8), parameter :: e_g = 0.0d0
  ! Center of gravity location, xcg
     real(kind=8), parameter :: xcg = 0.0d0
     real(kind=8), parameter :: ycg = 0.0d0
     real(kind=8), parameter :: hcg = 0.2d0

     real(kind=8), parameter :: C_W = 0.00595d0

     real(kind=8), parameter :: C_mx_f = 0.0d0
     real(kind=8), parameter :: C_my_f = 0.0d0
     real(kind=8), parameter :: C_mz_f = 0.0d0

     real(kind=8), parameter :: X_ac = 0.0d0
     real(kind=8), parameter :: Y_ac = 0.0d0
     real(kind=8), parameter :: H_ac = 0.0d0

     real(kind=8), parameter :: f_d = 0.0d0 !equivalent flat plate area

     real(kind=8), parameter :: theta_fp = 0.0d0
     real(kind=8), parameter :: beta_1c = 0.0d0

     real(kind=8), parameter :: k_f = 1.0d0


     integer, parameter :: n_trim = 5

     real(kind=8), parameter :: theta_tw = 0.0d0


end module aerodynamic_parameters
