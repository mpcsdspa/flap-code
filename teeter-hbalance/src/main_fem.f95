!=======================================================================
!> This subroutine computes the Mass and Stiffness matrices
!> for a rotor using FEM with or without Modal reduction.
!> It also uses other subroutines which are present below.
!                           ~             ~        ~
!                             M_bar, K_bar, Modes_bar
!                           ~             ~        ~
!=======================================================================
subroutine main_fem(rotor_type, flag_modal_reduction)
  use finite_elements
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  integer, intent(IN)                                 :: flag_modal_reduction, rotor_type

  !=======================================================================
  !                          Local variables
  !=======================================================================
  double precision                                    :: l
  integer                                             :: i, j, p, max_modes
  double precision, allocatable, dimension(:)         :: mass, Freqs
  integer, allocatable, dimension(:)                  :: b
  double precision, allocatable, dimension(:,:)       :: M, K, m1, k1, Modes, Modes_bar
  character(len=10)                                   :: name
  double precision                                    :: M_bar(num_modes, num_modes), &
                                                         K_bar(num_modes, num_modes)

  !=======================================================================
  !                        Blade Parmeters (Read from module)
  !=======================================================================
  allocate(mass(ne))
  l                    = R*(1 - offset)/dble(ne)
  mass                 = m0

  if (rotor_type == 1) then
    max_modes = 2*ne
    allocate(Modes_bar(2*ne, num_modes))
  else
    max_modes = 2*ne+1
    allocate(Modes_bar(2*ne+1, num_modes))
  end if

  !=======================================================================
  !                         Allocate Local variables
  !=======================================================================
  allocate(Freqs(max_modes))

  !=======================================================================
  !                             Inputs
  !=======================================================================
  allocate(Modes(max_modes, max_modes))
  allocate(b(max_modes))
  allocate(M(2*ne+2, 2*ne+2))
  allocate(K(2*ne+2, 2*ne+2))

  !=======================================================================
  !                         Elemental Degree of Freedom
  !=======================================================================
  if (.not. allocated(element_dofs)) then
    allocate(element_dofs(ne, dof_per_element))

    do i = 1, ne, 1
      p = 2*i - 1
      do j = 1, dof_per_element, 1
        element_dofs(i,j) = p
        p = p + 1
      end do
    end do

  end if

  allocate(m1(size(element_dofs(1,:)) , size(element_dofs(1,:))))
  allocate(k1(size(element_dofs(1,:)) , size(element_dofs(1,:))))

  !=======================================================================
  !                         Elemental coordinates
  !=======================================================================

  if (.not. allocated(r_left)) then
    allocate(r_left(ne))
    allocate(r_right(ne))
    do i = 1, ne, 1
      r_left(i)  = (i-1)*l + offset*R
      r_right(i) = i*l + offset*R
    end do
  end if

  !=======================================================================
  !                         Rotor Boundary Conditions
  !=======================================================================

  if (.not. allocated(b_hin)) then
    allocate(b_hin(2*ne))
  end if

  if (.not. allocated(b_art)) then
    allocate(b_art(2*ne+1))
  end if

  if (rotor_type == 1) then
    do i = 3, 2*ne+2, 1
      b(i-2) = i
    end do
    b_hin = b
  else if (rotor_type == 2) then
    do i = 2, 2*ne+2, 1
      b(i-1) = i
    end do
    b_art = b
  else
    print *, "Wrong Rotor Type"
  end if

  !=======================================================================
  !                         Main code
  !=======================================================================
  M = 0.0d0
  K = 0.0d0
  do i = 1, ne, 1
    call compute_mass_matrix(mass, EI, dof_per_element, r_left, r_right, i, ne, omega, m1, k1)
    M(element_dofs(i,:),element_dofs(i,:)) = M(element_dofs(i,:), element_dofs(i,:)) + m1;
    K(element_dofs(i,:),element_dofs(i,:)) = K(element_dofs(i,:), element_dofs(i,:)) + k1;
  end do

  if ( flag_modal_reduction == 0 ) then
    call compute_modes(max_modes, M(b,b), K(b,b), Freqs, Modes)
    M_bar = M(b,b)
    K_bar = K(b,b)
    Modes_bar = Modes
  else if ( flag_modal_reduction == 1) then
    call compute_modes(max_modes, M(b,b), K(b,b), Freqs, Modes)
    call modal_reduction(ne, num_modes, max_modes, M(b,b), K(b,b), Modes, M_bar, K_bar, Modes_bar)
  end if


  !=======================================================================
  !                         Hingeless Rotor
  !=======================================================================
  if (rotor_type == 1) then
    M_hin = M_bar
    K_hin = K_bar
    V_hin = Modes_bar
  else
  !=======================================================================
  !                         Articulated Rotor
  !=======================================================================
    M_art = M_bar
    K_art = K_bar
    V_art = Modes_bar
  end if

end subroutine main_fem


!---------------------------------------------------------
subroutine modal_reduction(ne, num_modes, max_modes, M, K, Modes, M_bar, K_bar, Modes_bar)
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  integer, intent(IN)                                               :: num_modes, ne, max_modes
  double precision, dimension(max_modes, max_modes), intent(IN)               :: Modes, M, K

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, dimension(num_modes, num_modes), intent(INOUT)  :: M_bar, K_bar
  double precision, dimension(max_modes, num_modes), intent(OUT)         :: Modes_bar


  !=======================================================================
  !                             Modal Reduction Code
  !                        M_bar = V' M V, K_bar = V' K V
  !=======================================================================
  Modes_bar = Modes(:, 1:num_modes)
  M_bar = MATMUL(transpose(Modes_bar), MATMUL(M, Modes_bar))
  K_bar = MATMUL(transpose(Modes_bar), MATMUL(K, Modes_bar))


end subroutine modal_reduction


!---------------------------------------------------------

subroutine mode_shape(rotor_type, max_modes, Modes, r_left, r_right, element_dofs, mode_num, n_interior, r_vec, phi)
  use my_funcs
  use hermite
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  integer, intent(IN)                                           :: mode_num, n_interior, rotor_type, max_modes
  double precision, dimension(ne), intent(IN)                   :: r_left, r_right
  double precision, dimension(max_modes, max_modes), intent(IN) :: Modes
  integer, dimension(ne, dof_per_element), intent(IN)           :: element_dofs

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, dimension(ne*n_interior), intent(OUT)       :: r_vec, phi

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision, allocatable, dimension(:)                   :: s, h_func, q
  double precision                                              :: l, w
  integer                                                       :: i, j, count
  double precision, dimension(max_modes)                        :: q1_temp
  double precision, dimension(2*ne+2)                           :: q1



  l = R/dble(ne)
  count = 1
  q1_temp = Modes(:, mode_num)


  !=======================================================================
  !                             Removing Boundary Conditions
  !=======================================================================
  if (rotor_type == 1) then
    do i = 3, 2*ne+2, 1
      q1(i) = q1_temp(i-2)
    end do
    q1(1) = 0.0d0
    q1(2) = 0.0d0
  else if (rotor_type == 2) then
    do i = 2, 2*ne+2, 1
      q1(i) = q1_temp(i-1)
    end do
    q1(1) = 0.0d0
  end if


  allocate(s(n_interior))
  allocate(h_func(dof_per_element))
  allocate(q(dof_per_element))

  !=======================================================================
  !                             Main Code (Interpolation)
  !=======================================================================
  do i = 1, ne, 1
    call linspace(r_left(i), r_right(i), n_interior, s)
    q = q1(element_dofs(i, :))
    do j = 1, n_interior, 1
      call Hermite_Funcs(s(j)-r_left(i), l, h_func)
      w = dot_product(h_func, q)
      phi(count) = w
      r_vec(count) = s(j)
      count = count + 1
    end do
  end do

end subroutine mode_shape


!---------------------------------------------------------


subroutine compute_mass_matrix(mass, EI, dof_per_element, r_left, r_right, element_num, ne, omega, M, K)
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                                              :: EI, omega
  integer, intent(IN)                                                       :: dof_per_element, element_num, ne
  double precision, dimension(ne), intent(IN)                               :: r_left, r_right, mass

  !=======================================================================
  !                               Outputs
  !=======================================================================
  double precision, dimension(dof_per_element,dof_per_element), intent(OUT) :: M, K

  !=======================================================================
  !                            Local variables
  !=======================================================================
  double precision                                                          :: A, l, ri, val
  integer                                                                   :: i, j, np
  double precision, dimension(dof_per_element,dof_per_element)              :: K1, K2, K3


  !=======================================================================
  !                             Main Code
  !=======================================================================
  A = 0.0d0
  do i = element_num, ne
    A = A + mass(i) * (r_right(i)**2 - r_left(i)**2)
  end do
  l = r_right(element_num) - r_left(element_num)
  ri = r_left(element_num)
  np = 6


  do i = 1, dof_per_element, 1
    do j = 1, dof_per_element, 1
      call numerical_integral(mass, EI, element_num, 0.0d0, l, i, j, np, ne, ri, 'mass      ', val)
      M(i,j) = val
      call numerical_integral(mass, EI, element_num, 0.0d0, l, i, j, np, ne, ri, 'stiffness1', val)
      K1(i,j)  = val
      call numerical_integral(m, EI, element_num, 0.0d0, l, i, j, np, ne, ri, 'stiffness2', val)
      K2(i,j) = (0.5*omega*omega*A) * val
      call numerical_integral(m, EI, element_num, 0.0d0, l, i, j, np, ne, ri, 'stiffness3', val)
      K3(i,j) = 0.5*(mass(element_num) * omega*omega) * val
    end do
  end do
  K = K1 + K2 - K3;
end subroutine compute_mass_matrix


!---------------------------------------------------------



subroutine numerical_integral(m, EI, element_num, min, max, i, j, np, ne, ri, flag, integral)
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)                     :: EI, ri, min, max
  integer, intent(IN)                              :: element_num, i, j, np, ne
  double precision, dimension(ne), intent(IN)      :: m
  character(len=10), intent(IN)                    :: flag

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                    :: integral

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                                 :: l, val
  integer                                          :: k
  double precision, dimension(np)                  :: t, w, x


  !=======================================================================
  !                             Main Code
  !=======================================================================
  l = max - min
  t = [-0.932469, -0.6612093, -0.23861918, 0.23861918, 0.6612093, 0.932469]
  w = [0.1713244, 0.36076157, 0.4679139, 0.4679139, 0.36076157, 0.1713244];
  x = 0.5*((max + min) +  t*(max - min));

  integral = 0.0d0
  do k = 1, np, 1
    if (flag(1:4) == 'mass') then
      call compute_function(m(element_num), x(k), i, j, l, ri, flag, val)
      integral = integral + w(k)*val
    else if (flag == 'stiffness1') then
      call compute_function(EI, x(k), i, j, l, ri, flag, val)
      integral = integral + w(k)* val
    else if (flag == 'stiffness2') then
      call compute_function(1.0d0, x(k), i, j, l, ri, flag, val)
      integral = integral + w(k)* val
    else if (flag == 'stiffness3') then
      call compute_function(1.0d0, x(k), i, j, l, ri, flag, val)
      integral = integral + w(k)*val
    end if
  end do

  integral = integral * 0.5 * l

end subroutine numerical_integral

!---------------------------------------------------------


subroutine compute_function(const, x, i, j, l, ri, flag, val)
  use hermite
  implicit none
  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN)        :: const, x, l, ri
  integer, intent(IN)                 :: i, j
  character(len=10), intent(IN)       :: flag

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)       :: val

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                    :: h1, h2

  !=======================================================================
  !                             Main Code
  !=======================================================================
  if ( flag == 'stiffness1' ) then
    call hermite_second_der(x,i,l,h1)
    call hermite_second_der(x,j,l,h2)
    val = const * h1 * h2
  else if (flag == 'stiffness2') then
    call hermite_first_der(x,i,l,h1)
    call hermite_first_der(x,j,l,h2)
    val = const * h1 * h2
  else if (flag == 'stiffness3') then
    call hermite_first_der(x,i,l,h1)
    call hermite_first_der(x,j,l,h2)
    val = (x*x + 2*ri*x) * h1 * h2
  else if (flag(1:4) == 'mass') then
    call hermite_function(x,i,l,h1)
    call hermite_function(x,j,l,h2)
    val = const * h1 * h2
  else if (flag == 'force') then
    call hermite_function(x,i,l,h1)
    val = const * h1
  end if

end subroutine compute_function


!--------------------------------------------------------------------
subroutine  outputra1(ra, m, n)
implicit none
!will output an real matrix of size MxN nicely
integer                               :: m, n, row,col
double precision,dimension(m,n)             :: ra
character                             :: reply*1
character(len=10)                     :: Format
Format = "(3I69)"
do row =1,m
  write(*,10) (ra(row,col),col=1,n)
  10    format(4f8.5)
   !as we don't know how many numbers are to be output,
   !specify  !more than we need - the rest are ignored
end do
print*,'__________________________________________________'
print*,'Hit a key and  press enter to continue'
read *,reply
end subroutine  outputra1
!---------------------------------------------------------
