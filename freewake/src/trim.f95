!=======================================================================
!> This subroutine computes the residual vector of trim for a given
!> trim parameters.
!                           ~         ~        ~
!                                  residual
!                           ~         ~        ~
!=======================================================================

subroutine trim_residual(residual)
  use loads
  use trim_residuals
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none



  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT)                  :: residual(n_trim)

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision                               :: T_BG(3,3), T_BH(3,3), F_B(3, 1), &
                                                    F_B_A_fuselage(3,1), M_B_A_fuselage(3,1), &
                                                    M_B_I_fuselage(3, 1), F_B_gravity(3,1), &
                                                    Weight_vec(3, 1), force_vec(3,1), moment_vec(3,1), &
                                                    F_B_rotor(3, 1), M_B_gravity(3, 1), &
                                                    M_B_rotor(3, 1), M_B(3, 1)

  double precision                               :: lambda_residual, alpha, phi, const

  integer                                        :: i


  !=======================================================================
  !                             Main Code
  !=======================================================================


  F_B_A_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]
  M_B_I_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]
  M_B_A_fuselage(:,1) = [0.0d0, 0.0d0, 0.0d0]

  alpha = trim_vec(1)
  phi = trim_vec(2)

  T_BG(1, :) = [cos(alpha), 0.0d0, sin(alpha)]
  T_BG(2, :) = [-sin(alpha)*sin(phi), cos(phi), cos(alpha)*sin(phi)]
  T_BG(3, :) = [-sin(alpha)*cos(phi), -sin(phi), cos(alpha)*cos(phi)]

  Weight_vec(:,1) = [0.0d0, 0.0d0, C_W]
  F_B_gravity = MATMUL(T_BG, Weight_vec)


  !=======================================================================
  !                             LOAD COEFFICIENTS
  !=======================================================================
  call get_hub_loads()

  C_H = 0.0d0
  C_Y = 0.0d0
  C_T = 0.0d0
  C_MX = 0.0d0
  C_MY = 0.0d0
  C_MZ = 0.0d0

  const  = 3.0d0 * sigma * Cl_alpha / (2.0d0 * 4 * atan (1.0_8) * Lock_no * Nb)


  do i = 1, size(time)-1, 1
    C_H = C_H + (time(i+1) - time(i))* 0.5d0 * (F_XH(i) + F_XH(i+1))
    C_Y = C_Y + (time(i+1) - time(i))* 0.5d0 * (F_YH(i) + F_YH(i+1))
    C_T = C_T + (time(i+1) - time(i))* 0.5d0 * (F_ZH(i) + F_ZH(i+1))
    C_MX = C_MX + (time(i+1) - time(i))* 0.5d0 * (M_XH(i) + M_XH(i+1))
    C_MY = C_MY + (time(i+1) - time(i))* 0.5d0 * (M_YH(i) + M_YH(i+1))
    C_MZ = C_MZ + (time(i+1) - time(i))* 0.5d0 * (M_ZH(i) + M_ZH(i+1))
  end do

  C_H = const * C_H
  C_Y = const * C_Y
  C_T = const * C_T
  C_MX = const * C_MX
  C_MY = const * C_MY
  C_MZ = const * C_MZ



  T_BH(1, :) = [-1.0d0, 0.0d0, 0.0d0]
  T_BH(2, :) = [0.0d0, 1.0d0, 0.0d0]
  T_BH(3, :) = [0.0d0, 0.0d0, -1.0d0]

  force_vec(:,1) = [C_H, C_Y, C_T]
  moment_vec(:,1) = [C_MX, C_MY, C_MZ]
  F_B_rotor = MATMUL(T_BH, force_vec)
  F_B = F_B_gravity + F_B_rotor + F_B_A_fuselage

  M_B_gravity(1,1) = ycg * F_B_gravity(3,1) - hcg * F_B_gravity(2,1)
  M_B_gravity(2,1) = hcg * F_B_gravity(1,1) - xcg * F_B_gravity(3,1)
  M_B_gravity(3,1) = xcg * F_B_gravity(2,1) - ycg * F_B_gravity(1,1)

  M_B_rotor = MATMUL(T_BH, moment_vec)
  M_B = M_B_gravity + M_B_A_fuselage + M_B_I_fuselage + M_B_rotor


  residual(1:3) = F_B(:,1)
  residual(4:5) = M_B(1:2,1)

end subroutine trim_residual

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the coupled trim solution with an intial
!> guess of trim parameters.
!                           ~         ~        ~
!                                  trim_vec
!                           ~         ~        ~
!=======================================================================
subroutine trim()
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  use freewake_interface
  use constant_parameters
  use precision
  use my_funcs
  implicit none

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision              :: alpha_s, phi_s, theta_0, theta_1c, &
                                   theta_1s, lambda, residual(n_trim), &
                                   Jacobian(n_trim, n_trim), delta_X(n_trim), &
                                   Jacobian_temp(n_trim, n_trim), Cl(NSEG, adim), &
                                   Cd(NSEG, adim), alpha_vec(NSEG, adim)

  integer                       :: stop_fac, niter, i, info, ipiv(n_trim), k, aa, j, cc, idx


  double precision              :: Gamma(NSEG, adim), beta(adim), beta_dot(adim), &
                                   old_inflow, new_inflow, old_t0, new_t0, Cl_temp(NSEG), ld(NSEG), &
                                   C_P, r_vec(NSEG+1), x_ctr(NSEG), dCp(NSEG)


  type(FreewakeDef) :: Free_wake

  call assign_constants()

  !=======================================================================
  !                             Initial Guesses
  !=======================================================================

  call rigid_blade_trim(alpha_s, phi_s, theta_0, theta_1c, theta_1s, lambda)

  if (.not. allocated(trim_vec)) allocate(trim_vec(n_trim))

  trim_vec(1) = alpha_s
  trim_vec(2) = phi_s
  trim_vec(3) = theta_0
  trim_vec(4) = theta_1c
  trim_vec(5) = theta_1s

  niter = 20


  call compute_inflow1()

  old_t0 = 100.0d0
  new_t0 = theta_0

  do k = 1, 6, 1 !Free wake loop

  ! do while(abs(old_t0 - new_t0) > 1e-2)

  stop_fac = 0

  do i = 1, niter, 1
    print  *, '------------------------------------------------------------'
    print *, 'Coupled Trim Iteration number', i

    call trim_residual(residual)


    if ( i == 1 .or. i == 6 .or. i == 11 .or. i == 16 ) then
      print *, 'Jacobian Computation started'
      call compute_Jacobian(residual, Jacobian)
      print *, 'Jacobian Computation ended'
      Jacobian_temp = Jacobian
    end if

    if ( i > 1 ) then
      Jacobian = Jacobian_temp
    end if

    print *, '  The L2 norm of the residual is', NORM2(residual)

    if ( NORM2(residual) < 0.0000001d0) then
      stop_fac = 1
    end if

    delta_X = residual



    call DGESV(n_trim, 1, Jacobian, n_trim, ipiv, delta_X, n_trim, info)


    trim_vec = trim_vec - delta_X

    print  *, '------------------------------------------------------------'


    if ( stop_fac == 1 ) then
      print *, 'The trim values are :'
      print *, 'alpha_s    =   ', trim_vec(1)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'phi_s      =   ', trim_vec(2)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_0    =   ', trim_vec(3)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_1c   =   ', trim_vec(4)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      print *, 'theta_1s   =   ', trim_vec(5)*180.0d0/(4 * atan (1.0_8)), 'degrees'
      exit
    end if

    print  *, '------------------------------------------------------------'

  end do

  old_t0 = new_t0
  new_t0 = theta_0
  new_inflow = SUM(lambda_vec(:,1))/NSEG
  old_inflow = 10000.0d0;
  cc = 1;

! do cc = 1, 4, 1
  do while (abs(new_inflow - old_inflow) > 1e-4)


    call compute_circulation(Gamma, beta, beta_dot, alpha_vec)

    Gamma = Gamma * R_dim* R_dim * omega_dim



    Cl = Cl_alpha * alpha_vec
    Cd = Cdo + Cd1 * alpha_vec + Cd2 * alpha_vec*alpha_vec


    open(unit = 1, file='../txt_files/lift_coeff')
    do i = 1, adim , 1
      write(1,*) Cl(:,1)
    end do
    close(1)

    open(unit = 1, file='../txt_files/angle_of_attack')
    do i = 1, adim , 1
      write(1,*) alpha_vec(:,1)
    end do
    close(1)


    call initialize_free_wake(Gamma, beta, beta_dot, Free_wake)


     if (cc > 1) Free_wake % qinitial = 'w'


    call wake_solver(Free_wake)


    open(unit = 1, file='../txt_files/viz')
    do i = 1, adim , 1
      write(1,*) Free_wake%Fw_rtr(1)%ViZ2(:, i)
    end do
    close(1)


    old_inflow = new_inflow

    call compute_inflow(Free_wake)



    new_inflow = SUM(lambda_vec(:,1))/NSEG


    print *, 'Average of inflow', SUM(lambda_vec(:,1))/NSEG

    open(unit = 1, file='../txt_files/inflow')
      do j = 1, adim, 1
        write(1,*) lambda_vec(:, j)
      end do
    close(1)

    open(unit = 1, file='../txt_files/gamma')
      do j = 1, adim, 1
        write(1,*) Gamma(:, j)
      end do
    close(1)

    cc = cc+ 1;

    !!!!!!!!!!!! ADDED BY MRINAL

      Cl_temp(:) = Cl(:,1)
      ld(:) = lambda_vec(:,1)

      call linspace(0.2d0, R, NSEG+1, r_vec)

      do i = 1, NSEG, 1
        x_ctr(i) = (r_vec(i) + r_vec(i+1))*0.5
      end do

      print *, C_P

      C_P = 0.0d0
      do idx = 1, size(x_ctr), 1
        C_P = C_P + (x_ctr(2) - x_ctr(1))* (0.5d0 * sigma * Cl_temp(idx) * x_ctr(idx)*x_ctr(idx) * ld(idx))
      end do

      print *, 'Induced Power calculated ----', C_P




  end do

end do

end subroutine trim

!-------------------------------------------------------------------------------
! Only called once at the start
subroutine compute_inflow1()

  use trim_parameters
  use my_funcs
  use blade_parameters
  implicit none

  integer :: i

  lambda_vec = 0.00585d0


end subroutine compute_inflow1


!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------


subroutine compute_inflow(Free_wake)

  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  use freewake_interface
  implicit none


  type(FreeWakeDef), intent(IN) :: Free_wake

  double precision :: alpha_s

  alpha_s = trim_vec(1)


  lambda_vec = mu*tan(alpha_s + beta_1c + theta_fp) - (Free_wake%Fw_rtr(1)%ViZ2(1:NSEG, 1:adim))/(omega_dim*R_dim)


end subroutine compute_inflow


!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------

! This subroutine computes the circulation(gamma) at linearly spaced points both
! in azimuth and radius. It also computes the flapping angle(beta) and flapping
! velocity(beta_dot) at linearly spaced azimuth points.

subroutine compute_circulation(Gamma, beta, beta_dot, alpha_vec)

  use finite_elements
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  use my_funcs
  implicit none


  double precision, intent(OUT) :: Gamma(NSEG, adim), beta(adim), beta_dot(adim), alpha_vec(NSEG, adim)

  double precision :: x_vec(NSEG+1), x_ctr(NSEG), azim_vec(adim), r1, t1, w, w_dash, w_dot, w_dot_dash, &
                      L_u_aero, L_v_aero, L_w_aero, M_phi_bar, q_vec(nodes, dof_per_element), u_R, u_P, u_T

  double precision :: ut(NSEG, adim), up(NSEG, adim), x_root, x_tip



  integer :: i, j


  q_input = 10.0d0
  q_output = 0.0d0
  if ( aerodynamic_model == 2 ) then
    do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
      q_input = q_output
      call aero_code()
    end do
  else if (aerodynamic_model == 1) then
    q_input = q_output
    call aero_code()
  else
    print *, 'No Such Aerodynamic Model exists!!!'
  end if


  q_input = q_output



  call linspace(rcout, R, NSEG+1, x_vec)

  do i = 1, NSEG, 1
    x_ctr(i) = (x_vec(i) + x_vec(i+1))*0.5
  end do


  call linspace(0.0d0, T, adim, azim_vec)

  do i = 1, NSEG, 1
    do j = 1, adim, 1

      !=======================================================================
      !                Compute w, w_dash, w_dot, w_dot_dash
      !=======================================================================
      call find_location(x_ctr(i), azim_vec(j), q_output, q_vec, r1, t1)
      call compute_w(x_ctr(i), azim_vec(j), q_vec, r1, t1, w, w_dash, w_dot, w_dot_dash)

      call compute_velocities(x_ctr(i), azim_vec(j), w, w_dash, w_dot, w_dot_dash, &
                              u_R, u_P, u_T, L_u_aero, L_v_aero, L_w_aero, M_phi_bar)
      !

      ut(i, j) = u_T
      up(i, j) = u_P

    end do
  end do


  open(unit = 1, file='../txt_files/up')
  do i = 1, adim , 1
    write(1,*) up(:,i)
  end do
  close(1)

  open(unit = 1, file='../txt_files/ut')
  do i = 1, adim , 1
    write(1,*) ut(:,i)
  end do
  close(1)

  alpha_vec = atan2(-up, ut)
  ! alpha_vec(:,1) = trim_vec(3)  - lambda_vec(:,1) / x_ctr

  Gamma = 0.5d0 * chord * Cl_alpha * omega * R * SQRT(up*up + ut*ut) * atan2(-up,ut)


  x_root = offset
  x_tip = R

  ! print *, azim_vec(1), azim_vec(adim)

  do i = 1, adim, 1

    call find_location(x_tip, azim_vec(i), q_output, q_vec, r1, t1)
    call compute_w(x_tip, azim_vec(i), q_vec, r1, t1, w, w_dash, w_dot, w_dot_dash)

    beta(i) = atan(w/(x_tip - x_root))

    beta_dot(i) = atan(w_dot/(x_tip - x_root))

  end do


end subroutine compute_circulation

!-------------------------------------------------------------------------------
subroutine initialize_free_wake(Gamma, beta, beta_dot, Free_wake)

  use trim_parameters
  use freewake_interface
  use blade_parameters
  use aerodynamic_parameters
  use my_funcs
  use precision           ! quantities and conversion constants
  implicit none

  type(FreeWakeDef), intent(INOUT) :: Free_wake

  double precision, intent(IN)  :: beta(adim), beta_dot(adim), Gamma(NSEG,adim)

  double precision :: alpha_s, phi_s, theta_0, theta_1c, theta_1s, Gamma_temp(NSEG, adim)

  integer :: i, j


  call assign_constants()


  i = 1
  j = 1


  alpha_s = trim_vec(1)
  phi_s = trim_vec(2)
  theta_0 = trim_vec(3)
  theta_1c = trim_vec(4)
  theta_1s = trim_vec(5)




  Free_wake % add_inflow       = .false.
  Free_wake % adim             = 36
  Free_wake % alpha0           = 0.0d0  !symmetric airfoil
  Free_wake % altitude         = 0.0d0
  Free_wake % asr              = alpha_s / d2r !long. shaft tilt, forwards
  Free_wake % betap            = 0.0d0  ! precone
  Free_wake % chord            = chord2R * R_dim !chord in m
!  Free_wake % CSD_RLOC(:)      = !from flap code, radial locations; 0 to 1 (quadrature point locations)
  Free_wake % delsp            = 0.0d0 ! swashplate phase offset between NR frame cyclic and blade pitch (array)
  Free_wake % dpsi             = 10.0d0*d2r ! degrees
  ! print *, d2r
 Free_wake % DWASH            = 0.0d0 !Airframe downwash
  Free_wake % E1               = offset * R_dim! flap hinge offset, nondiml with R (0 to 1)
!  Free_wake % flph0            = 0.0d0 !hinge offset, redundant, not used
!  Free_wake % found_Hloc(:)    = !outdated
  Free_wake % Fturn            = 6.0d0    ! number of wake turns
  Free_wake % Generate_history = .false.
  Free_wake % iaz              = 0        ! not used : single time step mode
  Free_wake % imarch           = 0        ! not used : single time step mode
  Free_wake % int_mode         = .false.  ! not used : single time step mode
  Free_wake % itimepc          = 0        ! not used : single time step mode
  Free_wake % kdim             = 1        ! number of vortex cores
  Free_wake % mux              = mu       ! edgewise advance ratio parallel to rotor disk along free-stream
  Free_wake % muy              = 0.0d0    ! sideslip advance ratio
  Free_wake % muz              = 0.0d0    ! should be Vinf sin(al)/Vtip
  Free_wake % Nblades          = Nb
  Free_wake % nfw              = floor(Free_wake % Fturn) * Free_wake % Nblades
  Free_wake % NPsi             = 37  !ask tyler
  Free_wake % Nrotors          = 1
  Free_wake % NSEG             = 50 !number of radial locations
  Free_wake % omega            = omega_dim
  Free_wake % pb               = 0.0d0 !Fuselage roll rate

  call linspace(0.0d0, 360.0d0*d2r, Free_wake % NPsi, Free_wake % psi)

  Free_wake % qb               = 0.0d0 !Fuselage pitch rate
  Free_wake % qinitial         = 'n'
  Free_wake % RadiusM          = R_dim
  Free_wake % rcout            = rcout !root cut out, nondiml with radius => not used
  Free_wake % rcout0           = rcout! root cut out; non diml with radius
  Free_wake % rdim             = 1

  ! call linspace(0.0d0, R, Free_wake % NSEG, Free_wake % RLOC) !not necessary

  Free_wake % RotGeo           = 0

  Free_wake % rrx(1)           = 0.0d0    ! hub location coordinates wrt some origin
  Free_wake % rry(1)           = 0.0d0
  Free_wake % rrz(1)           = 0.0d0
  Free_wake % sdim             = Free_wake % NSEG+1

  Free_wake %  setup_interface_run = .false.    ! for prasadum; don't need here
  Free_wake % taperst              = 0.0d0 !outdated
  Free_wake % taper0                = 1.0d0
  Free_wake % th0(1)                  = theta_0 /  d2r   ! deg

  Free_wake % th1c(1)                 = theta_1c / d2r ! deg
  Free_wake % th1s(1)                 = theta_1s / d2r  ! deg
  Free_wake % Turbine_BL           = .false.    ! to use atm boundary layer profile for wind turbines
  Free_wake % Turbine_clearance    = 0.0d0      !clearance btw wind turbine tip
  Free_wake % wdim                 = 1          ! number of rolled up trailers
  Free_wake % Wind_turbine         = .false.    !


  Free_wake % vtipSI               = Free_wake % omega * Free_wake % RadiusM
  Free_wake % zdim                 = floor(Free_wake % Fturn * Free_wake % adim) + 1



  Free_wake % Fw_rtr(1) % beta (1:Free_wake % NPsi)  = beta
  Free_wake % Fw_rtr(1) % betd  (1:Free_wake % NPsi) = beta_dot

  call ones(Free_wake % NSEG, chord, Free_wake % Fw_rtr(1) % chord)

  Free_wake % Fw_rtr(1) % crdroot = chord2R * R_dim
  Free_wake % Fw_rtr(1) % crdtip  = chord2R * R_dim

  call ones(Free_wake % NSEG, theta_tw, Free_wake % Fw_rtr(1) % geomtw)


  !Hard coded for 4 blades as of now and adim = 36
  Free_wake % Fw_rtr(1) % FW_Bld(1) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma
  ! Free_wake % Fw_rtr(1) % FW_Bld(2) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma
  Free_wake % Fw_rtr(1) % FW_Bld(3) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma
  Free_wake % Fw_rtr(1) % FW_Bld(4) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma

  !
  Gamma_temp(:, 1:28) = Gamma(:, 10:Free_wake % NPsi)
  Gamma_temp(:, 29:37) = Gamma(:, 2:10)

  Free_wake % Fw_rtr(1) % FW_Bld(2) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma_temp
  !
  ! Gamma_temp(:, 1:19) = Gamma(:, 19:Free_wake % NPsi)
  ! Gamma_temp(:, 20:37) = Gamma(:, 2:19)
  !
  ! Free_wake % Fw_rtr(1) % FW_Bld(3) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma_temp
  !
  ! Gamma_temp(:, 1:10) = Gamma(:, 28:Free_wake % NPsi)
  ! Gamma_temp(:, 11:37) = Gamma(:, 2:28)
  ! Free_wake % Fw_rtr(1) % FW_Bld(4) % gmb2 (1:Free_wake % NSEG, 1:Free_wake % NPsi) = Gamma_temp

  !!! Ask tyler
  Free_wake % Fw_rtr(1) % FW_Bld(1) % phi2 = 0.0d0
  Free_wake % Fw_rtr(1) % FW_Bld(2) % phi2 = 0.0d0
  Free_wake % Fw_rtr(1) % FW_Bld(3) % phi2 = 0.0d0
  Free_wake % Fw_rtr(1) % FW_Bld(4) % phi2 = 0.0d0


  Free_wake % Fw_rtr(1) % tw_rcout  = 0.0d0
  Free_wake % Fw_rtr(1) % twstrate  = 0.0d0


end subroutine initialize_free_wake

!-------------------------------------------------------------------------------


!=======================================================================
!> This subroutine computes the Jacobian used in the trim procedure.
!                           ~         ~        ~
!                                  Jacobian
!                           ~         ~        ~
!=======================================================================
subroutine compute_Jacobian(residual, Jacobian)
  use trim_parameters
  use aerodynamic_parameters
  use blade_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================
  double precision, intent(IN) :: residual(n_trim)

  !=======================================================================
  !                             Outputs
  !=======================================================================
  double precision, intent(OUT) :: Jacobian(n_trim, n_trim)

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  double precision              :: delta, residual_new(n_trim)

  integer                       :: i



  delta = 0.00001d0

  do i = 1, n_trim, 1
    trim_vec(i) = trim_vec(i) + delta
    call trim_residual(residual_new)
    Jacobian(:,i) = (residual_new - residual) / (delta)
    trim_vec(i) = trim_vec(i) - delta
  end do

end subroutine compute_Jacobian

!-------------------------------------------------------------------------------

subroutine rigid_blade_trim(alpha_s, phi_s, theta_0, theta_1c, theta_1s, lambda)
  use trim_parameters
  implicit none


  double precision, intent(OUT) :: alpha_s, phi_s, theta_0, theta_1c, theta_1s, lambda

  if (abs(mu - 0.3) < 1e-7) then
    alpha_s = 0.575d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -0.570d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.32d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.16d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -4.17d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0134d0
  else if (abs(mu - 0.0) < 1e-7) then
    alpha_s = 0.0d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -0.0d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 9.08d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.0d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -0.0d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0627d0
  else if (abs(mu - 0.4) < 1e-7) then
    alpha_s = 0.835d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = 0.0043d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 7.03d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.39d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -5.70d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0141d0
  else if (abs(mu - 0.1) < 1e-7) then
    alpha_s = 0.171d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -2.08d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.82d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.453d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -1.50d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0289d0
  else if (abs(mu - 0.2) < 1e-7) then
    alpha_s = 0.357d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = -1.15d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 6.05d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 0.834d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -2.77d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0163d0
  else
    alpha_s = 0.835d0 * 4 * atan (1.0_8)/180.0d0
    phi_s = 0.0043d0 * 4 * atan (1.0_8)/180.0d0
    theta_0 = 7.03d0 * 4 * atan (1.0_8)/180.0d0
    theta_1c = 1.39d0 * 4 * atan (1.0_8) /180.0d0
    theta_1s = -5.70d0*4 * atan (1.0_8)/180.0d0
    lambda = 0.0141d0
  end if

end subroutine rigid_blade_trim
