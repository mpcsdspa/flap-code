import numpy as np
import matplotlib.pyplot as plt

gam = np.loadtxt('FWG_b_mr.dat')

for i in range(37):
   plt.plot(gam[50*(i-1):50*i-1, i], gam[50*(i-1):50*i-1, i], linewidth = 2.0)
plt.xlim([0, 1])
plt.grid()
plt.xlabel('Non-dimensional radial location, x', fontsize = 16)
plt.ylabel('Bound Circulation', fontsize = 16)
plt.title('Bound Circulation distribution on blade', fontsize = 20)
plt.show()
