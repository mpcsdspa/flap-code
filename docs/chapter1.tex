
\section{Blade Response to Aerodynamic Forcing}
We have seen above the flap response to a sinusoidal tip load. Now, let's look at the blade flap response to the Aerodynamic loading. The derived expressions of the Aerodynamic forces and their derivations are presented in the Appendix section. The results of the trim analysis is calculated using two different methods. There is no such name for these methods, so we call them as \textbf{Method I} and \textbf{Method II}. Method I is more of a conventional method, while Method II is a novel way to solve the FET. We will see Method II in detail in the next sections. 


\subsection{Method I}
This method mainly focuses on bringing the terms to LHS and calculate the aerodynamic damping and stiffness matrices. The Aerodynamic forces are dependent on the displacement and their spatial and time derivatives. We categorize the terms of the whole expression in three categories, namely Constant, Linear and the Non-Linear terms. Based on this, we categorize two different models, namely Linear and Non-Linear Aerodynamic model.



\subsubsection{Linear Aerodynamic Model}
The linear aerodynamic model contains only first order terms along with the constant. The first order terms are those whose exponent is 1. The terms with $w, w', \dot{w}, \dot{w}'$ are considered as first order terms and are a part of Linear Aerodynamic Model. Any product of these terms with each other results in a higher order model. For example, $ww', w'^2, w\dot{w}$ is considered as second order. Hence, we neglect all such higher order terms in this model. This is not a accurate model due to the truncation of higher order terms. However, implementation will be quite easier and can be modified to Non-Linear Aerodynamic Model.

The flapping aerodynamic load can be expressed as
\begin{equation}
L_w^{A} =  L_0 + L_1 w + L_2 w' + L_3 \dot{w} + L_4 \dot{w}' + L_{Non-Linear}
\end{equation}

where $L_{Non-Linear}$ are non-linear terms. As stated above, the non-linear terms are neglected, hence the flapping aerodynamic load is given by,
\begin{equation}
L_w^{A} =  L_0 + L_1 w + L_2 w' + L_3 \dot{w} + L_4 \dot{w}'
\end{equation}

$L_0$ is the constant term which is independent of the displacement and its derivatives. The expressions for $L_0, L_1, L_2, L_3, L_4$ and their derivations are mentioned at the end of the report. Once we have the expressions for each of the terms, the aerodynamic damping, stiffness and forcing matrices for the $i^{th}$ are calculated using the following:

\begin{equation}
C_{e}^{A} = \int_{0}^{l} (-L_3 H^{T} H  - L_4 H^T  H') ds
\end{equation}
		
\begin{equation}
K_{e}^{A} = \int_{0}^{l} (-L_1 H^{T} H  - L_2 H^T  H') ds
\end{equation}

\begin{equation}
Q_{e}^{A} = \int_{0}^{l} (L_0 H^{T}) ds
\end{equation}

Basically, we will be solving the following equation.
\begin{equation}
M \ddot{q} + (K_{e}^{A} + K_{e}^{S}) q + (C_{e}^{A}) \dot{q} = Q_{e}^{A}
\end{equation}	

While implementing the Finite Element in Time, we need to evaluate the elemental matrices for a given time Gaussian point and assemble them to get the FET $\textbf{A}$ matrix.

\subsubsection{Non-Linear Aerodynamic Model}
The Non-Linear Aerodynamic Model consists of higher order products of $w$ with the temporal and spatial derivatives. For example, $w'^2, \dot{w}^2, w\dot{w}, \dot{w}\dot{w}'$ appear in the expression of non-linear aerodynamic model. Here, we restrict ourselves to the second-order and any order higher than second order are neglected. Rewriting the expression of flapping aerodynamic load,
\begin{equation}
L_w^{A} =  L_0 + L_1 w + L_2 w' + L_3 \dot{w} + L_4 \dot{w}' + L_{Non-Linear}
\end{equation}

where,
\begin{equation}
\begin{gathered}
L_{Non-Linear} = A_1 w^2 + A_2 w w'+ A_3 w \dot{w} + A_4 w \dot{w}' 
A_5 w'^2 + A_6 \dot{w} w' + \\
+ A_7 w' \dot{w}' + A_8 \dot{w}^2 + A_9 \dot{w} \dot{w}' + A_{10} \dot{w}'^2
\end{gathered}
\end{equation}	

On Linearizing the above terms, we get

where, $w_o$ is the flap deformation from the previous iteration. For the first iteration, we take the initial guess of flap deformation to be 0, which will result in a solution identical to the Linear Aerodynamic Model.
Once we have the expressions for the linearized terms, the aerodynamic damping, stiffness and forcing matrices for the $i^{th}$ are calculated using the following:

		\begin{equation}
		C_{e}^{A} = \int_{0}^{l} [-(L_3 + \left.\frac{\partial L_{nl}}{\partial \dot{w}}\right|_{w_0} ) H^{T} H  - (L_4 + \left.\frac{\partial L_{nl}}{\partial \dot{w}'}\right|_{w_0} )H^T  H' ]ds
		\end{equation}
		
		
		\begin{equation}
		K_{e}^{A} = \int_{0}^{l} [-(L_1 + \left.\frac{\partial L_{nl}}{\partial {w}}\right|_{w_0} ) H^{T} H  - (L_2 + \left.\frac{\partial L_{nl}}{\partial {w}'}\right|_{w_0} )H^T  H' ]ds
		\end{equation}
		
\begin{equation}
Q_{e}^{A} = \int_{0}^{l} H^{T} [L_0 + \left.L_{nl}\right|_{w_0} - \left.\frac{\partial L_{nl}}{\partial {w}}\right|_{w_0} w_0    - \left.\frac{\partial L_{nl}}{\partial {w}'}\right|_{w_0} w_0'-
\left.\frac{\partial L_{nl}}{\partial {\dot{w}}}\right|_{w_0} \dot{w_0}-
\left.\frac{\partial L_{nl}}{\partial {\dot{w}'}}\right|_{w_0} \dot{w_0}'] ds
\end{equation}
		
As stated above, the elemental matrices are evaluated at the time Gaussian points and assembled as done before.

\subsection{Method II}

One of the major problems we face while solving using Method I is that, we need to decompose the terms of Aerodynamic force. We need to explicitly know the expression for the terms say $L_1, L_2, B_1, B_5$ and so on. Writing these expressions without some assumptions might be a nightmare. However, this Method II presented below solves this problem by not caring about what are the expressions for each coefficients. As mentioned before, the Method I mainly focuses on bringing the terms to LHS and calculate the aerodynamic damping and stiffness matrices. However, in this we keep the complete forcing term on the RHS and solve the system. We know that on implementation of FET, we will be solving a  linear system of equations say
$A\eta = Q'$.  So, our final aim is to compute the value of $\eta$. The following are the steps involved in this approach:

	\begin{enumerate}
		\item In this method, we solve the equation by keeping all the forcing terms on the RHS.
		\begin{equation}
		M \ddot{q} + K_{e}^{S} q  = Q(q, \dot{q},.....)
		\end{equation}
		
		Note: The LHS comprises of time independent Mass matrix ($M$) and structural stiffness matrix ($K_e^{A}$).
		
		\item Compute the A matrix of FET from the LHS(only once)
		
		\item Assume some value of $\eta$, say zeros, and compute the residual 
		$\epsilon = A \eta - Q' $
		\item Compute the Jacobi $\frac{\partial \epsilon}{\partial \eta}$ (Computationally expensive.)
		\item Update the $\eta$ as,
		
		\begin{equation}
		\eta_{new} = \eta_{old} - J^{-1}\epsilon
		\end{equation}
		\item Iterate till you get a converged solution.
		\item For linear Aerodynamic forcing, the solution converges in one iteration ($\epsilon = 10^{-12}$)
		
	\end{enumerate}	
	
	This method solves the "Complete Non-Linear Flapping Equation", without any approximations or truncations. One of the problems this method poses is the computation of Jacobian Matrix, which can be computationally expensive for a large number of space and time elements. However, modal reduction can be used to reduce the time consumption. We can also use the Modified Newton Raphson method to solve, which is same as above except that we compute the Jacobian at specific intervals. We will discuss about the results obtained using this method in the later sections.
	
	
	
	