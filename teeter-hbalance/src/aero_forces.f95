!=======================================================================
!> This subroutine computes the linear aerodynamic force for a given vector
!> of trim parameters at a given azimuth and space location.
!                   ~     ~     ~     ~     ~     ~
!                               aero_force
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine compute_aero_force(x_val, t_val, w, w_dash, w_dot, w_dot_dash, aero_force)
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: x_val, t_val(np), w(np, 1), w_dash(np,1), &
                                                    w_dot(np, 1), w_dot_dash(np, 1)

  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: aero_force(np, 1)

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  double precision                               :: l, x, &
                                                    theta_0, theta_1c, theta_1s, lambda
                                                    
  double precision                               :: u_R(np, 1), u_T(np, 1), u_P(np, 1), psi(np, 1), &
                                                    a1(np, 1), a2(np, 1), a3(np, 1), L_w(np, 1), &
                                                    L_ubar(np, 1), L_vbar(np, 1), L_wbar(np, 1), &
                                                    theta(np, 1), theta_dot(np, 1)



  !=======================================================================
  !                     Extracting the trim parameters
  !=======================================================================
  theta_0 = trim_vec(3)
  theta_1c = trim_vec(4)
  theta_1s = trim_vec(5)
  lambda = trim_vec(6)



  l = R*(1.0d0 - offset)/dble(ne)


  psi(:, 1) = omega * t_val(:)
  x = x_val / R

  theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)

  theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))


  !=======================================================================
  !                           Dummy Variables
  !=======================================================================
  a1 = x + mu * sin(psi)
  a2 = lambda + mu * beta_p * cos(psi)
  a3 = theta_dot + beta_p

  !=======================================================================
  !                             Lift expression
  !=======================================================================

  u_R = -mu * cos(psi) * (1 - beta_p * w_dash) + lambda * (beta_p + w_dash) &
        -neta_r * cos(theta_0) - w_dot_dash*neta_r*sin(theta_0) + w_dot*w_dash &
        + 0.5d0 * mu * cos(psi) * w_dash * w_dash

  u_T = (sin(theta)*a2 + cos(theta)*a1) - beta_p*cos(theta)*w &
        + mu*cos(psi)*sin(theta)*w_dash + w_dot*sin(theta)

  u_P = (neta_r*a3 - sin(theta)*a1 + cos(theta)*a2) + beta_p*sin(theta)*w &
        + (neta_r + mu*cos(psi)*cos(theta))*w_dash + cos(theta)*w_dot

  L_ubar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_R * u_T)

  L_vbar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_T * u_T - (Cl_o*u_P - Cd1*abs(u_P))*u_T + (Cl_alpha - Cd2)*u_P*u_P)

  L_wbar = (Lock_no/(6.0d0*Cl_alpha)) * (Cl_o*u_T*u_T - (Cl_alpha + Cdo)*u_T*u_P + Cd1*abs(u_P)*u_P)

  L_w = sin(theta) * L_vbar + cos(theta) * L_wbar + w_dash *L_ubar &
        - (0.5d0*w_dash*w_dash)*sin(theta)*L_vbar - (0.5d0*w_dash*w_dash)*cos(theta)*L_wbar

  aero_force = L_w * m0 * omega * omega * R;

end subroutine compute_aero_force

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the odd and even harmonic contribution
!> of the aerodynamic force at a given spatial and time location
!> using Fourier Transform.
!                   ~     ~     ~     ~     ~     ~
!                         value_hin, value_art
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine fft_aero(x_val, t_val, w, w_dash, w_dot, w_dot_dash, value_hin, value_art)
  use finite_elements
  use blade_parameters
  implicit none

  !=======================================================================
  !                            Inputs
  !=======================================================================
  real(kind = 8), intent(IN)                     :: x_val, t_val, w(np, 1), &
                                                    w_dash(np, 1), w_dot(np, 1), &
                                                    w_dot_dash(np, 1)

  !=======================================================================
  !                            Outputs
  !=======================================================================
  real(kind = 8), intent(OUT)                    :: value_hin, value_art

  !=======================================================================
  !                             Local Variables
  !=======================================================================
  real(kind = 8)                                 :: aero_force(np), psi_vec(np), weight(np), &
                                                    temp(2*n_harm+1), sf1(2*n_harm+1), &
                                                    sf2(2*n_harm+1), temp_val

  integer                                        :: i, j

  !=======================================================================
  !                          Main Code
  !=======================================================================


  !Hard Coded Gaussian nodes (Very Bad!)
  psi_vec(:) = [6.225256331192224, 5.981960784187037,5.560313238817757,4.986706424098016,4.297169388601398,&
         3.535025009753433,2.748160297426153,1.986015918578189,1.296478883081570,0.722872068361829,&
         0.301224522992550,0.057928975987362]

  weight(:) = [0.148205690222493,0.335959800926735,0.502900900950709,0.638269295241926,0.733538437456942, &
           0.782718528790986,0.782718528790986,0.733538437456942,0.638269295241926,0.502900900950709, &
           0.335959800926735,0.148205690222493]

  call compute_aero_force(x_val, psi_vec, w, w_dash, w_dot, w_dot_dash, aero_force)


  temp = 0.0d0

  do i = 1, np, 1
    do j = 1, (2*n_harm+1), 1
      call shape_funcs(psi_vec(i), j, temp_val)
      temp(j) = temp(j) + weight(i) * aero_force(i) * temp_val
    end do
  end do  

  temp(1) = temp(1) / (2*pi)
  temp(2:) = temp(2:) /(pi)


  call shape_function(1, t_val, sf1) 
  value_hin = dot_product(temp(hin_dof), sf1(hin_dof))


  call shape_function(2, t_val, sf2) 
  value_art = dot_product(temp(art_dof), sf2(art_dof))


end subroutine fft_aero

!-------------------------------------------------------------------------------
