!=======================================================================
!> This subroutine computes the harmonic coefficients for the modal
!> coordinates using Jacobi iteration.
!                   ~     ~     ~     ~     ~     ~
!                         coeff, coeff_vec
!                   ~     ~     ~     ~     ~     ~
!=======================================================================
subroutine compute_harmonic_coeff()
	use trim_parameters
	use my_funcs
	use blade_parameters
	implicit none

	!=======================================================================
  	!                          	  Local Variables
  	!=======================================================================  

	real(kind = 8) 								 :: factor, residual((2*n_harm+1)*num_modes), &
												    Jacobian((2*n_harm+1)*num_modes, (2*n_harm+1)*num_modes), &
												    residual_new((2*n_harm+1)*num_modes), &
												    delta_q((2*n_harm+1)*num_modes), &
												    Jacobian_temp((2*n_harm+1)*num_modes, (2*n_harm+1)*num_modes)

	integer 									 :: i, k, stop_fac, info, &
													ipiv((2*n_harm+1)*num_modes)


	!=======================================================================
  	!                          	 	Main Code
  	!=======================================================================  
	Jacobian = 0.0d0
	factor = 0.01d0

	if (.not. allocated(coeff)) then
		allocate(coeff(num_modes, 2*n_harm+1))
		allocate(coeff_vec(num_modes*(2*n_harm+1)))
	end if

	coeff = 0.0d0
	coeff_vec = 0.0d0


	stop_fac = 0

	do i = 1, 31, 1
		call compute_residual(residual)

		if ( i == 1 .or. i == 11 .or. i == 21 ) then
			do k = 1, (2*n_harm+1)*num_modes
				coeff_vec(k) = coeff_vec(k) + factor
				call reshape((2*n_harm+1)*num_modes, coeff_vec, [num_modes, 2*n_harm+1], coeff)
				call compute_residual(residual_new)
				Jacobian(:, k) = (residual_new - residual) / (factor)
				coeff_vec(k) = coeff_vec(k) - factor 
				call reshape((2*n_harm+1)*num_modes, coeff_vec, [num_modes, 2*n_harm+1], coeff)
			end do
			Jacobian_temp = Jacobian
		end if

	    if ( i > 1 ) then
		      Jacobian = Jacobian_temp
		end if

		delta_q = residual
    	call DGESV(num_modes*(2*n_harm+1), 1, Jacobian, num_modes*(2*n_harm+1), ipiv, delta_q, num_modes*(2*n_harm+1), info)
		coeff_vec = coeff_vec - delta_q
		call reshape((2*n_harm+1)*num_modes, coeff_vec, [num_modes, 2*n_harm+1], coeff)

		if (NORM2(residual) < 1e-7) then
			stop_fac = 1
		end if

		if (stop_fac == 1) then
			exit
			stop
		end if

	end do

end subroutine compute_harmonic_coeff

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes residual by integrating the error at a azimuth
!> with the shape functions used (Galerkin Approach)
!                   ~     ~     ~     ~     ~     ~
!                         		residual
!                   ~     ~     ~     ~     ~     ~
!=======================================================================
subroutine compute_residual(residual)
	use finite_elements
	use trim_parameters
	use blade_parameters
	implicit none

	!=======================================================================
  	!                          	  	  Outputs
  	!=======================================================================
  	real(kind = 8), intent(OUT) :: residual(num_modes*(2*n_harm+1))

	!=======================================================================
  	!                          	  Local Variables
  	!=======================================================================  	
	real(kind = 8) 								 		:: psi, psi_vec(np), weight(np), &
														   sf1(2*n_harm+1), sf2(2*n_harm+1), sf, &
														   x_min, x_max, x_vec(np_spatial*ne), l, &
														   error_hin(num_modes, 1), error_art(num_modes, 1), &
														   res_hin(num_modes*(2*n_harm+1)), &
														   res_art(num_modes*(2*n_harm+1)), &
														   coeff_temp1(4), coeff_temp2(4)

	real(kind = 8), dimension(num_modes, 2*n_harm+1) 	:: sum_hin, sum_art

	real(kind = 8), dimension(num_modes, np) 			:: neta_hin, neta_dot_hin, &
														   neta_double_dot_hin, neta_art, &
														   neta_dot_art, neta_double_dot_art, &
														   Q_hin2, Q_art2


	real(kind = 8), dimension(2*ne, np) 				:: q_hin, q_dot_hin, Q_hin1

	real(kind = 8), dimension(2*ne+1, np) 				:: q_art, q_dot_art, q_tee, q_dot_tee, Q_art1

	real(kind = 8), dimension(np, np_spatial*ne) 		:: w_hin, w_dash_hin, w_dot_hin, &	
														   w_dot_dash_hin, w_art, w_dash_art, &
														   w_dot_art, w_dot_dash_art, w_tee, &
														   w_dash_tee, w_dot_tee, w_dot_dash_tee


	integer 											:: i, j, k

	!=======================================================================
  	!                          	  Main Code
  	!=======================================================================  

	if(.not. allocated(M_hin)) then
		allocate(M_hin(num_modes, num_modes))
		allocate(K_hin(num_modes, num_modes))
		allocate(V_hin(2*ne, num_modes))
		call main_fem(1, 1)
	end if


	if(.not. allocated(M_art)) then
		allocate(M_art(num_modes, num_modes))
		allocate(K_art(num_modes, num_modes))
		allocate(V_art(2*ne+1, num_modes))
		call main_fem(2, 1)
	end if

	do i = 1, ne, 1
		x_min = (R/ne) * (i-1)
		x_max = (R/ne) * i
		x_vec(np_spatial*(i-1)+1:np_spatial*i) = 0.5d0 * ((x_min + x_max) + l_nodes * (x_max - x_min))
	end do

	psi_vec(:) = [6.225256331192224, 5.981960784187037,5.560313238817757,4.986706424098016,4.297169388601398,&
		 	 	 3.535025009753433,2.748160297426153,1.986015918578189,1.296478883081570,0.722872068361829,&
		 	 	 0.301224522992550,0.057928975987362]

	weight(:) = [0.148205690222493,0.335959800926735,0.502900900950709,0.638269295241926,0.733538437456942, &
   				 0.782718528790986,0.782718528790986,0.733538437456942,0.638269295241926,0.502900900950709, &
   				 0.335959800926735,0.148205690222493]


	do i = 1, np, 1
		psi = psi_vec(i)

		call shape_function(1, psi, sf1)
		neta_hin(:,i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))
		call shape_function_der(1, psi, sf1)
		neta_dot_hin(:, i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))
		call shape_function_double_der(1, psi, sf1)
		neta_double_dot_hin(:, i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))

		call shape_function(2, psi, sf2)
		neta_art(:,i) = MATMUL(coeff(:, art_dof), sf2(art_dof))
		call shape_function_der(2, psi, sf2)
		neta_dot_art(:, i) = MATMUL(coeff(:, art_dof), sf2(art_dof))
		call shape_function_double_der(2, psi, sf2)
		neta_double_dot_art(:, i) = MATMUL(coeff(:, art_dof), sf2(art_dof))

		q_hin(:, i) = MATMUL(V_hin, neta_hin(:,i))
		q_dot_hin(:,i) = MATMUL(V_hin, neta_dot_hin(:,i))

		q_art(:, i) = MATMUL(V_art, neta_art(:,i))
		q_dot_art(:,i) = MATMUL(V_art, neta_dot_art(:,i))

		call compute_w(q_hin(:,i), q_dot_hin(:,i), q_art(:,i), q_dot_art(:,i), x_vec, np_spatial*ne, &
			           w_hin(i,:), w_dash_hin(i,:), w_dot_hin(i,:), w_dot_dash_hin(i,:), &
			           w_art(i,:), w_dash_art(i,:), w_dot_art(i,:), w_dot_dash_art(i,:))

		w_tee(i,:) = w_hin(i,:) + w_art(i,:)
		w_dash_tee(i,:) = w_dash_hin(i,:) + w_dash_art(i,:)
		w_dot_tee(i,:) = w_dot_hin(i,:) + w_dot_art(i,:)
		w_dot_dash_tee(i,:) = w_dot_dash_hin(i,:) + w_dot_dash_art(i,:)

	end do

	do i = 1, np, 1
		psi = psi_vec(i)
		call compute_Q(psi, w_tee, w_dash_tee, w_dot_tee, w_dot_dash_tee, Q_hin1(:,i), Q_art1(:,i))
		Q_hin2(:, i) = MATMUL(transpose(V_hin), Q_hin1(:, i))
		Q_art2(:, i) = MATMUL(transpose(V_art), Q_art1(:, i))	
	end do

	sum_hin = 0.0d0
	sum_art = 0.0d0

	do i = 1, np, 1
		error_hin(:,1) = MATMUL(M_hin, neta_double_dot_hin(:,i))+ MATMUL(K_hin, neta_hin(:,i)) - Q_hin2(:,i)
		error_art(:,1) = MATMUL(M_art, neta_double_dot_art(:,i))+ MATMUL(K_art, neta_art(:,i)) - Q_art2(:,i)
		do j = 1, (2*n_harm+1), 1
			call shape_funcs(psi_vec(i), j, sf)
			sum_hin(:,j) = sum_hin(:,j) + weight(i) * error_hin(:,1) * sf
			sum_art(:,j) = sum_art(:,j) + weight(i) * error_art(:,1) * sf
		end do
	end do

	k = 1
	do i = 1, 2*n_harm+1, 1
		do j = 1, num_modes, 1
			res_hin(k) = sum_hin(j, i)
			res_art(k) = sum_art(j, i)
			k = k + 1
		end do
	end do

	residual = res_hin + res_art

end subroutine compute_residual

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the Q vector for hingeless and articulated
!> using fourier transform, segregating the odd and even harmonics.
!                   ~     ~     ~     ~     ~     ~
!                         	   Q_hin, Q_art
!                   ~     ~     ~     ~     ~     ~
!=======================================================================
subroutine compute_Q(psi, w, w_dash, w_dot, w_dot_dash, Q_hin, Q_art)
	use finite_elements
	use blade_parameters
	implicit none

	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================
	real(kind = 8), intent(IN) 					 :: w(np, np_spatial*ne), w_dash(np, np_spatial*ne), & 
								  					w_dot(np, np_spatial*ne), &
								  					w_dot_dash(np, np_spatial*ne), psi

	!=======================================================================
  	!                          	  	  Outputs
  	!=======================================================================
	real(kind = 8), intent(OUT) 				 :: Q_hin(2*ne, 1), Q_art(2*ne+1, 1)


	!=======================================================================
  	!                          	  	  Local Variables
  	!=======================================================================
	real(kind = 8) 								 :: q_hin_temp(dof_per_element, 1), &
													q_art_temp(dof_per_element, 1)

	real(kind = 8)								 :: Q1_hin(2*ne+2, 1), Q1_art(2*ne+2, 1), l

	integer  									 :: i



	!=======================================================================
  	!                          	  	 Main Code
  	!=======================================================================
	l = R/ne

	Q1_hin = 0.0d0
	Q1_art = 0.0d0

	do i = 1, ne, 1
		call spatial_integral(psi, r_left(i), r_right(i), l, w(:,np_spatial*(i-1)+1:np_spatial*i), &
			 w_dash(:,np_spatial*(i-1)+1:np_spatial*i), w_dot(:,np_spatial*(i-1)+1:np_spatial*i), &
			 w_dot_dash(:,np_spatial*(i-1)+1:np_spatial*i), q_hin_temp, q_art_temp)

		Q1_hin(element_dofs(i, :), 1) = Q1_hin(element_dofs(i,:), 1) + q_hin_temp(:,1)
		Q1_art(element_dofs(i, :), 1) = Q1_art(element_dofs(i,:), 1) + q_art_temp(:,1)
	end do

	Q_hin(:,1) = Q1_hin(b_hin, 1)
	Q_art(:,1) = Q1_art(b_art, 1)


end subroutine compute_Q

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the force vector by spatial integration
!                   ~     ~     ~     ~     ~     ~
!                         	  F_hin, F_art
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine spatial_integral(t_val, x_min, x_max, l, w, w_dash, w_dot, w_dot_dash, F_hin, F_art)
	use hermite
	use blade_parameters
	implicit none

	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================
	real(kind = 8), intent(IN) 					 :: t_val, x_min, x_max, l
	real(kind = 8), intent(IN) 					 :: w(np, np_spatial), w_dash(np, np_spatial), &
								  					w_dot(np, np_spatial), w_dot_dash(np, np_spatial)

	!=======================================================================
  	!                          	  	  Outputs
  	!=======================================================================
	real(kind = 8), intent(OUT) 				 :: F_hin(dof_per_element, 1), F_art(dof_per_element, 1)

	!=======================================================================
  	!                          	  	Local Variables
  	!=======================================================================
	real(kind = 8) 								 :: x_vec(np_spatial), h_temp(1, dof_per_element),&
					  								h_func(dof_per_element), val_hin, val_art

	integer 									 :: i

	!=======================================================================
  	!                          	  	  Main Code
  	!=======================================================================
	x_vec = 0.5d0 * ((x_min + x_max) + l_nodes * (x_max - x_min))

	F_hin = 0.0d0
	F_art = 0.0d0

	do i = 1, np_spatial, 1
	    call Hermite_Funcs(x_vec(i)-x_min, l, h_func)
	    h_temp(1, :) = h_func
		call fft_aero(x_vec(i), t_val, w(:,i), w_dash(:,i), w_dot(:,i), w_dot_dash(:,i), val_hin, val_art)

		F_hin = F_hin + l_weights(i) * val_hin * transpose(h_temp)
		F_art = F_art + l_weights(i) * VAL_ART * transpose(h_temp)
	end do


	F_hin = F_hin * 0.5d0 * (x_max - x_min)
	F_art = F_art * 0.5d0 * (x_max - x_min)

end subroutine spatial_integral

!-------------------------------------------------------------------------------

!=======================================================================
!> The below subroutines compute the shape functions and its derivatives, 
!> basically sine and cosine at a given psi. (full vector or single)
!                   ~     ~     ~     ~     ~     ~
!                         	  	   sf
!                   ~     ~     ~     ~     ~     ~
!=======================================================================

subroutine shape_function(rotor_type, t_val, sf)
	use blade_parameters
	use finite_elements
	implicit none


	!=======================================================================
  	!                          		   Inputs
  	!=======================================================================
  	integer, intent(IN) 					     :: rotor_type

  	real(kind = 8), intent(IN)					 :: t_val

	!=======================================================================
  	!                         		  Outputs
  	!=======================================================================
  	real(kind = 8), intent(OUT)					 :: sf(2*n_harm+1)

	!=======================================================================
  	!                          	  Local variables
  	!=======================================================================
  	integer                                      :: i


	!=======================================================================
  	!                          	      Main Code
  	!=======================================================================

	sf(1) = 1
	do i = 1, n_harm ,1 
		sf(2*i) = cos(i*t_val)
		sf(2*i + 1) = sin(i*t_val)
	end do

end subroutine shape_function

!-------------------------------------------------------------------------------

subroutine shape_funcs(psi, flag, sf)
	implicit none

	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================
	real(kind = 8), intent(IN) 					 :: psi
	integer, intent(IN) 						 :: flag

	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================
	real(kind = 8), intent(OUT) 				 :: sf

	!=======================================================================
  	!                          	     Main Code
  	!=======================================================================
	if (flag == 1) then
		sf = 1
	end if

	if (mod(flag, 2) == 0) then
		sf = cos(INT(flag/2)*psi)
	end if

	if (mod(flag, 2) == 1 .and. flag .NE. 1) then
		sf = sin(INT(flag/2)*psi)
	end if

end subroutine shape_funcs

!-------------------------------------------------------------------------------


subroutine shape_function_der(rotor_type, psi, sf)

	use blade_parameters
	implicit none

	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================
	integer, intent(IN) 					     :: rotor_type

	real(kind = 8), intent(IN) 				     :: psi

	!=======================================================================
  	!                          	  	  Outputs
  	!=======================================================================
	real(kind=8), intent(OUT) 					 :: sf(2*n_harm+1)

	!=======================================================================
  	!                          	  Local Variables
  	!=======================================================================
	integer 									 :: i

	!=======================================================================
  	!                          	  	  Main Code
  	!=======================================================================
	sf(1) = 0

	do i = 1, n_harm, 1
		sf(2*i) = -i * sin(i * psi)
		sf(2*i + 1) = i * cos(i * psi)
	end do


end subroutine shape_function_der


!-------------------------------------------------------------------------------


subroutine shape_function_double_der(rotor_type, psi, sf)

	use blade_parameters
	implicit none


	!=======================================================================
  	!                          	  	  Inputs
  	!=======================================================================

	integer, intent(IN) 						 :: rotor_type

	real(kind = 8), intent(IN) 				 	 :: psi

	!=======================================================================
  	!                          	  	  Outputs
  	!=======================================================================
	real(kind=8), intent(OUT) 					 :: sf(2*n_harm+1)

	!=======================================================================
  	!                          	  Local variables
  	!=======================================================================
	integer 									 :: i

	!=======================================================================
  	!                          	  	Main Code
  	!=======================================================================

	sf(1) = 0

	do i = 1, n_harm, 1
		sf(2*i) = -i * i * cos(i * psi)
		sf(2*i + 1) = -i * i * sin(i * psi)
	end do

end subroutine shape_function_double_der

!-------------------------------------------------------------------------------
