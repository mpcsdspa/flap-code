program main_procedure
  use finite_elements
  use blade_parameters
  use trim_parameters
  implicit none

  !=======================================================================
  !                         Time Stamp Parameters
  !=======================================================================
  integer                                        :: a0, b0, c0, a1, b1, c1



  print *, '**************************************************************'
  print *, 'Coupled Trim for Teetering Rotor (2 bladed Harmonic Balance)'
  print *, '**************************************************************'

  if (.not. allocated(hin_dof)) then

    if(mod(n_harm, 2) == 0) then
      allocate(hin_dof(n_harm+1))
      allocate(art_dof(n_harm))
    else
      allocate(hin_dof(n_harm))
      allocate(art_dof(n_harm+1))
    end if

    hin_dof = [1, 4, 5, 8, 9]
    art_dof = [2, 3, 6, 7, 10, 11]

  end if


  call system_clock(a0, b0, c0)
  mu = 0.3d0
  call trim()
  call system_clock(a1, b1, c1)
  print  *, '------------------------------------------------------------'
  print  *, '------------------------------------------------------------'
  print *, 'The time elapsed is', (a1-a0)/1000.0, 'seconds'
  print  *, '------------------------------------------------------------'
  print  *, '------------------------------------------------------------'


  !=======================================================================
  !                           write data to files
  !=======================================================================
  call write2files()

end program main_procedure
