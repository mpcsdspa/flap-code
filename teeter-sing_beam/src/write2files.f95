subroutine write2files()
  use trim_residuals
  use trim_parameters
  use finite_elements
  use loads
  use aerodynamic_parameters
  use blade_parameters
  use my_funcs
  implicit none


  !=======================================================================
  !                           Local Variables
  !=======================================================================

  integer                                        :: i

  double precision                               :: fx_fs(100), fy_fs(100), fz_fs(100), &
                                                    mx_fs(100), my_fs(100), mz_fs(100), &
                                                    fz_mm(100), my_mm(100)

  character(len=:), allocatable                  :: name



  !=======================================================================
  !                           Set Directory Path
  !=======================================================================
   allocate(character(len=34) :: name)
   name = '../Postprocess/teetering/txt_files'

   print *, 'Writing Data to Files for Postprocessing'

   !=======================================================================
   !                           HUB LOADS
   !=======================================================================
   call get_hub_loads()

   F_XH = F_XH * m0_dim * omega_dim**2 * R_dim**2
   F_YH = F_YH * m0_dim * omega_dim**2 * R_dim**2
   F_ZH = F_ZH * m0_dim * omega_dim**2 * R_dim**2

   M_XH = M_XH * m0_dim * omega_dim**2 * R_dim**3
   M_YH = M_YH * m0_dim * omega_dim**2 * R_dim**3
   M_ZH = M_ZH * m0_dim * omega_dim**2 * R_dim**3

   open(unit = 1, file=name // '/hub_forces')
   do i = 1, n_azim, 1
     write(1,*) time(i), ',', F_XH(i), ',', F_YH(i), ',', F_ZH(i)
   end do
   close(1)

   open(unit = 1, file=name // '/hub_moments')
   do i = 1, n_azim, 1
     write(1,*) time(i), ',', M_XH(i), ',', M_YH(i), ',', M_ZH(i)
   end do
   close(1)

   !=======================================================================
   !                         Tip Displacement
   !=======================================================================
   q_input = 10.0d0
   q_output = 0.0d0
   if ( aerodynamic_model == 2 ) then
     do while (abs(q_input(1,1) - q_output(1,1)) > 1e-6)
       q_input = q_output
       call aero_code()
     end do
   else if (aerodynamic_model == 1) then
     q_input = q_output
     call aero_code()
   else
     print *, 'No Such Aerodynamic Model exists!!!'
   end if


   open(unit = 1, file=name // '/tip_disp')
   do i = 1, (nodes-1)*nt, 1
     write(1,*) i, ',', q_output(max_modes*i-1-ne,1)*R_dim
   end do
   close(1)


   !=======================================================================
   !                          Root Forces
   !=======================================================================
   call get_root_forces()

   fxa = fxa * m0_dim * omega_dim**2 * R_dim**2
   fya = fya * m0_dim * omega_dim**2 * R_dim**2
   fza = fza * m0_dim * omega_dim**2 * R_dim**2
   fxi = fxi * m0_dim * omega_dim**2 * R_dim**2
   fyi = fyi * m0_dim * omega_dim**2 * R_dim**2
   fzi = fzi * m0_dim * omega_dim**2 * R_dim**2
   fx = fx * m0_dim * omega_dim**2 * R_dim**2
   fy = fy * m0_dim * omega_dim**2 * R_dim**2
   fz = fz * m0_dim * omega_dim**2 * R_dim**2


   open(unit = 1, file=name // '/force_x')
   do i = 1, n_azim, 1
     write(1,*) i, ',', fxa(i), ',', fxi(i), ',', fx(i)
   end do
   close(1)

   open(unit = 1, file=name // '/force_y')
   do i = 1, n_azim, 1
     write(1,*) i, ',', fya(i), ',', fyi(i), ',', fy(i)
   end do
   close(1)

   open(unit = 1, file=name // '/force_z')
   do i = 1, n_azim, 1
     write(1,*) i, ',', fza(i), ',', fzi(i), ',', fz(i)
   end do
   close(1)

   !=======================================================================
   !                          Root Moments
   !=======================================================================
   call get_root_moments()


   mxa = mxa * m0_dim * omega_dim**2 * R_dim**3
   mya = mya * m0_dim * omega_dim**2 * R_dim**3
   mza = mza * m0_dim * omega_dim**2 * R_dim**3
   mxi = mxi * m0_dim * omega_dim**2 * R_dim**3
   myi = myi * m0_dim * omega_dim**2 * R_dim**3
   mzi = mzi * m0_dim * omega_dim**2 * R_dim**3
   mx = mx * m0_dim * omega_dim**2 * R_dim**3
   my = my * m0_dim * omega_dim**2 * R_dim**3
   mz = mz * m0_dim * omega_dim**2 * R_dim**3

   do i = 1, n_azim, 1
     my(i) = my(i) - offset*fz(i)
     mz(i) =  mz(i) + offset * fy(i)
   end do


   open(unit = 1, file=name // '/moment_x')
   do i = 1, n_azim, 1
     write(1,*) i, ',', mxa(i), ',', mxi(i), ',', mx(i)
   end do
   close(1)

   open(unit = 1, file=name // '/moment_y')
   do i = 1, n_azim, 1
     write(1,*) i, ',', mya(i), ',', myi(i), ',', my(i)
   end do
   close(1)

   open(unit = 1, file=name // '/moment_z')
   do i = 1, n_azim, 1
     write(1,*) i, ',', mza(i), ',', mzi(i), ',', mz(i)
   end do
   close(1)


   !=======================================================================
   !                         Spanwise Loads
   !=======================================================================
    call compute_spanwise_load_MM(100, 0.0d0, fz_mm, my_mm)

    fz_mm = fz_mm * m0_dim * omega_dim**2 * R_dim**2
    my_mm = my_mm * m0_dim * omega_dim**2 * R_dim**3

    open(unit = 1, file=name // '/spanwise_mm')
    do i = 1, 100, 1
      write(1,*) i, ',', fz_mm(i), ',', my_mm(i)
    end do
    close(1)

    call compute_spanwise_load_FS(100, 0.0d0, fx_fs, fy_fs, fz_fs, mx_fs, my_fs, mz_fs)

    fx_fs = fx_fs * m0_dim * omega_dim**2 * R_dim**2
    fy_fs = fy_fs * m0_dim * omega_dim**2 * R_dim**2
    fz_fs = fz_fs * m0_dim * omega_dim**2 * R_dim**2

    mx_fs = mx_fs * m0_dim * omega_dim**2 * R_dim**3
    my_fs = my_fs * m0_dim * omega_dim**2 * R_dim**3
    mz_fs = mz_fs * m0_dim * omega_dim**2 * R_dim**3


    open(unit = 1, file=name // '/spanwise_fs')
    do i = 1, 100, 1
      write(1,*) i, ',', fx_fs(i), ',', fy_fs(i), ',', fz_fs(i), ',', mx_fs(i), ',', my_fs(i), ',', mz_fs(i)
    end do
    close(1)



end subroutine write2files
