!======================================================================
! Simultaneous linear equation solver
!======================================================================
      
      subroutine DGESV2(N, A, LDA, B, X)
      use precision
      implicit none

!======================================================================
! Inputs
!======================================================================
      
      integer        , intent(in) :: N,LDA
      real(kind=rdp) , intent(in) :: A(LDA,N),B(N)

!======================================================================
! Outputs
!======================================================================

      real(kind=rdp) , intent(out):: X(N)      

!======================================================================
! Local variables
!======================================================================

      integer                    :: ii,jj,IPIV(N),info
      real(kind=rdp)             :: ANET(N,N),BNET(N,1)

!     COPY THE RIGHT HAND SIDE TO THE NEW STORAGE AREA

      DO II = 1, N
         BNET( II ,1 ) = B( II )
         DO JJ = 1, N
            ANET( II, JJ ) = A ( II, JJ )
         ENDDO
      ENDDO

!     CALCULATE THE SOLUTION OF THE LINEAR SYSTEM
!#ifdef usedouble
      CALL DGESV( N, 1, ANET, N, IPIV, BNET, N, INFO )
!#else
!      call sgesv(N,1,ANET,N,IPIV,BNET,N,INFO)
!#endif

!     CHECK THE STATUS OF THE NETLIB SUBROUTINE
      IF(INFO.NE.0) THEN
         WRITE(6,*) ' WARNING: PROBLEM WITH DGESV OUTPUT IN netkey1'
         
      ENDIF

!     COPY THE SOLUTION TO THE OUTPUT VECTOR

      DO II = 1, N
         X ( II ) = BNET ( II ,1 )
      ENDDO
      return
      end subroutine dgesv2