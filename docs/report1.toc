\babel@toc {english}{}
\contentsline {section}{\numberline {1}Introduction}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}List of Symbols}{6}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Procedure}{8}{subsection.1.2}
\contentsline {paragraph}{Implementation of FEM}{8}{section*.6}
\contentsline {paragraph}{Implementation of FET}{8}{section*.7}
\contentsline {paragraph}{Derivation of Aerodynamic Forcing}{8}{section*.8}
\contentsline {paragraph}{Blade response to Aerodynamic Forcing using FET }{9}{section*.9}
\contentsline {paragraph}{Calculation of Root Loads}{9}{section*.10}
\contentsline {paragraph}{Calculation of Hub Loads}{9}{section*.11}
\contentsline {paragraph}{Trim}{9}{section*.12}
\contentsline {paragraph}{Couple with Free Wake}{9}{section*.13}
\contentsline {subsection}{\numberline {1.3}Reference Frames}{9}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Gravity Frame; [$X_G, Y_G, Z_G$]; [$ \mathaccentV {hat}05E{I_G}, \mathaccentV {hat}05E{J_G}, \mathaccentV {hat}05E{K_G}$]}{9}{subsubsection.1.3.1}
\contentsline {subsubsection}{\numberline {1.3.2}Body Frame; [$X_B, Y_B, Z_B$]; [$ \mathaccentV {hat}05E{I_B}, \mathaccentV {hat}05E{J_B}, \mathaccentV {hat}05E{K_B}$]}{10}{subsubsection.1.3.2}
\contentsline {subsubsection}{\numberline {1.3.3}Hub Frame; [$X_H, Y_H, Z_H$]; [$\mathaccentV {hat}05E{I_H}, \mathaccentV {hat}05E{J_H}, \mathaccentV {hat}05E{K_H}$]}{10}{subsubsection.1.3.3}
\contentsline {subsubsection}{\numberline {1.3.4}Hub Rotating Frame; [$X, Y, Z$]; [$\mathaccentV {hat}05E{I}, \mathaccentV {hat}05E{J}, \mathaccentV {hat}05E{K}$] }{10}{subsubsection.1.3.4}
\contentsline {subsubsection}{\numberline {1.3.5}Undeformed Blade Frame; [$x, y, z$]; [$\mathaccentV {hat}05E{i}, \mathaccentV {hat}05E{j}, \mathaccentV {hat}05E{k}$]}{11}{subsubsection.1.3.5}
\contentsline {subsubsection}{\numberline {1.3.6}Deformed Blade Frame; [$\xi , \eta , \zeta $]; [$\mathaccentV {hat}05E{i_{\xi }}, \mathaccentV {hat}05E{j_{\eta }}, \mathaccentV {hat}05E{k_{\zeta }} $]}{11}{subsubsection.1.3.6}
\contentsline {subsection}{\numberline {1.4}Non-Dimensionalization}{12}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Ordering Scheme}{13}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Blade Parameters}{13}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Aerodynamic and Flight Parameters}{14}{subsection.1.7}
\contentsline {section}{\numberline {2}Mathematical Background}{15}{section.2}
\contentsline {subsection}{\numberline {2.1}Gaussian Quadrature}{15}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Shape Functions}{16}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Hermite Polynomials}{17}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Lagrangian Polynomials}{18}{subsubsection.2.2.2}
\contentsline {paragraph}{First order}{18}{section*.18}
\contentsline {paragraph}{Fifth order}{19}{section*.19}
\contentsline {section}{\numberline {3}Finite Element Method}{20}{section.3}
\contentsline {paragraph}{Kinetic Energy}{21}{section*.21}
\contentsline {paragraph}{Potential Energy}{21}{section*.22}
\contentsline {paragraph}{Virtual Work}{21}{section*.23}
\contentsline {section}{\numberline {4}Natural Frequency and Mode Shapes}{23}{section.4}
\contentsline {subsection}{\numberline {4.1}Hingeless Rotor}{23}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Frequency Plots}{23}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Fan Plots}{24}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Mode Shapes}{25}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Articulated Rotor}{27}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Frequency Plots}{28}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Fan Plots}{28}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Mode Shapes}{29}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}Teetering Rotor}{30}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Frequency Plots}{31}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Fan Plots}{31}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Mode Shapes}{32}{subsubsection.4.3.3}
\contentsline {section}{\numberline {5}Finite Element in Time}{34}{section.5}
\contentsline {section}{\numberline {6}Dynamic Response}{36}{section.6}
\contentsline {subsection}{\numberline {6.1}Sinusoidal Tip Load}{36}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Exact Solution}{36}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Tip Displacement}{37}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Damped Response}{38}{subsubsection.6.1.3}
\contentsline {subsubsection}{\numberline {6.1.4}Velocity Profiles}{39}{subsubsection.6.1.4}
\contentsline {subsection}{\numberline {6.2}Constant Tip Load}{40}{subsection.6.2}
\contentsline {section}{\numberline {7}Aerodynamic Modeling}{41}{section.7}
\contentsline {subsection}{\numberline {7.1}Aerodynamic Reference Frame}{41}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Aerodynamic Velocities}{41}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}Quasi-Steady Airloads}{43}{subsection.7.3}
\contentsline {section}{\numberline {8}Blade Response to Aerodynamic Forcing}{45}{section.8}
\contentsline {subsection}{\numberline {8.1}Method I}{45}{subsection.8.1}
\contentsline {subsubsection}{\numberline {8.1.1}Linear Aerodynamic Model}{45}{subsubsection.8.1.1}
\contentsline {subsubsection}{\numberline {8.1.2}Non-Linear Aerodynamic Model}{46}{subsubsection.8.1.2}
\contentsline {subsection}{\numberline {8.2}Method II}{47}{subsection.8.2}
\contentsline {section}{\numberline {9}Blade Force and Moment Calculation}{48}{section.9}
\contentsline {subsection}{\numberline {9.1}Aerodynamic Forces}{48}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Inertial Forces}{48}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Blade Shears and Moments}{50}{subsection.9.3}
\contentsline {subsection}{\numberline {9.4}Hub Loads}{51}{subsection.9.4}
\contentsline {section}{\numberline {10}Rotor Blade Trim}{53}{section.10}
\contentsline {subsection}{\numberline {10.1}Rigid Blade Trim}{53}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Elastic Blade Trim}{53}{subsection.10.2}
\contentsline {subsubsection}{\numberline {10.2.1}Newton's Method for Solving Nonlinear System of Equations}{53}{subsubsection.10.2.1}
\contentsline {subsubsection}{\numberline {10.2.2}Trim Equations}{54}{subsubsection.10.2.2}
\contentsline {paragraph}{Vertical Force:}{54}{section*.55}
\contentsline {paragraph}{Longitudinal Force:}{54}{section*.56}
\contentsline {paragraph}{Lateral Force:}{54}{section*.57}
\contentsline {paragraph}{Pitching Moment}{54}{section*.58}
\contentsline {paragraph}{Rolling Moment}{54}{section*.59}
\contentsline {subsubsection}{\numberline {10.2.3}Procedure}{57}{subsubsection.10.2.3}
\contentsline {section}{\numberline {11}Linear Aerodynamic Results}{58}{section.11}
\contentsline {subsection}{\numberline {11.1}Tip Displacement}{58}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Root Shears}{59}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Root Moments}{60}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Hub Forces and Moments}{61}{subsection.11.4}
\contentsline {section}{\numberline {12}Non-Linear Aerodynamic Results}{63}{section.12}
\contentsline {subsection}{\numberline {12.1}Tip Displacement}{63}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Root Shears}{64}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Root Moments}{65}{subsection.12.3}
\contentsline {subsection}{\numberline {12.4}Hub Forces and Moments}{65}{subsection.12.4}
\contentsline {section}{\numberline {13}Articulated Rotor Results}{67}{section.13}
\contentsline {subsection}{\numberline {13.1}Tip Displacement}{67}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Root Shears}{68}{subsection.13.2}
\contentsline {subsection}{\numberline {13.3}Root Moments}{68}{subsection.13.3}
\contentsline {subsection}{\numberline {13.4}Hub Forces and Moments}{69}{subsection.13.4}
\contentsline {section}{\numberline {14}Dimensional Cases}{71}{section.14}
\contentsline {subsection}{\numberline {14.1}Tip Displacement}{71}{subsection.14.1}
\contentsline {subsection}{\numberline {14.2}Root Shears}{72}{subsection.14.2}
\contentsline {subsection}{\numberline {14.3}Root Moments}{73}{subsection.14.3}
\contentsline {subsection}{\numberline {14.4}Hub Forces and Moments}{74}{subsection.14.4}
\contentsline {section}{\numberline {15}Free Wake}{75}{section.15}
\contentsline {subsection}{\numberline {15.1}Free Wake Parameters}{76}{subsection.15.1}
\contentsline {subsection}{\numberline {15.2}Hover}{77}{subsection.15.2}
\contentsline {subsection}{\numberline {15.3}Forward Flight}{80}{subsection.15.3}
\contentsline {subsection}{\numberline {15.4}BEMT vs FREE WAKE}{81}{subsection.15.4}
\contentsline {section}{\numberline {16}Teetering Rotor Results}{83}{section.16}
\contentsline {subsection}{\numberline {16.1}Tip Displacement}{83}{subsection.16.1}
\contentsline {subsection}{\numberline {16.2}Root Shears}{85}{subsection.16.2}
\contentsline {subsection}{\numberline {16.3}Root Moments}{85}{subsection.16.3}
\contentsline {subsection}{\numberline {16.4}Hub Forces and Moments}{86}{subsection.16.4}
\contentsline {section}{\numberline {17}Teetering Rotor - Superposition Method}{88}{section.17}
\contentsline {subsection}{\numberline {17.1}Superposition Method vs Single Beam}{89}{subsection.17.1}
\contentsline {subsection}{\numberline {17.2}Root Shears}{90}{subsection.17.2}
\contentsline {subsection}{\numberline {17.3}Root Moments}{91}{subsection.17.3}
\contentsline {subsection}{\numberline {17.4}Hub Forces and Moments}{91}{subsection.17.4}
\contentsline {section}{\numberline {18}Other Types of Teetering Rotor}{93}{section.18}
\contentsline {subsection}{\numberline {18.1}Underslung Teetering}{93}{subsection.18.1}
\contentsline {subsection}{\numberline {18.2}Hinged Teetering}{96}{subsection.18.2}
\contentsline {section}{\numberline {19}Force Summation vs Modal Method}{98}{section.19}
\contentsline {paragraph}{Modal Method :}{98}{section*.132}
\contentsline {subsection}{\numberline {19.1}Hingless Rotor}{98}{subsection.19.1}
\contentsline {subsection}{\numberline {19.2}Articulated Rotor}{100}{subsection.19.2}
\contentsline {section}{\numberline {20}Method II Results and Analysis}{101}{section.20}
\contentsline {section}{\numberline {21}Appendix}{105}{section.21}
\contentsline {subsection}{\numberline {21.1}Derivation of Aerodynamic Velocities}{105}{subsection.21.1}
\contentsline {subsection}{\numberline {21.2}Derivation of Aerodynamic Forces}{105}{subsection.21.2}
\contentsline {subsection}{\numberline {21.3}Derivation of Inertial Loads}{105}{subsection.21.3}
