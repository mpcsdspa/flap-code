!=======================================================================
!> This subroutine computes the hub loads for a given vector
!> of trim parameters.
!                   ~     ~     ~     ~     ~     ~
!                 F_XH, F_YH, F_ZH, M_XH, M_YH, M_ZH
!                   ~     ~     ~     ~     ~     ~
!=======================================================================
subroutine get_hub_loads()
	use loads
	use aerodynamic_parameters
	use blade_parameters
	use my_funcs
	implicit none

	!=======================================================================
	!                             Local Variables
	!======================================================================
	integer                                        :: i, temp_var, j

  
	call compute_harmonic_coeff()

	call compute_root_loads()

	!=======================================================================
	!                        Initialization of Variables
	!======================================================================
	F_XH = 0.0d0
	F_YH = 0.0d0
	F_ZH = 0.0d0
	M_XH = 0.0d0
	M_YH = 0.0d0
	M_ZH = 0.0d0

	call linspace(0.0d0, T, n_azim, time)

	temp_var = (n_azim-1)/Nb + 1

	!=======================================================================
	!                      Compute Hub Forces and Moments
	!=======================================================================

	do i = 1, temp_var, 1
		do j = 1, Nb, 1
			F_ZH(i) = F_ZH(i) + fz(i + (j-1)*(n_azim-1)/Nb) + fx(i + (j-1)*(n_azim-1)/Nb) * beta_p

			F_XH(i) = F_XH(i) + fx(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - fy(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - fz(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb))*beta_p

			F_YH(i) = F_YH(i) + fx(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb)) &
			         + fy(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - fz(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb))*beta_p

			M_ZH(i) = M_ZH(i) + mz(i + (j-1)*(n_azim-1)/Nb) + mx(i + (j-1)*(n_azim-1)/Nb) * beta_p

			M_XH(i) = M_XH(i) + mx(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - my(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - mz(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb))*beta_p

			M_YH(i) = M_YH(i) + mx(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb)) &
			         + my(i + (j-1)*(n_azim-1)/Nb) * cos(time(i + (j-1)*(n_azim-1)/Nb)) &
			         - mz(i + (j-1)*(n_azim-1)/Nb) * sin(time(i + (j-1)*(n_azim-1)/Nb))*beta_p
		end do
	end do


	do i = 1, Nb-1, 1
		F_ZH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = F_ZH(1:temp_var)
		F_XH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = F_XH(1:temp_var)
		F_YH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = F_YH(1:temp_var)
		M_ZH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = M_ZH(1:temp_var)
		M_XH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = M_XH(1:temp_var)
		M_YH( (i*(n_azim-1)/Nb)+1 : ((i+1)*(n_azim-1)/Nb)+1 ) = M_YH(1:temp_var)
	end do


end subroutine get_hub_loads


!-------------------------------------------------------------------------------

!===============================================================================
!> This subroutine computes the root moments and forces for a given vector
!> of trim parameters.
!                   ~     ~     ~     ~     ~     ~    ~
!                 fxa, fya, fza, fxi, fyi, fzi, fx, fy, fz
!                 mxa, mya, mza, mxi, myi, mzi, mx, my, mz
!                   ~     ~     ~     ~     ~     ~    ~
!===============================================================================


subroutine compute_root_loads()
	use finite_elements
	use trim_parameters
	use loads
	use aerodynamic_parameters
	use blade_parameters
	use my_funcs
	implicit none


	!=======================================================================
	!                             Local Variables
	!=======================================================================

	real(kind = 8) 								   :: psi, sf1(2*n_harm+1), sf2(2*n_harm+1), &
													  x_domain(np_spatial),	x_min, x_max, l

	real(kind = 8), dimension(num_modes, n_azim)   :: neta_hin, neta_dot_hin, &
													  neta_double_dot_hin, neta_art, &
													  neta_dot_art, neta_double_dot_art

	real(kind = 8), dimension(2*ne, n_azim) 	   :: q_hin, q_dot_hin, q_ddot_hin

	real(kind = 8), dimension(2*ne+1, n_azim) 	   :: q_art, q_dot_art, q_ddot_art

	real(kind = 8), dimension(n_azim, np_spatial)  :: w_hin, w_dash_hin, w_dot_hin, &	
													  w_dot_dash_hin, w_art, w_dash_art, &
													  w_dot_art, w_dot_dash_art, w_tee, &
													  w_dash_tee, w_dot_tee, w_dot_dash_tee, &
													  w_ddot_hin, w_ddot_dash_hin, &
													  w_ddot_art, w_ddot_dash_art, &
													  w_ddot_tee, w_ddot_dash_tee

  	double precision                               :: L_u_aero, L_v_aero, L_w_aero, &
                                                      L_ubar, L_vbar, L_wbar, M_phi_bar,&
                                                      L_u_inertial, L_v_inertial, L_w_inertial, &
                                                      M_u_aero, M_v_aero, M_w_aero, &
                                                      M_u_inertial, M_v_inertial, M_w_inertial

    double precision                               :: r1, t1, theta_1s, lambda, &
                                                      theta, theta_dot, theta_double_dot,  &
                                                      u_R, u_P, u_T, theta_0, theta_1c, &
                                                      q_vec(nodes, dof_per_element)

    double precision                               :: w, w_dash, w_dot, w_dot_dash, &
                                                      w_double_dot, w_tdot_dash, w_tdot, &
                                                      w_root, w_ddot_dash
    
    integer 									   :: i, k

  	!=======================================================================
  	!                             Main Code
  	!=======================================================================

	call linspace(0.0d0, T, n_azim, time)


	l = R/ne

	fxa = 0.0d0
	fya = 0.0d0
  	fza = 0.0d0
  	fxi = 0.0d0
  	fyi = 0.0d0
  	fzi = 0.0d0

	mxa = 0.0d0
  	mya = 0.0d0
  	mza = 0.0d0
  	mxi = 0.0d0
  	myi = 0.0d0
  	mzi = 0.0d0

  	x_min = offset*R
	x_max = R

	x_domain = 0.5d0*((x_max + x_min) + l_nodes*(x_max - x_min))

	if(.not. allocated(M_hin)) then
		allocate(M_hin(num_modes, num_modes))
		allocate(K_hin(num_modes, num_modes))
		allocate(V_hin(2*ne, num_modes))
		call main_fem(1, 1)
	end if


	if(.not. allocated(M_art)) then
		allocate(M_art(num_modes, num_modes))
		allocate(K_art(num_modes, num_modes))
		allocate(V_art(2*ne+1, num_modes))
		call main_fem(2, 1)
	end if

  
  	!=======================================================================
  	!                         Calculation of Deflections
  	!=======================================================================

	do i = 1, n_azim, 1
		psi = time(i)


		call shape_function(1, psi, sf1)
		neta_hin(:,i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))
		call shape_function_der(1, psi, sf1)
		neta_dot_hin(:, i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))
		call shape_function_double_der(1, psi, sf1)
		neta_double_dot_hin(:, i) = MATMUL(coeff(:, hin_dof), sf1(hin_dof))


		call shape_function(2, psi, sf2)
		neta_art(:,i) = MATMUL(coeff(:, art_dof), sf2(art_dof))
		call shape_function_der(2, psi, sf2)
		neta_dot_art(:, i) = MATMUL(coeff(:, art_dof), sf2(art_dof))
		call shape_function_double_der(2, psi, sf2)
		neta_double_dot_art(:, i) = MATMUL(coeff(:, art_dof), sf2(art_dof))

		q_hin(:, i) = MATMUL(V_hin, neta_hin(:,i))
		q_dot_hin(:,i) = MATMUL(V_hin, neta_dot_hin(:,i))
		q_ddot_hin(:,i) = MATMUL(V_hin, neta_double_dot_hin(:,i))


		q_art(:, i) = MATMUL(V_art, neta_art(:,i))
		q_dot_art(:,i) = MATMUL(V_art, neta_dot_art(:,i))
		q_ddot_art(:,i) = MATMUL(V_art, neta_double_dot_art(:,i))

		call compute_w(q_hin(:,i), q_dot_hin(:,i), q_art(:,i), q_dot_art(:,i), x_domain, np_spatial, &
			           w_hin(i,:), w_dash_hin(i,:), w_dot_hin(i,:), w_dot_dash_hin(i,:), &
			           w_art(i,:), w_dash_art(i,:), w_dot_art(i,:), w_dot_dash_art(i,:))

		call compute_wddot(q_ddot_hin(:,i), q_ddot_art(:,i), x_domain, w_ddot_hin(i,:), w_ddot_dash_hin(i,:), &
						   w_ddot_art(i,:), w_ddot_dash_art(i, :))

		!=======================================================================
		!                	Superposing Hingeless and Articulated
	    !=======================================================================

		w_tee(i,:) = w_hin(i,:) + w_art(i,:)
		w_dash_tee(i,:) = w_dash_hin(i,:) + w_dash_art(i,:)
		w_dot_tee(i,:) = w_dot_hin(i,:) + w_dot_art(i,:)
		w_dot_dash_tee(i,:) = w_dot_dash_hin(i,:) + w_dot_dash_art(i,:)

	    w_ddot_tee(i,:) = w_ddot_hin(i,:) + w_ddot_art(i,:);
	    w_ddot_dash_tee(i,:) = w_ddot_dash_hin(i,:) + w_ddot_dash_art(i,:);

	end do

  	!=======================================================================
  	!                        Extraction of Trim Variables
  	!=======================================================================

	theta_0 = trim_vec(3)
	theta_1c = trim_vec(4)
	theta_1s = trim_vec(5)
	lambda = trim_vec(6)



  !=======================================================================
  !                Loop over spatial points for each azimuth
  !=======================================================================

  do i = 1, n_azim, 1
    psi = omega * time(i)
    do k = 1, np_spatial, 1
      theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)
      theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))
      theta_double_dot = -omega*omega * (theta_1c * cos(psi) + theta_1s * sin(psi))

      !=======================================================================
      !                Compute w, w_dash, w_dot, w_dot_dash
      !=======================================================================
      w = w_tee(i, k)
      w_dash = w_dash_tee(i, k)
      w_dot = w_dot_tee(i,k)
      w_dot_dash = w_dot_dash_tee(i, k)
      w_double_dot = w_ddot_tee(i, k)
      w_ddot_dash = w_ddot_dash_tee(i, k)

      !=======================================================================
      !                Compute w_root, w_dash_root, w_dot_root, w_dot_dash_root
      !=======================================================================
      w_root = 0.0d0


      !=======================================================================
      !                Compute velocities
      !=======================================================================
      call compute_velocities(x_domain(k), time(i), w, w_dash, w_dot, w_dot_dash, u_R, u_P, u_T, &
                              L_u_aero, L_v_aero, L_w_aero, M_phi_bar)

	  L_ubar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_R * u_T)

	  L_vbar = (Lock_no/(6.0d0*Cl_alpha)) * (-Cdo * u_T * u_T - (Cl_o*u_P - Cd1*abs(u_P))*u_T + (Cl_alpha - Cd2)*u_P*u_P)

	  L_wbar = (Lock_no/(6.0d0*Cl_alpha)) * (Cl_o*u_T*u_T - (Cl_alpha + Cdo)*u_T*u_P + Cd1*abs(u_P)*u_P)

	  L_u_aero = (1 - w_dash*w_dash*0.5d0) * L_ubar - w_dash * sin(theta) * L_vbar - w_dash * cos(theta) * L_wbar
	  !L_u_aero = 0.0d0

	  L_v_aero = cos(theta) * L_vbar - sin(theta) * L_wbar

	  L_w_aero = w_dash * L_ubar + (1 - 0.5d0*w_dash*w_dash) * sin(theta)*L_vbar + (1 - w_dash*w_dash*0.5d0)*cos(theta)*L_wbar

	  M_phi_bar = (Lock_no/(6.0d0*Cl_alpha)) * ((chord/R)*(Cmac*(u_T*u_T + u_P*u_P) - f1*u_T*u_P)) - e_d*L_wbar


	  M_u_aero = (1 - 0.5d0*w_dash*w_dash) * M_phi_bar - (w - w_root)*L_v_aero
	  M_v_aero = (w - w_root)*L_u_aero - (x_domain(k) - x_min)*L_w_aero
	  M_w_aero = (x_domain(k) - x_min)*L_v_aero


	  L_u_inertial = - (-x_domain(k) + beta_p * w + 2.0d0*theta_dot*e_g*sin(theta))

	  L_v_inertial = - (-2.0d0*beta_p*w_dot + e_g*cos(theta)*(-theta_dot*theta_dot -2.0d0*beta_p*theta_dot &
	                          - 1.0d0 - 2.0d0*w_dash*theta_dot) + e_g*sin(theta)*(-theta_double_dot - 2.0d0*w_dot_dash))

	  L_w_inertial = - (w_double_dot + beta_p*x_domain(k) &
	                          + e_g*sin(theta)*(-w_dash - 2.0d0*beta_p*theta_dot - theta_dot*theta_dot) &
	                          + e_g*cos(theta)*theta_double_dot)

	  M_u_inertial = - (Km*Km*theta_double_dot + cos(theta)*sin(theta)*(Km2*Km2 - Km1*Km1)*(1 + 2.0d0*w_dash*theta_dot) &
	                          + 2.0d0*w_dot_dash*(Km2*Km2*sin(theta)*sin(theta) + Km1*Km1*cos(theta)*cos(theta)) &
	                          + e_g*cos(theta)*(w_double_dot + beta_p*x_domain(k)) + 2.0d0*e_g*sin(theta)*beta_p*w_dot)

	  M_v_inertial = - (-2.0d0*w_dot_dash*theta_dot*cos(theta)*sin(theta)*(Km2*Km2 - Km1*Km1) &
	                          + (Km2*Km2*sin(theta)*sin(theta) + Km1*Km1*cos(theta)*cos(theta)) * (w_dash &
	                          + beta_p + 2.0d0*theta_dot - 2.0d0*beta_p*theta_dot - w_ddot_dash) &
	                          + e_g*sin(theta)*(beta_p*w - x_domain(k) + beta_p*x_domain(k)*w_dash))
	  !M_v_inertial = 0.0d0

	  M_w_inertial = - (Km*Km*w_dash*theta_double_dot + sin(theta)*cos(theta)*(Km2*Km2 - Km1*Km1)*(w_ddot_dash &
	                          - beta_p - 2.0d0*theta_dot) + 2.0d0*w_dot_dash*theta_dot*(Km2*Km2*cos(theta)*cos(theta) &
	                          + Km1*Km1*sin(theta)*sin(theta)) + e_g*cos(theta)*(x_domain(k) - beta_p*w))

	  M_u_inertial = M_u_inertial - (w - w_root) * L_v_inertial
	  M_v_inertial = M_v_inertial + (w - w_root) * L_u_inertial - (x_domain(k) - x_min)*L_w_inertial
	  M_w_inertial = M_w_inertial + (x_domain(k) - x_min) * L_v_inertial


      mxa(i) = mxa(i) + l_weights(k) * M_u_aero
      mya(i) = mya(i) + l_weights(k) * M_v_aero
      mza(i) = mza(i) + l_weights(k) * M_w_aero

      mxi(i) = mxi(i) + l_weights(k) * M_u_inertial
      myi(i) = myi(i) + l_weights(k) * M_v_inertial
      mzi(i) = mzi(i) + l_weights(k) * M_w_inertial

      fxa(i) = fxa(i) + l_weights(k) * L_u_aero
      fya(i) = fya(i) + l_weights(k) * L_v_aero
      fza(i) = fza(i) + l_weights(k) * L_w_aero

      fxi(i) = fxi(i) + l_weights(k) * L_u_inertial
      fyi(i) = fyi(i) + l_weights(k) * L_v_inertial
      fzi(i) = fzi(i) + l_weights(k) * L_w_inertial

    end do
    mxa(i) = mxa(i) * 0.5d0 * (x_max - x_min)
    mya(i) = mya(i) * 0.5d0 * (x_max - x_min)
    mza(i) = mza(i) * 0.5d0 * (x_max - x_min)
    mxi(i) = mxi(i) * 0.5d0 * (x_max - x_min)
    myi(i) = myi(i) * 0.5d0 * (x_max - x_min)
    mzi(i) = mzi(i) * 0.5d0 * (x_max - x_min)

    fxa(i) = fxa(i) * 0.5d0 * (x_max - x_min)
    fya(i) = fya(i) * 0.5d0 * (x_max - x_min)
    fza(i) = fza(i) * 0.5d0 * (x_max - x_min)
    fxi(i) = fxi(i) * 0.5d0 * (x_max - x_min)
    fyi(i) = fyi(i) * 0.5d0 * (x_max - x_min)
    fzi(i) = fzi(i) * 0.5d0 * (x_max - x_min)

  end do


  mx = mxa + mxi
  my = mya + myi
  mz = mza + mzi

  fx = fxa + fxi
  fy = fya + fyi
  fz = fza + fzi


end subroutine compute_root_loads


!-------------------------------------------------------------------------------

!===============================================================================
!> This subroutine computes the velocities of the blade for a given vector
!> of trim parameters. It also outputs the linearized forces and moments.
!                   ~     ~     ~     ~     ~     ~    ~    ~    ~    ~
!                 u_R, u_P, u_T, L_u_aero, L_v_aero, L_w_aero, M_phi_bar
!                   ~     ~     ~     ~     ~     ~    ~    ~    ~    ~
!===============================================================================

subroutine compute_velocities(x_val, t_val, w, w_dash, w_dot, w_dot_dash, &
                              u_R, u_P, u_T, L_u_aero, L_v_aero, L_w_aero, M_phi_bar)
  use trim_parameters
  use blade_parameters
  use aerodynamic_parameters
  implicit none

  !=======================================================================
  !                             Inputs
  !=======================================================================

  double precision, intent(IN)                   :: x_val, t_val, &
                                                    w, w_dash, w_dot, w_dot_dash


  !=======================================================================
  !                             Outputs
  !=======================================================================

  double precision, intent(OUT)                  :: u_R, u_P, u_T, M_phi_bar, &
                                                    L_u_aero, L_v_aero, L_w_aero

  !=======================================================================
  !                             Local Variables
  !=======================================================================

  double precision                               :: l, dt, psi, x, theta, theta_dot, &
                                                    a0, a1, a2, a3, a4, a5, a6, &
                                                    b0, b1, b2, b3, c0, c1, c2, c3, &
                                                    L0, L1, L2, L3, L4, &
                                                    theta_0, theta_1c, theta_1s, lambda

  double precision                               :: q_vec(nodes, dof_per_element), r1, t1, L_wbar

  !=======================================================================
  !                       Extraction of trim variables
  !=======================================================================

  theta_0 = trim_vec(3)
  theta_1c = trim_vec(4)
  theta_1s = trim_vec(5)
  lambda = trim_vec(6)



  l = R*(1.0d0 - offset)/dble(ne)
  dt = T/nt


  psi = omega * t_val
  x = x_val / R

  theta = theta_0 + theta_1c * cos(psi) + theta_1s * sin(psi)

  theta_dot = omega * (-theta_1c * sin(psi) + theta_1s * cos(psi))


  a0 = -mu*cos(psi) + lambda*beta_p - neta_r*cos(theta)
  a1 = 0.0d0
  a2 = mu*beta_p*cos(psi) + lambda
  a3 = 0.0d0
  a4 = -neta_r*sin(theta)
  a5 = 1.0d0
  a6 = 0.5d0*mu*cos(psi)


  b0 = sin(theta) * (lambda + mu*cos(psi)*beta_p) + cos(theta) * (x + mu*sin(psi))
  b1 = -beta_p * cos(theta)
  b2 = mu*cos(psi)*sin(theta)
  b3 = sin(theta)


  c0 = -sin(theta) * (x + mu*sin(psi)) + cos(theta) * (lambda + mu*beta_p*cos(psi)) + neta_r*(theta_dot + beta_p)
  c1 = beta_p * sin(theta)
  c2 = neta_r + mu*cos(psi)*cos(theta)
  c3 = cos(theta)

  !=======================================================================
  !                         Compute velocities
  !=======================================================================

  u_R = a0 + a1 * w + a2 * w_dash + a3 * w_dot + a4 * w_dot_dash + a5 * w_dot * w_dash + a6 * w_dash * w_dash

  u_T = b0 + b1 * w + b2 * w_dash + b3 * w_dot

  u_P = c0 + c1 * w + c2 * w_dash + c3 * w_dot

  !=======================================================================
  !                      Compute Linearized Aero Forces
  !=======================================================================

  L0 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-Cdo * b0*b0 + Cl_alpha * c0*c0) &
                                - cos(theta) * (Cl_alpha + Cdo)*b0*c0)
  L1 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b1 + 2.0d0*Cl_alpha*c0*c1) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c1 + b1*c0))
  L2 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b2 + 2.0d0*Cl_alpha*c0*c2) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c2 + b2*c0) - Cdo*a0*b0)
  L3 = (Lock_no/(6.0d0*Cl_alpha)) * (sin(theta) * (-2.0d0*Cdo*b0*b3 + 2.0d0*Cl_alpha*c0*c3) &
                                - cos(theta) * (Cl_alpha + Cdo)*(b0*c3 + c0*b3))
  L4 = 0.0d0

  !=======================================================================
  !                             Linearized Aerodynamic Forces
  !=======================================================================

  L_w_aero = L0 + L1 * w + L2 * w_dash + L3 * w_dot + L4 * w_dot_dash

  L_u_aero = 1.0d0 ! Not used

  L_v_aero = 2.0d0 ! Not used

  L_wbar = (Lock_no/(6.0d0*Cl_alpha)) * (Cl_o*u_T*u_T - (Cl_alpha + Cdo)*u_T*u_P + Cd1*abs(u_P)*u_P)

  M_phi_bar = (Lock_no/(6.0d0*Cl_alpha)) * ((chord/R)*(Cmac*(u_T*u_T + u_P*u_P) - f1*u_T*u_P)) - e_d*L_wbar


end subroutine compute_velocities

!-------------------------------------------------------------------------------
