!=======================================================================
!> This subroutine finds the eigenvalues and eigenvectors of
!> a system of second-order oridinary differential equations
!
!                          of the form
!                           ..
!                     [ M ] q    +  [ K ] q    =   0
!                           ~             ~        ~
!=======================================================================

      subroutine compute_modes(TotalDOF, Mall, Kall, Freqs, Modes)

      ! use precision
      implicit none

!=======================================================================
!                             Inputs
!=======================================================================

      integer           , intent(in)  ::  TotalDOF

      real(kind=8)    , intent(in)  ::  Mall(TotalDOF,TotalDOF),      &
                                          Kall(TotalDOF,TotalDOF)

!=======================================================================
!                             Outputs
!=======================================================================

      real(kind=8)    , intent(out) :: Freqs(TotalDOF),               &
                                         Modes(TOtalDOF,TOtalDOF)

!=======================================================================
!                          Local variables
!=======================================================================

      integer                         :: key(TotalDOF) , ierr, i, j

      real(kind=8)                  :: alfr(TotalDOF), alfi(TotalDOF),&
                                         beta(TotalDOF), srtf(TotalDOF),&
                                         evec(TotalDOF,TotalDOF), temp1(TotalDOF,1),&
                                         mu, temp2(1, TotalDOF), M_bar(TotalDOF, TotalDOF), &
                                         K_bar(TotalDOF, TotalDOF)

    M_bar = Mall
    K_bar = Kall

!=======================================================================
!                Compute eigenvalues/eigenvectors
!=======================================================================

      call rgg(TotalDOF,TotalDOF,Kall,Mall,alfr,alfi,beta,1,evec,ierr)

!=======================================================================
! compute frequencies from NETLIB routine outputs
!=======================================================================

      do i=1,TotalDOF
         Freqs(i) = sqrt(abs(alfr(i))/beta(i))        !store frequencies
         key(i)   = i
      end do

!=======================================================================
! sort by order of increasing frequencies
!=======================================================================

      call DLASRT2('i',TotalDOF,Freqs,KEY,ierr)

!=======================================================================
! rearrange eigenvector order (increasing freq)
!=======================================================================

      do i=1,TotalDOF
         Modes(:,i) = evec(:,key(i))
      end do

!=======================================================================


      !call outputra1(Mall, 6, 6)
!=======================================================================
! Extra Code Added - Normalization of eigenvectors ---- Mrinal Added 0 Feb 2, 2018
!=======================================================================

      do i=1,TotalDOF
        do j = 1, TotalDOF, 1
          temp1(j, 1) = Modes(j,i)
        end do
         temp1  = MATMUL(M_bar, temp1)
        do j = 1, TotalDOF, 1
          temp2(1, j) = Modes(j,i)
        end do
        temp1 = MATMUL(temp2, temp1)
        mu = temp1(1,1)
        Modes(:,i) = Modes(:,i)/sqrt(mu)
      end do

!=======================================================================





      return
      end subroutine compute_modes




! !---------------------------------------------------------
! subroutine  outputra1(ra, m, n)
! implicit none
! !will output an real matrix of size MxN nicely
! integer                               :: m, n, row,col
! double precision,dimension(m,n)             :: ra
! character                             :: reply*1
! character(len=10)                     :: Format
! Format = "(3I69)"
! do row =1,m
!   write(*,10) (ra(row,col),col=1,n)
!   10    format(6f8.5)
!    !as we don't know how many numbers are to be output,
!    !specify  !more than we need - the rest are ignored
! end do
! print*,'__________________________________________________'
! print*,'Hit a key and  press enter to continue'
! read *,reply
! end subroutine  outputra1
!---------------------------------------------------------
