!=======================================================================
!> This module contains the parameters which can be made global like
!> can be accessed at any time as long as they are allocated. I should
!> have added more parameters, but its okay!
!=======================================================================


!=======================================================================
!                          Finite Element Parameters
!=======================================================================

module finite_elements

    double precision, allocatable                :: M_hin(:, :), K_hin(:, :), &
                                                    V_hin(:, :), M_art(:,:), Modes_bar1(:, :), &
                                                    K_art(:,:), V_art(:,:), &
                                                    r_left(:), r_right(:)


    integer, allocatable                         :: element_dofs(:, :), b_hin(:), b_art(:), &
                                                    hin_dof(:), art_dof(:)

end module finite_elements

!=======================================================================
!                           Trim  Parameters
!=======================================================================

module trim_parameters

    double precision, allocatable                :: trim_vec(:), coeff_vec(:), coeff(:,:)

    double precision                             :: mu

end module trim_parameters

!=======================================================================
!                          Force Coefficients
!=======================================================================

module trim_residuals

    double precision                             :: C_H, C_Y, C_T, C_MX, C_MY, C_MZ

end module trim_residuals

!=======================================================================
!                          Root and Hub Load Parameters
!=======================================================================

module loads

    integer, parameter                           :: n_azim = 361

    double precision                             :: fxa(n_azim), fya(n_azim), fza(n_azim), &
                                                    fxi(n_azim), fyi(n_azim), fzi(n_azim), &
                                                    fx(n_azim), fy(n_azim), fz(n_azim), &
                                                    mxa(n_azim), mya(n_azim), mza(n_azim), &
                                                    mxi(n_azim), myi(n_azim), mzi(n_azim), &
                                                    mx(n_azim),  my(n_azim), mz(n_azim)

    double precision                             :: F_XH(n_azim), F_YH(n_azim), F_ZH(n_azim), &
                                                    M_XH(n_azim), M_YH(n_azim), M_ZH(n_azim), &
                                                    time(n_azim)

end module loads
