\section {Blade Force and Moment Calculation}

We have seen two different methods for computing the flap response to Aerodynamic Forcing. However, we can solve for the response for any given collective and cyclic inputs. Eventually, the rotor forces and moments determine the trim state of the rotor. Therefore, it is important to compute the blade forces and moments. The rotor forces and moments are a result of inertial and aerodynamic loading on the blade. The shear forces and moments are computed by integrating the forces along the span. Finally, we make use of the Multiblade Coordinate Transformation (MCT), to compute the total rotor forces in the Hub Fixed frame.

\subsection{Aerodynamic Forces}
The blade response was determined by taking into account of Aerodynamic Forces. Once we calculated the blade response using FET, we need to calculate the value of the Aerodynamic Forces using the computed blade response. The aerodynamic forces generate a distributed load along the blade. The complete set of equations for the Aerodynamic forces can be found in the Appendix. Note that, the expressions for the aerodynamic forces vary with the nature of model used in the solution procedure. For linear aerodynamic model, only linear terms retained in the expressions of forces and moments.

\subsection{Inertial Forces}
The dynamics of the flapping rotor will generate inertial loads in the blade. These are derived using the first principles as,
The acceleration of a point on blade is defined using the velocity vector as,
\begin{equation}
\vec{a_p}  = \dot{\vec{v_p}} + \vec{\Omega} \times \vec{v_p}
\end{equation}

On expanding, we get,

\begin{equation}
\vec{a_p} = \ddot{\vec{r_p}} + \vec{\Omega} \times (\vec{\Omega} \times \vec{r_p}) + 2 (\vec{\Omega} \times \dot{\vec{r_p}}) + \dot{\vec{\Omega}} \times \vec{r_p}
\end{equation}

For a constant angular velocity, $\dot{\vec{\Omega}}$ is 0. A complete derivation for determining the accelerations is provided in the Appendix section. Here, we will present the final expressions, which are:

\begin{equation}
\vec{a_p} = a_{px} \hat{i} + a_{py} \hat{j} + a_{pz} \hat{k}
\end{equation}

where,

\begin{equation}
\begin{split}
a_{px} = -\ddot{w}'(\eta sin(\theta) + \zeta cos(\theta)) - 2\dot{w}'\dot{\theta}(\eta cos(\theta) - \zeta sin(\theta)) \\ +
w' \dot{\theta}^2 (\eta sin(\theta) + \zeta cos(\theta)) + \Omega^2 \beta_p(w + \eta sin(\theta) + \zeta cos(\theta)) - \\
\Omega^2 (r - w'(\eta sin(\theta) + \zeta cos(\theta))) + 2 \dot{\theta} \Omega (\eta sin(\theta) + \zeta cos(\theta))
\end{split}
\end{equation}




\begin{equation}
\begin{split}
a_{py} = -\ddot{\theta}(\eta sin(\theta) + \zeta cos(\theta)) - \dot{\theta}^2 (\eta cos(\theta) - \zeta sin(\theta))\\ -\Omega^2 (\eta cos(\theta) - \zeta sin(\theta)) - 2\Omega \dot{w}' (\eta sin(\theta) + \zeta cos(\theta))  \\- 2 \Omega w' \dot{\theta} (\eta cos(\theta) - \zeta sin(\theta))  - 2\Omega \beta_p \dot{w} - 2\Omega \beta_p \dot{\theta} (\eta cos(\theta) - \zeta sin(\theta))
\end{split}
\end{equation}


\begin{equation}
\begin{split}
a_{pz} = \ddot{w} + \ddot{\theta}(\eta cos(\theta) - \zeta sin(\theta)) - \dot{\theta}^2 (\eta sin(\theta) + \zeta cos(\theta)) \\
+ \Omega^2 \beta_p (r - w' (\eta sin(\theta) + \zeta cos(\theta))) - 2\Omega \beta_p \dot{\theta} (\eta sin(\theta) + \zeta cos(\theta))
\end{split}
\end{equation}

Using Newton's Second law, the inertial forces and moments can be determined directly from the accelerations using

\begin{equation}
\vec{F}^I = - \iint_A \rho_s \vec{a} \,d\eta\,d\zeta
\end{equation}


\begin{equation}
\vec{M}^I = - \iint_A \rho_s (\vec{r}_{p,d} \times \vec{a}) \,d\eta\,d\zeta
\end{equation}

In calculating these integrals the following definitions have been used

\begin{equation}
m = - \iint_A \rho_s \,d\eta\,d\zeta
\end{equation}
 

\begin{equation}
0 = - \iint_A \rho_s \zeta \,d\eta\,d\zeta
\end{equation}


\begin{equation}
m e_g = - \iint_A \rho_s \eta \,d\eta\,d\zeta
\end{equation}


\begin{equation}
m K_{m1}^2 = - \iint_A \rho_s \zeta^2 \,d\eta\,d\zeta
\end{equation}


\begin{equation}
m K_{m2}^2 = - \iint_A \rho_s \eta^2 \,d\eta\,d\zeta
\end{equation}
 


The derivations involved in the process are presented in the Appendix. Here, we present the final expressions of the inertial forces and moments on the blade.

\begin{equation}
L_u^I = -m [-x + \beta_p w + 2 \dot{\theta} e_g sin(\theta)]
\end{equation}

\begin{equation}
L_v^I = -m [-2 \beta_p \dot{w} + e_g cos(\theta) (-\dot{\theta}^2 - 2\beta_p \dot{\theta} -1 - \beta_p^2 - w'\dot{\theta}) + e_g sin(\theta) (-\ddot{\theta} - 2 \dot{w}')] 
\end{equation}

\begin{align}
L_w^I = -m [\ddot{w} + \beta_p x - \beta_p^2 w + e_g cos(\theta) \ddot{\theta} - \dot{\theta}^2 e_g sin(\theta) ]
\end{align}


\begin{equation}
\begin{split}
M_u^I = -m [K_m^2 \ddot{\theta} + cos(\theta)sin(\theta) (K_{m2}^2 - K_{m1}^2) ( 1+ \beta_p \dot{\theta} + 2 w' \dot{\theta}) + \\ 2\dot{w}'(K_{m2}^2 sin^2(\theta) + K_{m1}^2 cos^2(\theta)) + e_gcos(\theta)(\ddot{w} + \beta_p x) + 2e_gsin(\theta)\beta_p \dot{w}]
\end{split}
\end{equation}


\begin{equation}
\begin{split}
M_v^I = -m[-2 \dot{w}' \dot{\theta} cos(\theta) sin(\theta) (K_{m2}^2 - K_{m1}^2) + (K_{m2}^2 sin^2(\theta) + K_{m1}^2 cos^2(\theta)) \\(w' + \beta_p + 2\dot{\theta} - 2\beta_p \dot{\theta} - \ddot{w}') + e_g sin(\theta) (\beta_p w - x + \beta_p x w')   ]
\end{split}
\end{equation}

\begin{equation}
\begin{split}
M_vw^I = -m[K_m^2 w' \ddot{\theta} + sin(\theta) cos(\theta) (K_{m2}^2 - K_{m1}^2) (\ddot{w}' - \beta_p - 2\dot{\theta}) + \\
2\dot{w}' \dot{\theta} (K_{m2}^2 cos^2(\theta) + K_{m1}^2 sin^2(\theta)) + e_g cos(\theta) (x - \beta_p w) ]
\end{split}
\end{equation}

Note that some of the above inertial loads are a function of $\ddot{w}$. Until now, we never had the need to compute to $\ddot{w}$. One way of computing  $\ddot{w}$ is to double differentiate the time shape functions. However, this method provides garbage results as the time shape functions are just displacement continuous. So, we can use a higher order shape function which is displacement, velocity continuous at the nodes. There is a much better way to compute  $\ddot{w}$, which is to use the following equation,

\begin{equation}
\ddot{q} = M^{-1} (Q - K q - C \dot{q})
\end{equation}

Once we know the  $\ddot{q}$ vector,  $\ddot{w}$ is computed by interpolation using Hermite polynomials. The net rotor forces and moments is the vector sum of inertial and aerodynamic components. Using these, we will compute the blade shear force and moments,which in turn are used to compute the Hub forces required for trim analysis.

\begin{align}
L_u = L_u^I + L_u^A \\
L_v =  L_v^I + L_v^A  \\
L_w =  L_w^I + L_w^A \\
M_u =  M_u^I + M_u^A \\
M_v =  M_v^I + M_v^A \\
M_w =   M_w^I + M_w^A \\
\end{align}

\subsection{Blade Shears and Moments}
Once the aerodynamic and inertial forces and moments are calculated, the blade shears and moments are computed using the following equations. The blade shear and moment at any spanwise location $x_o$ is calculated as,

\[
\begin{pmatrix}
F_x(x_o)       \\
F_y(x_o)        \\
F_z(x_o)
\end{pmatrix}
=
\int_{x_o}^{1}
\begin{pmatrix}
L_u \\
L_v \\
L_w
\end{pmatrix}\quad
dx
\]


\[
\begin{pmatrix}
M_x(x_o)       \\
M_y(x_o)        \\
M_z(x_o)
\end{pmatrix}
=
\int_{x_o}^{1}
\begin{pmatrix}
M_u - (w(x) - w(x_o))L_v \\
M_v + (w(x) - w(x_o))L_u  - (x - x_o)L_w \\
M_w + (x - x_o)L_v
\end{pmatrix}\quad
dx
\]

For a blade with hinge offset, e, the blade root shear and moment are calculated in a more general way using  the below expressions. Note that for e=0, the below expressions are identical to the above expressions.

\[
\begin{pmatrix}
F_{x,h}       \\
F_{y,h}        \\
F_{z,h} 
\end{pmatrix}
=
\int_{e}^{1}
\begin{pmatrix}
L_u \\
L_v \\
L_w
\end{pmatrix}\quad
dx
\]

\[
\begin{pmatrix}
M_{x,h}       \\
M_{y,h}        \\
M_{z,h} 
\end{pmatrix}
=
\begin{pmatrix}
M_x^b(e) \\
M_y^b(e) - eF_z^b(e) \\
M_z^b(e) + eF_z^b(e)
\end{pmatrix}\quad
\]

This method of computing the forces and moments in the blade is called as $\textbf{Force}$ $\textbf{Summation Method}$. There is also a less accurate but a faster way to compute the blade forces and moments, known as the $\textbf{Modal Method}$. We will see the similarity and difference between the two in the later sections of this report. In the above expressions, $F_{x,h}, M_{x,h}$ are the force and moment at the hub in X direction. Similar notations are used for Y and Z directions. Note that, the above computation of integrals involves both inertial and aerodynamic forces. We can compute the inertial and aerodynamic forces and moments separately, however the total force and moment is the vectoral sum of the both at the location.


\subsection{Hub Loads}

After we have computed the blade shears and moments at any spanwise location, we need to compute the total Hub forces and moments, which are used in the Trim analysis of the rotor. Consider a rotor with $N_b$ identical blades, then using the Multiblade Coordinate Transformation (MCT), the rotor forces and moments in the hub fixed frame are given by, 


\begin{equation}
F_X^H(\psi)  = \sum_{m=1}^{N_b} (F_{x,h}^m cos(\psi_m) - F_{y,h}^m sin(\psi_m) - F_{z,h}^m cos(\psi) \beta_p)
\end{equation}


\begin{equation}
F_Y^H(\psi)  = \sum_{m=1}^{N_b} (F_{x,h}^m sin(\psi_m) + F_{y,h}^m cos(\psi_m) - F_{z,h}^m sin(\psi) \beta_p)
\end{equation}

\begin{equation}
F_Z^H(\psi)  = \sum_{m=1}^{N_b} (F_{z,h}^m + F_{x,h}^m \beta_p)
\end{equation}



\begin{equation}
M_X^H(\psi)  = \sum_{m=1}^{N_b} (M_{x,h}^m cos(\psi_m) - M_{y,h}^m sin(\psi_m) - M_{z,h}^m cos(\psi) \beta_p)
\end{equation}


\begin{equation}
M_Y^H(\psi)  = \sum_{m=1}^{N_b} (M_{x,h}^m sin(\psi_m) + M_{y,h}^m cos(\psi_m) - M_{z,h}^m sin(\psi) \beta_p)
\end{equation}

\begin{equation}
M_Z^H(\psi)  = \sum_{m=1}^{N_b} (M_{z,h}^m + M_{x,h}^m \beta_p)
\end{equation}


For a tracked and balanced rotor, the forces and moments at the hub contain only the harmonics which are integer multiples of the number of blades. This can be seen easily for a 4-bladed rotor, by expanding the summation in the above expressions.

The trim equations for the rotor are dealt in the next section. For a helicopter in a trim state, the average forces and moments generated by the main rotor are balanced by forces and moments generated by other parts. Here, the average is computed over time taken to complete one rotor revolution. For example, the steady component of the vertical force is computed as,

\begin{equation}
T = \frac{1}{2\pi} \int_{0}^{2\pi} F_Z^H(\psi) d\psi
\end{equation}

The non-dimensional expressions for the hub forces and moments are computed using the following expressions. The derivations pertaining to these expressions can found in the Appendix.

\begin{equation}
\begin{split}
C_H = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} F_X^H(\psi) d \psi \\
C_Y = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} F_Y^H(\psi) d \psi \\
C_T = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} F_Z^H(\psi) d \psi \\
C_{Mx} = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} M_X^H(\psi) d \psi \\
C_{My} = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} M_Y^H(\psi) d \psi \\
C_{Mz} = \frac{3 \sigma a}{2 \pi \gamma N_b} \int_{0}^{2 \pi} M_Z^H(\psi) d \psi 
\end{split}
\end{equation}


