!=======================================================================
!> This module contains the simple subroutines which help to perform
!> simple calculations. These are readymade functions present in PYTHON,
!> MATLAB.
!=======================================================================
module my_funcs
  implicit none

  contains
  !=======================================================================
  !> This subroutine divides the space between x_min and x_max into
  !> n divisions, returning x. (MATLAB, PYTHON)
  !=======================================================================
  subroutine linspace(x_min, x_max, n, x)

    !=======================================================================
    !                             Inputs
    !=======================================================================
    double precision, intent(IN)                 :: x_min, x_max

    integer, intent(IN)                          :: n

    !=======================================================================
    !                             Outputs
    !=======================================================================
    double precision, dimension(n), intent(OUT)  :: x

    !=======================================================================
    !                             Local Variables
    !=======================================================================
    integer                                      :: i

    double precision                             :: dx

    dx = (x_max - x_min)/(n-1)

    do i = 1, n, 1
      x(i) = x_min + (i-1) * dx
    end do

  end subroutine linspace

!-------------------------------------------------------------------------------

  !=======================================================================
  !> This subroutine divides the returns the Identity matrix I of size N.
  !=======================================================================
  subroutine eye(N, I)

    !=======================================================================
    !                             Inputs
    !=======================================================================
    integer, intent(IN)                          :: N

    !=======================================================================
    !                             Outputs
    !=======================================================================
    double precision, dimension(N,N), intent(OUT) :: I

    !=======================================================================
    !                             Local Variables
    !=======================================================================
    integer                                      :: k, j


    I = 0.0d0

    do k = 1, N, 1
      do j = 1, N, 1
        if ( k == j ) then
          I(k,j) = 1.0d0
        end if
      end do
    end do

  end subroutine eye

!-------------------------------------------------------------------------------

  !=======================================================================
  !> This subroutine performs the linear Interpolation (NOT USED)
  !=======================================================================

  subroutine interp1(r1, r2, r, f1, f2, f)

    double precision, intent(IN) :: r1, r2, r, f1, f2

    double precision, intent(OUT) :: f

    f = f1 + (r - r1) * (f2 - f1) / (r2 - r1)

  end subroutine interp1

!-------------------------------------------------------------------------------

  !=======================================================================
  !> This subroutine performs the linear Interpolation (NOT USED)
  !=======================================================================

  subroutine interp2(N, x, v, Nq, xq, vq)

    integer, intent(IN) :: N
    double precision, intent(IN) :: x(N), v(N)

    integer, intent(IN) :: Nq
    double precision, intent(IN) :: xq(Nq)

    double precision, intent(OUT) :: vq(Nq)

    integer :: i, j

    double precision :: r1, r2, f1, f2

    do i = 1, Nq, 1

      do j = 1, N, 1
        if ( xq(i) <= x(j+1) .AND. xq(i) >= x(j) ) then
          r1 = x(j)
          r2 = x(j+1)
          f1 = v(j)
          f2 = v(j+1)
          EXIT
        end if
      end do

      vq(i) = f1 + (xq(i) - r1) * (f2 - f1) / (r2 - r1)

    end do


  end subroutine interp2

!-------------------------------------------------------------------------------


end module my_funcs

!-------------------------------------------------------------------------------
