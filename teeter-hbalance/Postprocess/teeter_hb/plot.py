import numpy as np
import matplotlib.pyplot as plt

########################################## TIP DISPLACEMENT PLOT ########################################
# file = open('./txt_files/tip_disp', 'r')
# lines = file.read().split('\n')
# file.close()

# r = np.ones(len(lines))
# phi = np.ones(len(lines))
# for i in range(len(lines)-1):
#     r[i], phi[i] = lines[i  ].split(',')


# r[i+1] = r[i]+1
# phi[i+1] = phi[1]


# plt.figure(figsize=(10,8))
# plt.plot((r-1)*(360/50.0), phi, linewidth = 2.0)
# plt.xlabel('Azimuth', fontsize=16)
# plt.ylabel('Tip Displacement(m)', fontsize=16)
# plt.title('Tip Displacement vs Azimuth', fontsize=16)
# plt.xlim([0, 360])
# plt.savefig('./figures/tip_disp.png')




########################################## ROOT FORCES (A, I, T) ########################################

file = open('./txt_files/force_x', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
fxa = np.ones(len(lines)-1)
fxi = np.ones(len(lines)-1)
fx = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], fxa[i], fxi[i], fx[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.subplot(2,1,1)
plt.plot(x, fxa, 'r', linewidth=2.0, label='Aerodynamic')
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{F_x}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.subplot(2,1,2)
plt.plot(x, fxi, linewidth=2.0, label='Inertial')
plt.plot(x, fx, 'k--', linewidth=2.0, label='Total')
plt.xlabel('Azimuth', fontsize=16)
plt.xlim([0, 360])
plt.ylabel(r'$\frac{F_x}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.legend(fontsize=16)
plt.savefig('./figures/root_fx.png')



file = open('./txt_files/force_y', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
fya = np.ones(len(lines)-1)
fyi = np.ones(len(lines)-1)
fy = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], fya[i], fyi[i], fy[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.plot(x, fya, linewidth=2.0, label='Aerodynamic')
plt.plot(x, fyi, linewidth=2.0, label='Inertial')
plt.plot(x, fy, 'k--', linewidth=2.0, label='Total')
plt.title('Chordwise Blade Root Shear vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{F_y}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.savefig('./figures/root_fy.png')



file = open('./txt_files/force_z', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
fza = np.ones(len(lines)-1)
fzi = np.ones(len(lines)-1)
fz = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], fza[i], fzi[i], fz[i] = lines[i].split(',')

plt.figure(figsize=(10,8))
plt.plot(x, fza, linewidth=2.0, label='Aerodynamic')
plt.plot(x, fzi, linewidth=2.0, label='Inertial')
plt.plot(x, fz, 'k--', linewidth=2.0, label='Total')
plt.title('Vertical Blade Root Shear vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{F_z}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.savefig('./figures/root_fz.png')



########################################## ROOT MOMENTS(A, I, T) ########################################

file = open('./txt_files/moment_x', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
mxa = np.ones(len(lines)-1)
mxi = np.ones(len(lines)-1)
mx = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], mxa[i], mxi[i], mx[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.plot(x, mxa, 'r', linewidth=2.0, label='Aerodynamic')
plt.plot(x, mxi, linewidth=2.0, label='Inertial')
plt.plot(x, mx, 'k--', linewidth=2.0, label='Total')
plt.title('Torsional Blade Root Moment vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{M_x}{m_0 \Omega^2 R^3}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.savefig('./figures/root_mx.png')



file = open('./txt_files/moment_y', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
mya = np.ones(len(lines)-1)
myi = np.ones(len(lines)-1)
my = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], mya[i], myi[i], my[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.plot(x, mya, linewidth=2.0, label='Aerodynamic')
plt.plot(x, myi, linewidth=2.0, label='Inertial')
plt.plot(x, my, 'k--', linewidth=2.0, label='Total')
plt.title('Flapping Blade Root Moment vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{M_y}{m_0 \Omega^2 R^3}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.savefig('./figures/root_my.png')



file = open('./txt_files/moment_z', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
mza = np.ones(len(lines)-1)
mzi = np.ones(len(lines)-1)
mz = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], mza[i], mzi[i], mz[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.plot(x, mza, linewidth=2.0, label='Aerodynamic')
plt.plot(x, mzi, linewidth=2.0, label='Inertial')
plt.plot(x, mz, 'k--', linewidth=2.0, label='Total')
plt.title('Lagging Blade Root Moment vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{M_z}{m_0 \Omega^2 R^3}$', fontsize=16)
plt.xlim([0, 360])
plt.legend(fontsize=16)
plt.savefig('./figures/root_mz.png')


############################### HUB FORCES ###################################
file = open('./txt_files/hub_forces', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
F_XH = np.ones(len(lines)-1)
F_YH = np.ones(len(lines)-1)
F_ZH = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], F_XH[i], F_YH[i], F_ZH[i] = lines[i].split(',')

plt.figure(figsize=(10,8))
plt.plot(x, F_XH, label='Longitudinal')
plt.plot(x, F_YH, label='Lateral')
plt.legend(fontsize=16)
plt.title('Longitudinal and Lateral Hub forces vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{F_h}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.xlim([0, 2*np.pi])
plt.savefig('./figures/hub_fxy.png')


plt.figure(figsize=(10,8))
plt.plot(x, F_ZH)
plt.title('Vertical Hub force vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{F_{zh}}{m_0 \Omega^2 R^2}$', fontsize=16)
plt.xlim([0, 2*np.pi])
plt.savefig('./figures/hub_fz.png')


############################### HUB MOMENTS ###################################
file = open('./txt_files/hub_moments', 'r')
lines = file.read().split('\n')
file.close()

x = np.ones(len(lines)-1)
M_XH = np.ones(len(lines)-1)
M_YH = np.ones(len(lines)-1)
M_ZH = np.ones(len(lines)-1)
for i in range(len(lines)-1):
    x[i], M_XH[i], M_YH[i], M_ZH[i] = lines[i].split(',')


plt.figure(figsize=(10,8))
plt.plot(x, M_XH, label='Rolling')
plt.plot(x, M_YH, label='Pitching')
plt.title('Rolling and Pitching Hub Moments vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{M_h}{m_0 \Omega^2 R^3}$', fontsize=16)
plt.xlim([0, 2*np.pi])
plt.legend(fontsize=16)
plt.savefig('./figures/hub_mxy.png')


plt.figure(figsize=(10,8))
plt.plot(x, M_ZH)
plt.title('Yawing Hub Moment vs Azimuth', fontsize = 20)
plt.xlabel('Azimuth', fontsize=16)
plt.ylabel(r'$\frac{M_{zh}}{m_0 \Omega^2 R^3}$', fontsize=16)
plt.xlim([0, 2*np.pi])
plt.savefig('./figures/hub_mz.png')



# ############################### SPANWISE FX ###################################

# file = open('./txt_files/spanwise_fs', 'r')
# lines = file.read().split('\n')
# file.close()


# x = np.ones(len(lines)-1)
# fx_fs  = np.ones(len(lines)-1)
# fy_fs  = np.ones(len(lines)-1)
# fz_fs  = np.ones(len(lines)-1)
# mx_fs  = np.ones(len(lines)-1)
# my_fs  = np.ones(len(lines)-1)
# mz_fs  = np.ones(len(lines)-1)

# for i in range(len(lines)-1):
#     x[i], fx_fs[i], fy_fs[i], fz_fs[i], mx_fs[i], my_fs[i], mz_fs[i] = lines[i].split(',')


# file = open('./txt_files/spanwise_mm', 'r')
# lines = file.read().split('\n')
# file.close()


# x = np.ones(len(lines)-1)
# fz_mm = np.ones(len(lines)-1)
# my_mm = np.ones(len(lines)-1)

# for i in range(len(lines)-1):
#     x[i], fz_mm[i], my_mm[i] = lines[i].split(',')

# plt.figure(figsize=(10,8))
# plt.plot(x, fx_fs)
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{F_{x}}{m_0 \Omega^2 R^2}$', fontsize=16)
# plt.title('Spanwise Distribution of Fx', fontsize=20)
# plt.savefig('./figures/spanwise_fx.png')


# plt.figure(figsize=(10,8))
# plt.plot(x, fy_fs)
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{F_{y}}{m_0 \Omega^2 R^2}$', fontsize=16)
# plt.title('Spanwise Distribution of Fy', fontsize=20)
# plt.savefig('./figures/spanwise_fy.png')



# plt.figure(figsize=(10,8))
# plt.plot(x, fz_fs, label='Force Summation')
# # plt.plot(x, fz_mm, label='Modal Method')
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{F_{z}}{m_0 \Omega^2 R^2}$', fontsize=16)
# plt.title('Spanwise Distribution of Fz', fontsize=20)
# plt.legend(fontsize=16)
# plt.savefig('./figures/spanwise_fz.png')



# plt.figure(figsize=(10,8))
# plt.plot(x, mx_fs)
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{M_{x}}{m_0 \Omega^2 R^3}$', fontsize=16)
# plt.title('Spanwise Distribution of Mx', fontsize=20)
# plt.savefig('./figures/spanwise_mx.png')




# plt.figure(figsize=(10,8))
# plt.plot(x, my_fs, label='Force Summation')
# plt.plot(x, -my_mm, label='Modal Method')
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{M_{y}}{m_0 \Omega^2 R^3}$', fontsize=16)
# plt.title('Spanwise Distribution of My', fontsize=20)
# plt.legend(fontsize=16)
# plt.savefig('./figures/spanwise_my.png')



# plt.figure(figsize=(10,8))
# plt.plot(x, mz_fs)
# plt.xlabel('x/R', fontsize=16)
# plt.ylabel(r'$\frac{M_{z}}{m_0 \Omega^2 R^3}$', fontsize=16)
# plt.title('Spanwise Distribution of Mz', fontsize=20)
# plt.savefig('./figures/spanwise_mz.png')
