# FLAP-CODE

Flapping is the primary means of compensating for dis-symmetry of lift, thereby making
it a primary motion of the helicopter blades. A code has been developed that determines
the dynamic response of flapping rotor blades. Hingeless, Articulated and Teetering rotors
are analysed in this project. Also, the trim code is coupled with FreeWake Solver to incorporate
the non-uniform inflow. Both Linear and Non-Linear aerodynamic models are implemented in each case.

## Prerequisites

The code is written in Fortran, compiled and tested using "gfortran" compiler.
Some of the plotting code requires Python 2.7 or above with numpy and matplotlib
packages. For freewake, some of the charts are obtained using "gnuplot".
More importantly, the code is automated using a Makefile which is generated using
CMake. So, Installing Cmake is necessary.

## Code Description

#### hingart
	Coupled Trim for Hingeless and Articulated Rotors
#### teeter_sing_beam
	Coupled Trim for Teetering Rotor (2 blades) using Single Beam Method
#### teeter_hbalance
	Coupled Trim for Teetering Rotor (2 blades) using Harmonic Balance(Superposition Method)
#### freewake
	Coupled Trim with Freewake

Most of the input parameters are set in the module namely blade_parameters.f95, aerodynamic_parameters.f95. Documentation is added in the code when necessary. All the post processed data and figures can be found in the
Postprocess directory present in each directory.

## Documentation

 A detailed report is also provided which includes all the results obtained and a brief description on them.

## Execute the Code

Consider teetering rotor using harmonic balance,
  cd teeter_hbalance #(or any particular folder except freewake)  
  cd build  
  cmake ../src  
  make  
  ./execute

For freewake,  
  cd freewake/bin  
  make
  ./execute
  

Thanks to Ananth S, Bharath G, Tyler and all my labmates.




