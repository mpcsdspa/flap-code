\section{Teetering Rotor - Superposition Method}

We know that the modes of a teetering rotor are a combination of both hingeless and articulated rotor, i.e. the teetering rotor behaves as an hingeless for even harmonics (symmetric) and as articulated for odd harmonics (anti-symmetric). The above statement is true for two bladed teetering rotors. For higher number of blades, the hingeless and articulated blade should be excited with their corresponding harmonics. A very important note to keep in mind is that, this method is efficient when implemented in modal space. This method is more of a Galerkin type Approach, no finite element in time is used.

The modal coordinate of the $i^{th}$ mode is expressed as:
\begin{equation}
\eta_i = \sum_{1}^{N} A_{i,j} h_{j}(\psi)
\end{equation}

where $h_j(\psi)$ are the fourier series functions given by

\begin{equation}
  h_j(\psi)=\begin{cases}
1, & \text{if $j = 1$}.\\
cos(\frac{j}{2}\psi), & \text{if $j > 1$ and even}.\\
sin(\frac{j-1}{2}\psi), & \text{if $j > 1$ and odd}.
\end{cases}
\end{equation}

The superposition method is implemented in the following way:

\begin{enumerate}
	\item Assume a set of harmonic coefficients, say zeros
	
	\item Obtain the $\eta_{hin}$, $\eta_{art}$ by the above fourier series expansion. Similary obtain the first and second order derivatives in time of $\eta_{hin}$, $\eta_{art}$.
	
	\item Transform the above $\eta$ vectors to physical space, say $q_{hin}$, $q_{art}$ vectors.
	
	\item Using $q_{hin}$, $q_{art}$ vectors, compute the $w_{hin}$, $w'_{hin}$, $\dot{w}_{hin}$, $\dot{w'}_{hin}$ and $w_{art}$, $w'_{art}$, $\dot{w}_{art}$, $\dot{w'}_{art}$
	
	\item Compute the deflections of teetering rotor, $w_{tee}$, $w'_{tee}$, $\dot{w}_{tee}$, $\dot{w'}_{tee}$ as a sum of $w_{hin}$, $w'_{hin}$, $\dot{w}_{hin}$, $\dot{w'}_{hin}$ and $w_{art}$, $w'_{art}$, $\dot{w}_{art}$, $\dot{w'}_{art}$
	
	\item Using the displacments of teetering rotor ($w_{tee}$), compute the non-linear aerodynamic force. Obtain the odd and even harmonics of this force using the below equations:
	
	\begin{equation}
	A_o(r) = \frac{1}{2\pi} \int_{0}^{2\pi} F(r, \psi) d\psi
	\end{equation}
	\begin{equation}
	A_{mc}(r) = \frac{1}{\pi} \int_{0}^{2\pi} F(r, \psi) cos(m \psi) d\psi
	\end{equation}
	\begin{equation}
	A_{ms}(r) = \frac{1}{\pi} \int_{0}^{2\pi} F(r, \psi) sin(m \psi) d\psi
	\end{equation}
	
	\item Compute the $Q_{hin}$ and $Q_{art}$ (RHS vector) for hingeless and articulated separately based on the even and odd harmonics respectively.
	
	
	\item Obtain the Mass and Stiffness matrices for hingeless and articulated rotors, say $[M_{hin}, K_{hin}]$ and $[M_{art}, K_{art}]$. Compute the error as,
	
	\begin{equation}
	\epsilon_{hin} = M_{hin} \ddot{\eta}_{hin} + K_{hin} \eta_{hin} - Q_{hin}
	\end{equation}
	
	
	\begin{equation}
	\epsilon_{art} = M_{art} \ddot{\eta}_{art} + K_{art} \eta_{art} - Q_{art}
	\end{equation}
	
	\item Compute the residual vector for each harmonic coefficient as,
	
	\begin{equation}
	\epsilon_{i,j} = \int_{0}^{2\pi} \epsilon_{i}(\psi) h_{j}(\psi) d\psi 
	\end{equation}
	
	\item Compute the Jacobian by perturbing each of the harmonic coefficient by a small amount. Iterate till the norm of the residual goes below the tolerance.
	
	\item For performing trim, we follow similar steps as stated in the earlier sections.
	

\end{enumerate}

\subsection{Superposition Method vs Single Beam}

In this section, we will compare the results obtained from the above method with the Single Beam method. In the below figure, we see that the tip displacements obtain from both the methods are the same. The superposition method results were obtained by using harmonics till 5/rev. The below plots were obtained for a fixed trim parameters.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teetering_nonlinear/hover.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teetering_nonlinear/ff.png}
	\end{minipage}
	\caption{Comparison of tip displacements for Single beam and Superposition method.}
\end{figure}

The trim values for $\mu=0.3$ obtained from the superposition method are mentioned  below. We see that they are almost same to the values obtained from single beam method.


\begin{table}[H]
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
\multicolumn{2}{|c|}{$\textbf{Trim Values}$} & \multicolumn{2}{c|}{$\textbf{Force Coefficients}$} \\ \hline
		$\bm{\alpha_s}$               & 0.202               & $\bm{C_H}$                 & $2.1 \times 10^{-5}$                  \\ \hline
		$\bm{\phi_s}$               & 0.099               & $\bm{C_Y}$                  & $-1.03 \times 10^{-5}$                  \\ \hline
		$\bm{\theta_0}$              & 6.344               & $\bm{C_T}$                  & 0.00595                  \\ \hline
		$\bm{\theta_{1c}}$               & 1.13               & $\bm{C_{MX}}$                  & $-2.1 \times 10^{-6}$                 \\ \hline
		$\bm{\theta_{1s}}$               & -4.71               & $\bm{C_{MY}}$                  & $4.19 \times 10^{-6}$                 \\ \hline
		$\bm{\lambda}$               & 0.011               & $\bm{C_{MZ}}$                  & $-1.91 \times 10^{-4}$                  \\ \hline
	\end{tabular}
\caption{Trim values and force coefficients from superposition method.}
\end{table}



\subsection{Root Shears}
The plots shown below correspond to $\mu = 0.3$. The inertial component of spanwise root shear is 0.5. The inertial component of the chordwise root shear is 0. In case of vertical root shear, the aerodynamic and inertial are out of phase with each other.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Fx.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Fy.png}
	\end{minipage}
	\caption{Root shears for non-linear aerodynamics for Teetering rotor. The figure on the left corresponds to spanwise blade root shear and on the right corresponds to chordwise blade root shear.}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./teeter_superposition/Fz.png}
	\caption{Blade root shear in the vertical direction for Teetering rotor.}
\end{figure}

\subsection{Root Moments}
The root moments are computed using the Force summation method as described above. In lagging moment, the inertial contribution is zero. The flapping blade moment is more less looks similar to that ofa hingeless rotor. 

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Mx.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/My.png}
	\end{minipage}
	\caption{Root moments for non-linear aerodynamics for Teetering rotor. The figure on the left corresponds to torsional blade root moment and on the right corresponds to flapping blade root moment.}
\end{figure}


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./teeter_superposition/Mz.png}
	\caption{Lagging Blade root moment for linear aerodynamics for Teetering rotor.}
\end{figure}


\subsection{Hub Forces and Moments}
The hub forces are the total forces on the hub due to all the blades. Since the rotor is 2 bladed, the hub forces and moments contain integral multiples of 2/rev harmonics. From the below plots, we see that the dominant harmonics in each of the hub forces and moments is 2/rev.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Fxyh.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Fzh.png}
	\end{minipage}
	\caption{Hub forces for nonlinear aerodynamics for Teetering rotor. The figure on the left corresponds to Longitudinal and lateral hub forces and on the right corresponds to vertical hub force.}
\end{figure}



\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Mxyh.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./teeter_superposition/Mzh.png}
	\end{minipage}
	\caption{Hub Moments for nonlinear aerodynamics for Teetering rotor. The figure on the left corresponds to Rolling and Pitching hub moments and on the right corresponds to Yawing hub moment.}
\end{figure}

