program main_procedure
  use trim_parameters
  use blade_parameters
  implicit none

  !=======================================================================
  !                         Time Stamp Parameters
  !=======================================================================
  integer                                        :: a0, b0, c0, a1, b1, c1


  if ( rotor_type == 1 ) then
    print *, '**************************************************************'
    print *, 'Coupled Trim for Hingeless Rotor'
    print *, '**************************************************************'
  else if (rotor_type == 2) then
    print *, '**************************************************************'
    print *, 'Coupled Trim for Articulated Rotor'
    print *, '**************************************************************'
  end if

  call system_clock(a0, b0, c0)
  mu = 0.0d0
  call trim()
  call system_clock(a1, b1, c1)
  print  *, '------------------------------------------------------------'
  print  *, '------------------------------------------------------------'
  print *, 'The time elapsed is', (a1-a0)/1000.0, 'seconds'
  print  *, '------------------------------------------------------------'
  print  *, '------------------------------------------------------------'


end program main_procedure
