\section{Other Types of Teetering Rotor}

\subsection{Underslung Teetering}
Basically, underslung Teetering rotor is a teetering rotor with a vertical offset between the flap hinge and the hub. The vertical offset is given to reduce the effect of Coriolis forces as the blade flaps. To model the underslung teetering rotor, we use an extra parameter namely, the teeter angle $\beta_T$. $\beta_T$ is usually the followed notation to call the teeter angle. However, in my analysis and the expressions derived, I have made use of the angle of $\alpha_T$.

Note : The following results shown below are not validated with any of the results. (There might be some mistakes!.)


\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{underslung.png}
	\caption{Model of Underslung Teetering Rotor.}
\end{figure}

From the above figure, the transformation between the undeformed blade frame and the rotating frame is a function of both precone angle, $\beta_p$ and teeter angle $\alpha_T$. Hence, the transformation between the two frames is given by,

\begin{equation}
\begin{pmatrix}
\hat{i}\\ \hat{j}\\ \hat{k}
\end{pmatrix}
=
\begin{bmatrix}
cos(\beta_p + \alpha_T) &  0 & sin(\beta_p+ \alpha_T) \\
0 & 1 & 0 \\
-sin(\beta_p+ \alpha_T) & 0 & cos(\beta_p+ \alpha_T)
\end{bmatrix}
\begin{pmatrix}
\hat{I} \\ \hat{J} \\ \hat{K}
\end{pmatrix}
= T_{UR}
\begin{pmatrix}
\hat{I} \\ \hat{J} \\ \hat{K}
\end{pmatrix}
\end{equation}

The presence of teeter angle forces us to re-derive the expressions for velocities, because the hub is offset from the hinge and is not stationary.
So, the position vector of a point P, on the blade in the undeformed frame is given by,

\begin{equation}
\begin{split}
\vec{r_p}  = [r - w'(\eta sin(\theta) + \zeta cos(\theta) - d sin(\beta_p)] \hat{i} + 
[\eta cos(\theta) - \zeta sin(\theta)] \hat{j} + \\
[w + (1 - \frac{w'^2}{2})(\eta sin(\theta) + \zeta cos(\theta) - dcos(\beta_p))] \hat{k}
\end{split}
\end{equation}

The angular velocity of the undeformed frame is given by,

\begin{equation}
\vec{\Omega} = \Omega sin(\beta_p + \alpha_T) \hat{i} - \dot{\alpha_T} \hat{j}	+ \Omega cos(\beta_p + \alpha_T) \hat{k}
\end{equation}


Using the above equations, we need to compute the expressions for velocities, accelerations, inertial forces and moments. The analysis and the derivations involved in the underslung teetering case are mentioned in the Appendix. Note that, the number of terms in this case are very high and care must be taken while simplifying the expressions. Here, only the final expressions of the velocities are mentioned.


\begin{equation}
\begin{split}
u_R = \frac{U_R}{\Omega R} = [\lambda sin(\beta_p+\alpha_T) - \eta_r cos\theta cos(\beta_p + \alpha_T) - \dot{\alpha_T} \eta_r sin(\theta) -\dot{\alpha_T} d cos(\beta_p)  - \mu cos\psi cos(\beta_p + \alpha_T)] \\ - [\dot{\alpha_T}]w+ (\mu sin(\beta_p+\alpha_T) cos\psi + \lambda cos(\beta_p + \alpha_T)) w' + (-\eta_r sin\theta) \dot{w}' + \dot{w}w' + (\frac{1}{2} \mu cos\psi cos(\beta_p + \alpha_T)) w'^2
\end{split}
\end{equation}


\begin{equation}
\begin{split}
u_T = \frac{U_T}{\Omega R} = [sin\theta (\dot{\alpha_T}x - \dot{\alpha_T}d sin(\beta_p) +  \lambda cos(\beta_p + \alpha_T) + \mu cos\psi sin(\beta_p + \alpha_T)) + cos\theta (x + \mu sin\psi)]  \\+ (-sin(\beta_p+\alpha_T) cos\theta) w + (\mu cos\psi sin\theta cos(\beta_p + \alpha_T) - \lambda sin(\theta) sin(\beta_p + \alpha_T)) w' + (sin\theta) \dot{w}
\end{split}
\end{equation}

\begin{equation}
\begin{split}
u_P = \frac{U_P}{\Omega R} = [\eta_r (\dot{\theta} +\beta_p) - sin\theta (x + \mu sin\psi) + cos\theta (\lambda + \mu \beta_p cos\psi) ] +  (sin(\beta_p+\alpha_T) sin\theta) w \\+ (\eta_r cos(\beta_p + \alpha_T) + \mu cos\psi cos\theta cos(\beta_p + \alpha_T) - \dot{\alpha_T}d cos(\beta_p) - \lambda sin(\beta_p + \alpha_T) cos\theta) w' + (cos\theta)\dot{w}
\end{split}
\end{equation}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.57]{./underslung/tip_disp3.png}
	\caption{Total tip displacement with Non-Linear Aerodynamic Forcing.}
\end{figure}



The tip displacement for the underslung teetering rotor with no precone and vertical offset is shown above. Note that, to obtain this we need $\alpha_T$, which is obtained from the trim. The dominant contribution in the tip displacement is from 1/rev. The shape looks similar to that of teetering rotor with different amplitudes. The aerodynamic loads and the inertial loads are calculated in a similar way as done before. We know that the hub origin is no longer coincident with the shaft, i.e. the hub origin is translating with respect to the shaft. In this case, it is best to use the teeter hinge as the reference point for the trim equations. Hence, all the root forces and moments are translated to the teetering hinge for solving the trim.


\[
\begin{pmatrix}
F_{x,h}      \\
F_{y,h}        \\
F_{z,h}
\end{pmatrix}
=
\begin{pmatrix}
F_x^b(e)\\
F_y^b(e) \\
F_z^b(e)
\end{pmatrix}\quad
\]


\[
\begin{pmatrix}
M_{x,h}      \\
M_{y,h}        \\
M_{z,h}
\end{pmatrix}
=
\begin{pmatrix}
M_x^b(e) - d cos(\beta_p) F_y^b(e)\\
M_y^b(e) + d cos(\beta_p) F_x^b(e) - (d sin(\beta_p) + e) F_z^b(e)\\
M_z^b(e) + (d sin(\beta_p) + e) F_y^b(e)
\end{pmatrix}\quad
\]


\begin{equation}
F_X^H(\psi)  = \sum_{m=1}^{N_b} (F_{x,h}^m cos(\psi_m)cos(\beta_p + \alpha_T) - F_{y,h}^m sin(\psi_m) - F_{z,h}^m cos(\psi) sin(\beta_p + \alpha_T))
\end{equation}


\begin{equation}
F_Y^H(\psi)  = \sum_{m=1}^{N_b} (F_{x,h}^m sin(\psi_m) cos(\beta_p + \alpha_T) + F_{y,h}^m cos(\psi_m) - F_{z,h}^m sin(\psi) sin(\beta_p + \alpha_T))
\end{equation}

\begin{equation}
F_Z^H(\psi)  = \sum_{m=1}^{N_b} (F_{z,h}^m cos(\beta_p + \alpha_T) + F_{x,h}^m cos(\beta_p + \alpha_T))
\end{equation}



\begin{equation}
M_X^H(\psi)  = \sum_{m=1}^{N_b} (M_{x,h}^m cos(\psi_m) cos(\beta_p + \alpha_T) - M_{y,h}^m sin(\psi_m) - M_{z,h}^m cos(\psi) sin(\beta_p + \alpha_T))
\end{equation}


\begin{equation}
M_Y^H(\psi)  = \sum_{m=1}^{N_b} (M_{x,h}^m sin(\psi_m) cos(\beta_p + \alpha_T) + M_{y,h}^m cos(\psi_m) - M_{z,h}^m sin(\psi) sin(\beta_p + \alpha_T))
\end{equation}

\begin{equation}
M_Z^H(\psi)  = \sum_{m=1}^{N_b} (M_{z,h}^m cos(\beta_p + \alpha_T) + M_{x,h}^m sin(\beta_p + \alpha_T))
\end{equation}

After determining these forces and moments, we can calculate the corresponding force and moment coefficients using the expressions mentioned in the Chapter 8. 

How do we bring in the parameter $\alpha_T$ in the trim analysis? We know that the teeter angle is a function of azimuth, so we only the teeter angle cannot be a trim parameter. And we know that $\alpha_T$ is periodic, hence we can use the Fourier series expansion to define $\alpha_T$.

\begin{equation}
\alpha_T = \alpha_{T0} + \alpha_{T1c} cos\psi + \alpha_{T1s} sin\psi + \alpha_{T2c} cos2\psi + \alpha_{T2s} sin2\psi + ....
\end{equation} 

Now, the unknowns are the variables $ \alpha_{T0}, \alpha_{T1c}, \alpha_{T1s}, \alpha_{T2c},  \alpha_{T2s} $. To compute these variables, we make use of the Galerkin Method also known as Harmonic Balance method. 
We know that the moment about the teeter hinge is 0.

\begin{equation}
M_T = \sum_{m=1}^{2} (-1)^m (M_y^m - F_x^m d) = 0
\end{equation}

The residual equation for the above is given by, 

\begin{equation}
\epsilon(\psi) = \sum_{m=1}^{2} (-1)^m (M_y^m - F_x^m d)
\end{equation}

The trim equation for $j^{th}$ harmonic coefficient using Galerkin Method is given by, 
\begin{equation}
\epsilon_j(\psi) = \int_{0}^{2 \pi} \epsilon(\psi) H_j(\psi) d\psi
\end{equation}

where $H_j(\psi)$ can be $cos(\psi), sin(\psi), cos(2\psi), sin(2\psi)$ and so on. Hence, we get N new trim unknowns and N new equations. 


\subsection{Hinged Teetering}

In this type, a hinge is located at an offset from the hub, where the slope of the deflection is discontinuous. So as a result, an extra Degree of Freedom is incorporated into the system on either side of the blade. We need to make sure the matrices are assembled correctly based on the degree of freedoms. A sample figure is shown below.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{./hinged_teetering/image.PNG}
	\caption{Hinged Teetering Model (Source - Ravi Lumba)}
\end{figure}

As shown in the above figure, at the hinge we have three degree of freedoms instead of two. The same thing is observed on the other blade. In reality, we need to make the stiffness of the blades inboard of the hinges more stiffer compared to that of the blade. The introduction of hinge can be considered as reducing the stiffness (decreasing the constraint of continuity of slope), hence the natural frequencies are lower with that of the simple teetering rotor.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.75]{./hinged_teetering/mode_shape.png}
	\caption{Mode shapes of the hinged teetering rotor.}
	
	
\end{figure}