\section{Free Wake}

The Maryland Free Wake program was coupled with the Flap code to obtain a non-uniform inflow distribution. Until now, we have made the assumption of uniform flow for the coupled trim. However, the inflow is uniform only for a ideally twisted hyperbolic rotor. In this section, a brief procedure is provided on how to couple Free Wake program to the Flap code.

\begin{enumerate}
	\item {Perform the coupled trim with constant inflow or linear inflow (inflow is no longer a trim variable)}
	\item {Initialize the $\textbf{Free-Wake}$ derived data type of the Free Wake code.}
	\item {Call the $\textbf{wake-solver}$ subroutine, which is the main program of the Free Wake code.}
	\item {Compute the induced inflow from the Free Wake output}
	\item {Using this inflow, compute the Aerodynamic loads (Linear Interpolation is necessary)}
	\item {Perform the trim iterations with this new inflow}
	\item {Repeat till the control angles converge.}
\end{enumerate}


There are some requirements for coupling your code with the Free Wake program. Some of them, I have tried to mention them here. The Free Wake program takes its inputs in dimensional form. In addition, the inputs ($\Gamma$ distribution, quarter chord points) are to provided at linearly spaced points (both azimuthally and radially) across the blade. Also, the outputs(inflow distribution) are also obtained at linearly spaced points. So, in order to obtain at a particular point, we need to resort to linear interpolation. Some of the inputs like the bound circulation are calculated as,

\begin{equation}
\Gamma = \frac{1}{2} c a \Omega R \sqrt{u_P^2 + u_T^2} tan^{-1}(\frac{-u_P}{u_T})
\end{equation}

The other important calculation is the flap angle and its velocity. This is because the free wake program is based on a rigid blade model.


\begin{equation}
\beta(\psi) = tan^{-1}(\frac{w_{tip}(\psi)}{x_{tip} - x_{root}})
\end{equation}

\begin{equation}
\dot{\beta(\psi)} = tan^{-1}(\frac{\dot{w_{tip}}(\psi)}{x_{tip} - x_{root}})
\end{equation}


After the wake-solver is executed, the inflow distribution is obtained as, 


\begin{equation}
	\lambda = \mu tan(\alpha_s + \beta_{1c} + \theta_{fp}) - \frac{FreeWake\%Fwrtr(1)\%ViZ2}{\Omega R}
\end{equation}

Some brief summary about the free vortex wake methods. Free Vortex Wake methods can be categorized into two types, namely
\begin{enumerate}
	\item $\textbf{Time Accurate Methods}$ : Wake governing equations are integrated in time.
	Example : PCC scheme, PC2B scheme
	\item $\textbf{Steady State Algorithms}$ : Solve for periodic solution directly or by relaxation
methods. Example : pseudo-implicit predictor-corrector (PIPC) relaxation
	formulation
\end{enumerate}

PCC scheme introduces some numerical instabilities ( produced some apparently erratic distortions after about two rotor revolutions.) Some of them fortuitously, replicate the observed (physical) behavior of the rotor wake. But the PC2B scheme provides a stable wake solution. The Free-Wake code (Maryland Free Wake) uses the PC2B (Predictor Corrector
Second Order Backward Difference Scheme), so we end up getting a stable solution. But in reality, the breakdown of wake happens which is difficult to model. One of good sources for the above material is a paper by Mahendra Bhagawat, Leishman, 1998.

The required variables that need to be initialized before calling the Free Wake are mentioned below.

\subsection{Free Wake Parameters}
\begin{table}[H]
	\centering
	\label{my-label}
	\begin{tabular}{|l|r|}
		\hline
		FreeWake              & Derived Data Type \\ \hline
		FreeWake\%add\_inflow & .false.   \\ \hline
		FreeWake\%adim      & Number of azimuth points (exclude 360)          \\ \hline
		FreeWake\%alpha0&Zero Lift Angle of Attack          \\ \hline
		FreeWake\%Altitude	& Operating Altitude           \\ \hline
		FreeWake\%asr&   Shaft Tilt Angle (+ve nose down)        \\ \hline
		FreeWake\%betap & Precone angle (deg)          \\ \hline
		FreeWake\%chord & Chord(m)          \\ \hline
		FreeWake\%dpsi & Spacing of Azimuth points (deg)         \\ \hline
		FreeWake\%flph0 & Hinge offset          \\ \hline
		FreeWake\%Fturn           & Number of wake turns(double precision)          \\ \hline
		FreeWake\%generate\_history			&   .false.        \\ \hline
		FreeWake\%Nblades			&   Number of blades        \\ \hline
		FreeWake\%nfw	& floor(Free\_wake\% Fturn) $\times$ Free\_wake\%Nblades           \\ \hline
		FreeWake\%kdim		&     1      \\ \hline
		FreeWake\%mux	& edge-wise advance ratio parallel to rotor disk          \\ \hline
		FreeWake\%muy	& sideslip advance ratio          \\ \hline
		FreeWake\%	&   z velocity        \\ \hline	
		FreeWake\%Npsi            & Number of Azimuth points(include 360)           \\ \hline
		FreeWake\%Nrotors	& Number of rotors          \\ \hline
		FreeWake\%NSEG & Number of radial locations          \\ \hline
		FreeWake\%omega& Rotating frequency(rad/s)          \\ \hline
		FreeWake\%pb	& Fuselage Roll rate          \\ \hline
		FreeWake\%psi& Azimuth locations(linearly spaced)          \\ \hline
		FreeWake\%qinitial	& 'n' (changes to 'w' after 1 iteration)           \\ \hline
		FreeWake\%qb	& Fuselage pitch rate          \\ \hline
	\end{tabular}
\end{table}

\begin{table}[H]
	\centering
	\label{my-label}
	\begin{tabular}{|l|r|}
		\hline
				FreeWake\%RadiusM  & Rotor Radius (m)           \\ \hline
		FreeWake\%rcout0& Root cut-out (non-dimensional with radius)           \\ \hline
		FreeWake\%rdim & 1          \\ \hline
		FreeWake\%RotGeo & 0           \\ \hline
		FreeWake\%rrx(i) & $i^{th}$ rotor x hub position           \\ \hline
		FreeWake\%rry(i) & $i^{th}$ rotor y hub position           \\ \hline
		FreeWake\%rrz(i) & $i^{th}$ rotor z hub position           \\ \hline
	FreeWake\%sdim	&  NSEG + 1           \\ \hline
	FreeWake\%taper0	& taper ratio          \\ \hline
		FreeWake\%th0(i)&    $i^{th}$ rotor collective (deg)       \\ \hline
		FreeWake\%th1c(i)&    $i^{th}$ rotor Lateral cyclic (deg)       \\ \hline
		FreeWake\%th1s(i)&    $i^{th}$ rotor Long. cyclic (deg)      \\ \hline
		FreeWake\%Turbince\_BL& .false.(to use atm. bdd layer profile)       \\ \hline
	FreeWake\%Turbine\_clearance	& clearance btw wind turbine            \\ \hline
	FreeWake\%wdim	& Number of rolled up trailers (1)           \\ \hline
	FreeWake\%Wind\_turbine	& .false.           \\ \hline
	FreeWake\%vtipSI	& Tip speed(m/s)           \\ \hline
		FreeWake\%zdim &    floor(Free\_wake\% Fturn $\times$ Free\_wake \% adim) + 1        \\ \hline
		Free\_wake\%Fw\_rtr(i)\%beta&  Flap angle over azimuth (rad)         \\ \hline
		Free\_wake\%Fw\_rtr(i)\%betd&  Flap velocity over azimuth  \\ \hline
		Free\_wake\%Fw\_rtr(i)\%chord &  Spanwise distribution of chord (m) \\ \hline
		Free\_wake\%Fw\_rtr(i)\%crdroot & chord root (m)\\ \hline
		Free\_wake\%Fw\_rtr(i)\%crdtip & chord tip (m)\\ \hline
		Free\_wake\%Fw\_rtr(i)\%geomtw & Spanwise twist distribution (deg)\\ \hline
		Free\_wake\%Fw\_rtr(i)\%Fw\_bld(j)\%gmb2 & Bound circulation distribution  ($m^2/s$)\\ \hline
	\end{tabular}
\end{table}


\subsection{Hover}

In this section, we will look into the results obtained from coupling Free Wake with flap code. So, the whole of FreeWake (all the Biot-Savart law and the time marching) returns the parameter $\textbf{inflow}$ which is used for computing the blade aero loads. We observe a drop in the bound circulation near the tip. The maximum bound circulation is the strength of the tip vortex trailed from the tip of the blade. 


\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/inf_hover.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/gam_hover1.png}
	\end{minipage}
	\caption{Inflow and Bound Circulation distribution on the blade in hover.}
\end{figure}


Similarly, the spanwise distribution of lift coefficient and the angle of attack are shown below. We see a similar drop in the lift coefficient near the blade tip due to the formation of tip vortex (increase in up). In Classical theory, this is accounted using Prandtl Tip Loss Factor.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/aoa_hover.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/cl_hover.png}
	\end{minipage}
	\caption{Angle of Attack and Lift Coefficient distribution on the blade in hover.}
\end{figure}

Now, we look into the wake geometry obtained in hover. The side view, top view and the rear view of the wake geometry are shown below. The wake initially contracts for about a rotor radius height and then expands. The expansion of the wake is explained using the Vortex Ring Analogy.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/FWS.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/FWT.png}
	\end{minipage}
	\caption{Side and Top View of the Wake Geometry in Hover.}
\end{figure}




\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{./free-wake/FWR.png}
	\caption{Rear View of Wake Geometry in Hover.}
\end{figure}


Consider the helical wake as vortex rings. Suppose there are two vortex rings one above the
other. The upper ring tries to expand the lower ring while lower ring tries to expand the upper
ring. For a given number of turns, the lower ring does not have anything below it, as a result it
expands. So no matter whatever be the number of turns, we end up getting contraction
initially(near blade) (due to only below rings) while expansion takes place far away from the
blade.
Since the expansion is far from the blade, they have very less effect on the blade loads,
inflow.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.45]{./free-wake/vortex_ring.png}
	\caption{Vortex Ring Analogy.}
\end{figure}



\subsection{Forward Flight}

In the forward flight, we see a non-axisymmetric inflow over the blades. The inflow distribution and the bound circulation distribution for all azimuths over the blades are shown below. The results are obtained for $\mu=0.1$.	

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/inf_ff.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/gam_ff.png}
	\end{minipage}
	\caption{Inflow and Bound Circulation distribution on the blade in forward flight.}
\end{figure}

Now, we look into the wake geometry obtained in forward flight. The side view, top view and the rear view of the wake geometry are shown below.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/FWS_ff.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/FWT_ff.png}
	\end{minipage}
	\caption{Side and Top View of the Wake Geometry in forward flight.}
\end{figure}


For no roll state, the tip vortex strength on the retreating side (270 deg) has to be greater than the advancing side (90 deg). More the tip vortex strength, more is the diffusion. Hence, from the rear view, we see that the wake at the advancing side is more crowded than the one tries to skew to the right (Tip vortex strength at right (90 deg) is less than Tip vortex strength at left (270 deg)).



\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{./free-wake/FWR_ff.png}
	\caption{Rear View of Wake Geometry in Forward Flight.}
\end{figure}



\subsection{BEMT vs FREE WAKE}

In this section, we will compare the results obtained from FREE WAKE with Blade Element Momentum Theory (BEMT) with Prandtl Tip Loss Factor. The below plot is the Power Polar which shows the variation of thrust coefficient with power coefficient.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{./free-wake/hover_pp.png}
	\caption{Power Polar for Hover.}
\end{figure}

Similarly, the induced power factor $\kappa$ and Figure of Merit are obtained for both methods, which are shown below. The Figure of Merit becomes a constant for higher $\frac{C_T}{\sigma}$.

\begin{figure}[H]
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/hover_k.png}
	\end{minipage}
	\hspace{0.5cm}
	\begin{minipage}[b]{0.5\linewidth}
		\centering
		\includegraphics[width=1.\linewidth, height=6cm]{./free-wake/hover_fm.png}
	\end{minipage}
	\caption{Variation of $\kappa$ and Figure of Merit with $\frac{C_T}{\sigma}$ in Hover.}
\end{figure}

