!=======================================================================
!> This subroutine computes the w, w_dash, w_dot, w_dot_dash
!> at a given spatial and time location for hingeless and articulated.
!                           ~   ~   ~   ~   ~   ~   ~   ~
!                   w_hin, w_dash_hin, w_dot_hin, w_dot_dash_hin
!					w_art, w_dash_art, w_dot_art, w_dot_dash_art
!                           ~   ~   ~   ~   ~   ~   ~   ~
!=======================================================================
subroutine compute_w(q_hin, q_dot_hin, q_art, q_dot_art, x_vec, sz, w_hin, w_dash_hin, w_dot_hin, w_dot_dash_hin, &
					w_art, w_dash_art, w_dot_art, w_dot_dash_art)
	use hermite
	use blade_parameters
	use finite_elements
	implicit none

	!=======================================================================
	!                                 Inputs
	!=======================================================================

	integer, intent(IN) 						   :: sz

	real(kind = 8), intent(IN) 					   :: q_hin(2*ne, 1), q_dot_hin(2*ne, 1), &
								  					  q_art(2*ne+1, 1), q_dot_art(2*ne+1, 1), &
								  					  x_vec(sz)

	!=======================================================================
	!                                 Outputs
	!=======================================================================

	real(kind = 8), intent(OUT) 				   :: w_hin(1, np_spatial*ne), &
								   					  w_dash_hin(1, np_spatial*ne), &
								   					  w_dot_hin(1, np_spatial*ne), &
								   					  w_dot_dash_hin(1, np_spatial*ne)

	real(kind = 8), intent(OUT) 				   :: w_art(1, np_spatial*ne), &
								   					  w_dash_art(1, np_spatial*ne), &
								   					  w_dot_art(1, np_spatial*ne), &
								   					  w_dot_dash_art(1, np_spatial*ne)								   					  

	!=======================================================================
	!                             Local Variables
	!=======================================================================

	integer 									   :: i, x_element_no					

	real(kind = 8)                                 :: r1, l, q_hin_temp(2*ne+2, 1), &
													  q_dot_hin_temp(2*ne+2, 1), &
													  q_art_temp(2*ne+2, 1), q_dot_art_temp(2*ne+2, 1), &
													  h(dof_per_element), h_dash(dof_per_element), &
													  q1_hin(dof_per_element), q1_dot_hin(dof_per_element), &
													  q1_art(dof_per_element), q1_dot_art(dof_per_element)		  

	!=======================================================================
	!                             Main Code
	!=======================================================================

	q_hin_temp(1:2, 1) = 0.0d0
	q_hin_temp(3:, 1) = q_hin(:, 1)
	q_dot_hin_temp(1:2, 1) = 0.0d0
	q_dot_hin_temp(3:, 1) = q_dot_hin(:, 1)


	q_art_temp(1, 1) = 0.0d0
	q_art_temp(2:,1) = q_art(:,1)
	q_dot_art_temp(1, 1) = 0.0d0
	q_dot_art_temp(2:,1) = q_dot_art(:,1)

	l = R/ne

	do i = 1, SIZE(x_vec), 1
		x_element_no = ((x_vec(i) -offset - mod(x_vec(i)-offset, l))/l) + 1
	  	if (x_element_no == ne + 1) then
	    	x_element_no = x_element_no - 1
	  	end if

	  	r1 = (x_element_no - 1)*l

	  	q1_hin(:) = q_hin_temp(element_dofs(x_element_no, :), 1)
	  	q1_dot_hin(:) = q_dot_hin_temp(element_dofs(x_element_no, :), 1)


	  	q1_art(:) = q_art_temp(element_dofs(x_element_no, :), 1)
	  	q1_dot_art(:) = q_dot_art_temp(element_dofs(x_element_no, :), 1)


	  	call Hermite_Funcs(x_vec(i)-r1, l, h)
	  	call Hermite_First_Der_Funcs(x_vec(i)-r1, l, h_dash)

	  	w_hin(1, i) = dot_product(h, q1_hin)
	  	w_dot_hin(1, i) = dot_product(h, q1_dot_hin)
	  	w_dash_hin(1, i) = dot_product(h_dash, q1_hin)
	  	w_dot_dash_hin(1, i) = dot_product(h_dash, q1_dot_hin)

	  	w_art(1,i) = dot_product(h, q1_art)
	  	w_dot_art(1,i) = dot_product(h, q1_dot_art)
	  	w_dash_art(1,i) = dot_product(h_dash, q1_art)
	  	w_dot_dash_art(1,i) = dot_product(h_dash, q1_dot_art)

	end do

end subroutine compute_w

!-------------------------------------------------------------------------------

!=======================================================================
!> This subroutine computes the w_ddot, w_ddot_dash at a given 
!> spatial and time location for hingeless and articulated.
!                           ~   ~   ~   ~   ~   ~   ~   ~
!                  			 w_ddot_hin, w_ddot_dash_hin
!							 w_ddot_art, w_ddot_dash_art
!                           ~   ~   ~   ~   ~   ~   ~   ~
!=======================================================================

subroutine compute_wddot(q_ddot_hin, q_ddot_art, x_vec, w_ddot_hin, w_ddot_dash_hin, &
						 w_ddot_art, w_ddot_dash_art)
	use hermite
	use blade_parameters
	use finite_elements
	implicit none

	!=======================================================================
	!                                 Inputs
	!=======================================================================
	real(kind = 8), intent(IN) 					   :: q_ddot_hin(2*ne, 1), &
													  q_ddot_art(2*ne+1, 1), &
													  x_vec(np_spatial)

	!=======================================================================
	!                                 Outputs
	!=======================================================================
	real(kind = 8), intent(OUT) 				   :: w_ddot_hin(1, np_spatial), &
													  w_ddot_art(1, np_spatial), &
								   				      w_ddot_dash_hin(1, np_spatial), &
								   				      w_ddot_dash_art(1, np_spatial)

	!=======================================================================
	!                             Local Variables
	!=======================================================================

	real(kind = 8)  		                       :: q_ddot_hin_temp(2*ne+2, 1), &
													  q_ddot_art_temp(2*ne+2, 1), &
					  								  l, r1, q1_hin(dof_per_element), &
					  								  q1_art(dof_per_element), &
					  								  h(dof_per_element), h_dash(dof_per_element)

	integer 									   :: i, x_element_no

	!=======================================================================
	!                                 Main Code
	!=======================================================================

	q_ddot_hin_temp(1:2, 1) = 0.0d0
	q_ddot_hin_temp(3:, 1) = q_ddot_hin(:, 1)


	q_ddot_art_temp(1, 1) = 0.0d0
	q_ddot_art_temp(2:,1) = q_ddot_art(:,1)

	l = R/ne

	do i = 1, SIZE(x_vec), 1
		x_element_no = ((x_vec(i) -offset - mod(x_vec(i)-offset, l))/l) + 1
	  	if (x_element_no == ne + 1) then
	    	x_element_no = x_element_no - 1
	  	end if

	  	r1 = (x_element_no - 1)*l

	  	q1_hin(:) = q_ddot_hin_temp(element_dofs(x_element_no, :), 1)
	  	q1_art(:) = q_ddot_art_temp(element_dofs(x_element_no, :), 1)

	  	call Hermite_Funcs(x_vec(i)-r1, l, h)
	  	call Hermite_First_Der_Funcs(x_vec(i)-r1, l, h_dash)

	  	w_ddot_hin(1,i) = dot_product(h, q1_hin)
		w_ddot_art(1,i) = dot_product(h, q1_art)
	  	w_ddot_dash_art(1,i) = dot_product(h_dash, q1_art)
	  	w_ddot_dash_hin(1,i) = dot_product(h_dash, q1_hin)

	end do

end subroutine compute_wddot

!-------------------------------------------------------------------------------
